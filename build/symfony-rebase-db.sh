#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

echo "Removing old baseline Migration version"

cd /var/www/$APPNAME/www/app/DoctrineMigrations

find . -type f -not -name "Version1.php" -exec rm -rf {} \;

cd /var/www/$APPNAME/www

#php app/console doctrine:generate:entities
php app/console doctrine:database:drop --force
php app/console doctrine:database:create
php app/console doctrine:migrations:diff

cd /var/www/$APPNAME/www

sudo php app/console cache:clear