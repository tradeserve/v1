#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

APP_PATH=/var/www/$APPNAME/www

sudo chmod -R 0777 ~/.composer
sudo chmod -R 0777 $APP_PATH/bin
sudo chmod -R 0777 $APP_PATH/app/logs
sudo chmod -R 0777 $APP_PATH/app/cache
sudo chmod -R 0777 $APP_PATH/app/config/parameters.yml

sudo rm -rf $APP_PATH/app/cache/*

sudo php $APP_PATH/app/console camelot:core:assets

sudo composer update

sudo php $APP_PATH/app/console doctrine:migrations:migrate --no-interaction
sudo php $APP_PATH/app/console tradeserve:core:update