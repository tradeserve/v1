#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

sudo apt-get install -y python-software-properties
sudo apt-get install -y software-properties-common

# Update Repositories
DEBIAN_FRONTEND=noninteractive sudo add-apt-repository -y ppa:ondrej/php5-5.6
DEBIAN_FRONTEND=noninteractive sudo add-apt-repository -y ppa:nginx/stable

# Update the box
# --------------
# Downloads the package lists from the repositories
# and "updates" them to get information on the newest
# versions of packages and their dependencies
DEBIAN_FRONTEND=noninteractive sudo apt-get -y update

echo "SET grub-pc/install_devices /dev/sda" | debconf-communicate

# Nginx acts weird here, this shouldn't be necessary, but it is..
DEBIAN_FRONTEND=noninteractive sudo apt-get -y upgrade nginx
DEBIAN_FRONTEND=noninteractive sudo apt-get -y upgrade

echo "America/New_York" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata

# Install LEMP before anything else
DEBIAN_FRONTEND=noninteractive sudo apt-get -y -f install php5 php5-mysql nginx

# Install the packages we need..
DEBIAN_FRONTEND=noninteractive sudo apt-get -y install php5-dev php5-cli php5-curl php5-mcrypt phpunit memcached php5-memcached php5-fpm curl git vim build-essential openssl libreadline6 libreadline6-dev git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison pkg-config sqlite3 libsqlite3-dev
sudo pecl install xdebug

# Update the box (AGAIN)
# ----------------------
# Downloads the package lists from the repositories
# and "updates" them to get information on the newest
# versions of packages and their dependencies
#sudo apt-get -y update
#sudo apt-get -y upgrade

# Install MySQL
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
DEBIAN_FRONTEND=noninteractive sudo apt-get install -y mysql-server-5.5

# MySQL Secure Installation as defined via: mysql_secure_installation
# mysql -uroot -p$DBPASSWD -e "DROP DATABASE test"
# mysql -uroot -p$DBPASSWD -e "DELETE FROM mysql.user WHERE User='root' AND NOT IN ('localhost', '127.0.0.1', '::1')"
# mysql -uroot -p$DBPASSWD -e "DELETE FROM mysql.user WHERE User=''"
# mysql -uroot -p$DBPASSWD -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
# mysql -uroot -p$DBPASSWD -e "FLUSH PRIVILEGES"

# Setup required database structure
mysql_install_db

# Setup MySQL Configuration
sudo cp /var/www/$APPNAME/build/configs/mysql/my.cnf /etc/mysql/my.cnf

# Enable mycrpt for PHP5
php5enmod mcrypt

# Install Composer
curl -s https://getcomposer.org/installer | php
# Make Composer available globally
mv composer.phar /usr/local/bin/composer
sudo chmod a+x /usr/local/bin/composer

# Setup HTTP Server (NGINX, PHP5-FPM, PHP configs, etc)
sudo bash $DIR/http_config.sh

# Install self-signed SSL Cert
#sudo mkdir /etc/nginx/ssl
#sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/key.key -out /etc/nginx/ssl/cert.crt -subj "/C=SG/ST=USA/L=USA/O=Webmaster/OU=./CN=$APPNAME/emailAddress=ssl@$APPNAME.dev"

# # Install PHPMyAdmin NOT ADVISABLE FOR PRODUCTION
# echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
# echo "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD" | debconf-set-selections
# echo "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD" | debconf-set-selections
# echo "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD" | debconf-set-selections
# echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
# DEBIAN_FRONTEND=noninteractive sudo apt-get install -y phpmyadmin
# # Make PHPMyAdmin available as http://localhost/phpmyadmin
# ln -s /usr/share/phpmyadmin /var/www/$APPNAME/www/web/phpmyadmin

# Set up the database
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME"
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASSWD'"
#mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'$MACHINE_IP' IDENTIFIED BY '$DBPASSWD'"
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'192.168.50.20' IDENTIFIED BY '$DBPASSWD'"

DEBIAN_FRONTEND=noninteractive sudo apt-get install -y unzip php5-gd gawk php-apc php5-mysqlnd php5-curl git expect sendemail libio-socket-ssl-perl nfs-common portmap vim

# Setup Frontend Tools (ruby, compass, sass, uglify, etc)
sudo bash $DIR/frontend_config.sh

sudo apt-get -y remove apache2
sudo apt-get -y autoremove

sudo service nginx restart
sudo service php5-fpm restart
sudo service mysql restart

# If you're running composer and/or Symfony
cd /var/www/$APPNAME/www
composer update --prefer-dist
composer dump-autoload --optimize

# Hack fix for swifmailer if you use Symfony 2.4+
sudo cp /var/www/$APPNAME/build/configs/swiftmailer/MailboxHeader.php /var/www/$APPNAME/www/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php

sudo bash /var/www/$APPNAME/build/symfony-db.sh

echo ''
echo '**************************************************'
echo ' Stack build complete!'
echo '**************************************************'
echo ''