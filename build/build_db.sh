#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

# Set up the database
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME"
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASSWD'"
#mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'$MACHINE_IP' IDENTIFIED BY '$DBPASSWD'"
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES on $DBNAME.* to '$DBUSER'@'192.168.50.20' IDENTIFIED BY '$DBPASSWD'"