#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

echo 'Installing Frontend Tools..'
sudo chmod -R 0777 /home/vagrant
sudo chown vagrant:vagrant -R /home/vagrant

echo 'Installing Frontend Tools..'
sudo apt-get remove -y nodejs
sudo apt-get remove -y npm
sudo curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs
sudo npm config set registry http://registry.npmjs.org/
sudo npm install source-map -g
sudo npm update --save-dev
sudo npm install uglify-js@1.3 -g

# Fix permissions and add Vagrant to the RVM group
sudo rvm group add rvm vagrant
sudo rvm fix-permissions

echo 'Installing Ruby and Gems..'
sudo apt-get remove --purge ruby-rvm ruby
sudo rm -rf /usr/share/ruby-rvm /etc/rmvrc /etc/profile.d/rvm.sh
rm -rf ~/.rvm* ~/.gem/ ~/.bundle*
echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc
echo "export rvm_max_time_flag=20" >> ~/.rvmrc
#tail ~/.gemrc
echo "[[ -s '${HOME}/.rvm/scripts/rvm' ]] && source '${HOME}/.rvm/scripts/rvm'" >> ~/.bashrc

sudo chmod -R 0777 /home/vagrant
sudo chown vagrant:vagrant -R /home/vagrant

sudo gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
command curl -sSL https://rvm.io/mpapis.asc | gpg --import -
sudo curl -L https://get.rvm.io | bash -s stable --ruby

source /home/vagrant/.rvm/scripts/rvm
source /usr/local/rvm/scripts/rvm

rvm group add rvm vagrant
rvm fix-permissions

# Now that permissions are fixed, install ruby 2.2.2
rvm install 2.2.2

# Reinstall ruby 1.9.3 for backwards compatability
rvm reinstall 1.9.3

# Use Ruby 2.2.2 by default globally
rvm --default use 2.2.2

rvm all do gem install compass
rvm all do gem install sass
rvm all do gem install --pre sass-css-importer

sudo rm -rf /usr/local/bin/sass
sudo rm -rf /usr/local/bin/compass
sudo rm -rf /usr/local/bin/ruby

sudo ln -s /home/vagrant/.rvm/rubies/ruby-2.2.2/bin/ruby /usr/local/bin/ruby
sudo ln -s /home/vagrant/.rvm/gems/ruby-2.2.2/bin/compass /usr/local/bin/compass
sudo ln -s /home/vagrant/.rvm/gems/ruby-2.2.2/bin/sass /usr/local/bin/sass