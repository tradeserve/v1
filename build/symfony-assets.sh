#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

sudo rm -rf /var/www/$APPNAME/www/app/cache/*
sudo php /var/www/$APPNAME/www/app/console assets:install
sudo php /var/www/$APPNAME/www/app/console assetic:dump
sudo php /var/www/$APPNAME/www/app/console cache:clear --env=dev
sudo chmod 0777 -R /var/www/$APPNAME/www/app/cache

echo 'Done!'