#!/bin/bash

# Include the global config file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/config

sudo cp /var/www/$APPNAME/build/configs/nginx/nginx.conf /etc/nginx/nginx.conf
sudo cp /var/www/$APPNAME/build/configs/nginx/app.conf /etc/nginx/app.conf

if [ $STACKNAME == 'dev' ]
then
    find /etc/nginx/app.conf -type f -exec sed -i "s/{FRONTEND_CONTROLLER}/app_dev/g" {} \;
else
    find /etc/nginx/app.conf -type f -exec sed -i "s/{FRONTEND_CONTROLLER}/app/g" {} \;
fi

find /etc/nginx/app.conf -type f -exec sed -i "s/{APP_NAME}/$APPNAME/g" {} \;
sudo cp /var/www/$APPNAME/build/configs/nginx/sites-available/default_vagrant /etc/nginx/sites-available/default
find /etc/nginx/sites-available/default -type f -exec sed -i "s/{APP_NAME}/$APPNAME/g" {} \;
sudo rm -rf /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
sudo cp /var/www/$APPNAME/build/configs/php5/mods-available/xdebug.ini /etc/php5/mods-available/xdebug.ini
sudo cp /var/www/$APPNAME/build/configs/php5/php.ini /etc/php5/fpm/php.ini
sudo cp /var/www/$APPNAME/build/configs/php5/php.ini /etc/php5/cli/php.ini
sudo cp /var/www/$APPNAME/build/configs/php5-fpm/www.conf /etc/php5/fpm/pool.d/www.conf

sudo nginx -v
sudo service nginx restart
sudo service php5-fpm restart