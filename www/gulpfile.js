var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
// var image = require('gulp-image');
var util = require('gulp-util');
var fs = require("fs");
var s3 = require("gulp-s3");
// var awsCredentials = JSON.parse(fs.readFileSync('./aws.json'));  

var config = {
    production: !!util.env.production
};

var baseDirectory = { components:'src/TradeServe/AppBundle/Resources/public/js/components' };

var stylesheets = [
  'src/TradeServe/AppBundle/Resources/public/sass/*.scss',
  'src/TradeServe/AppBundle/Resources/public/css/common/*.css'
];

var javascripts = [
    'src/TradeServe/AppBundle/Resources/public/js/components/**/*.js',
    'src/TradeServe/AppBundle/Resources/public/js/*.js'
];

var templates = [
    'src/TradeServe/AppBundle/Resources/public/js/components/**/*.html',  
];


var images = [
  'src/TradeServe/CoreBundle/Resources/public/img/*',
  'src/TradeServe/CoreBundle/Resources/public/img/*/*'
];

gulp.task('styles', function () {
  gulp.src(stylesheets)
      .pipe(sass())
      .pipe(minifyCss())
      .pipe(concat('tradeserve.css'))
      .pipe(gulp.dest('web/assets/css'));
});

gulp.task('javascripts', function () {
  gulp.src(javascripts)
      .pipe(concat('tradeserve.js'))
      .pipe(gulp.dest('web/assets/js'));
});

gulp.task('templates', function () {
  return gulp.src(templates, {
    base: baseDirectory.components
  }).pipe(gulp.dest('web/'));
});

// gulp.task('images', function () {
//   gulp.src(images)
//       .pipe(image())
//       .pipe(gulp.dest('web/assets/img'));
// });

gulp.task('default', ['styles', 'javascripts', 'templates']);

gulp.task('upload', ['default'], function () {
  gulp.src('web/assets/css/*')
      .pipe(s3(awsCredentials, {
        uploadPath: "/assets/css/",
        headers: {
          'x-amz-acl': 'public-read'
        }
      }));
  gulp.src('web/assets/js/*')
      .pipe(s3(awsCredentials, {
        uploadPath: "/assets/js/",
        headers: {
          'x-amz-acl': 'public-read'
        }
      }));
  gulp.src(['web/assets/img/*', 'web/assets/img/*/*'])
      .pipe(s3(awsCredentials, {
        uploadPath: "/assets/img/",
        headers: {
          'x-amz-acl': 'public-read'
        }
      }));
});