<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version1 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
           'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("CREATE TABLE IF NOT EXISTS _tradeserve (name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, value VARCHAR(8000) DEFAULT NULL COLLATE utf8_unicode_ci, activity_date DATETIME DEFAULT NULL, status INT NOT NULL, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE IF NOT EXISTS _session (session_id VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, session_value LONGTEXT NOT NULL COLLATE utf8_unicode_ci, session_time INT NOT NULL, PRIMARY KEY(session_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
           'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("DROP TABLE _tradeserve");
        $this->addSql("DROP TABLE _session");
    }
}
