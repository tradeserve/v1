;(function() {
  'use strict';

  function configure ( $routeProvider ) {

    $routeProvider.when('/app/dashboard', {
      templateUrl: 'dashboard/dashboard.html',
      controller: 'DashboardCtrl'
    });      

  }


  function DashboardCtrl ( ) {

  }

  angular
    .module( 'tradeServe.dashboard', [
      'ngRoute'
    ])
    .config( configure )
    .controller( 'DashboardCtrl', DashboardCtrl )
    ;

})();

;(function() {
  'use strict';

  function configure ( $routeProvider ) {

    $routeProvider.when('/app/dispatch', {
      templateUrl: 'dispatch/dispatchBoard.html',
      controller: 'DispatchCtrl'
    });

  };


  function DispatchCtrl ( $scope, $http, DispatchFactory ) {

      var init,
          addToSchedule;

      $scope.viewDate = new Date();

      init = function() {

        // getting customers
        // DispatchFactory.getServiceRequests().then(function(result) {
        //   $scope.serviceRequests = result.data.service_request;
        // });
        $scope.serviceRequests = DispatchFactory.getServiceRequests;

        // getting schedule
        $scope.schedule = DispatchFactory.getSchedule;

      };

      init();

      $scope.addToSchedule = function(sr) {
        console.log('adding...');
        $scope.schedule.push(
          {name: 'Hutch White', schedules:[{start_date:'1/22/2015 10:00', time: '10', client_name: 'Jane Jones'}]}
        );
      };

  };

  angular
    .module( 'tradeServe.dispatch', [
      'tradeServe.dispatchFactory',
      'ngRoute',
      'angular.filter'
    ])
    .config( configure )
    .controller( 'DispatchCtrl', DispatchCtrl )
    ;

})();

;(function() {
  'use strict';


  function DispatchFactory ( $http ) {

      var getServiceRequests,
          getSchedule;

      // getServiceRequests = function() {
      //     // $http() returns a $promise that we can add handlers with .then()
      //     return $http({
      //         method: 'GET',
      //         url: 'http://tradeserve.dev/api/v1/1/call/service_request'
      //      });
      //  };

      getServiceRequests = [
            {customer: {address: "4002 Beechwood Dr. Columbia, KS 02135", email: null, first_name: "Peter", home_phone: null, last_name: "Petrelli", mobile_phone: "2345768903"}, estimated_start_time:'1/22/15 10:00'},
            {customer: {address: "123 E Main. Columbia, KS 02135", email: null, first_name: "Erica", home_phone: null, last_name: "Kravit", mobile_phone: "123456781"}, estimated_start_time:'1/22/15 13:00'},
            {customer: {address: "3299 Silversmith Rd. Columbia, KS 02135", email: null, first_name: "Monica", home_phone: null, last_name: "Lewis", mobile_phone: "9829340820"}, estimated_start_time:'1/22/15 16:00'},
            {customer: {address: "111 Millwood. Columbia, KS 02135", email: null, first_name: "Tommy", home_phone: null, last_name: "Lang", mobile_phone: "0812098221"}, estimated_start_time:'1/22/15 17:00'},
            {customer: {address: "24 Rivers Edge. Columbia, KS 02135", email: null, first_name: "Hiro", home_phone: null, last_name: "Nakamora", mobile_phone: "0829038420"}, estimated_start_time:'1/22/15 08:00'},
        ];

       getSchedule =  [
            {name: 'Hutch White', schedules:[{start_date:'1/17/2015 8:00', time: '8', client_name: 'Jane Jones'}]},
            {name: 'Jake Litwicki', schedules:[{start_date:'1/17/2015 8:00', time: '9', client_name: 'Jane Jones'}]},
            {name: 'Josh Stevens', schedules:[{start_date:'1/17/2015 8:00', time: '13', client_name: 'Jane Jones'}]}
          ];

       return {
        getServiceRequests: getServiceRequests,
        getSchedule: getSchedule
       }

  };

  angular
    .module( 'tradeServe.dispatchFactory', [
    ])
    .factory( 'DispatchFactory', DispatchFactory )
    ;

})();

/*
 AngularJS v1.5.0-build.4480+sha.495d40d
 (c) 2010-2016 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(r,d,C){'use strict';function w(s,h,g){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,c,b,f,y){function k(){n&&(g.cancel(n),n=null);l&&(l.$destroy(),l=null);m&&(n=g.leave(m),n.then(function(){n=null}),m=null)}function z(){var b=s.current&&s.current.locals;if(d.isDefined(b&&b.$template)){var b=a.$new(),f=s.current;m=y(b,function(b){g.enter(b,null,m||c).then(function(){!d.isDefined(u)||u&&!a.$eval(u)||h()});k()});l=f.scope=b;l.$emit("$viewContentLoaded");
l.$eval(x)}else k()}var l,m,n,u=b.autoscroll,x=b.onload||"";a.$on("$routeChangeSuccess",z);z()}}}function A(d,h,g){return{restrict:"ECA",priority:-400,link:function(a,c){var b=g.current,f=b.locals;c.html(f.$template);var y=d(c.contents());if(b.controller){f.$scope=a;var k=h(b.controller,f);b.controllerAs&&(a[b.controllerAs]=k);c.data("$ngControllerController",k);c.children().data("$ngControllerController",k)}a[b.resolveAs||"$resolve"]=f;y(a)}}}r=d.module("ngRoute",["ng"]).provider("$route",function(){function s(a,
c){return d.extend(Object.create(a),c)}function h(a,d){var b=d.caseInsensitiveMatch,f={originalPath:a,regexp:a},g=f.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(a,d,b,c){a="?"===c?c:null;c="*"===c?c:null;g.push({name:b,optional:!!a});d=d||"";return""+(a?"":d)+"(?:"+(a?d:"")+(c&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([\/$\*])/g,"\\$1");f.regexp=new RegExp("^"+a+"$",b?"i":"");return f}var g={};this.when=function(a,c){var b=d.copy(c);d.isUndefined(b.reloadOnSearch)&&
(b.reloadOnSearch=!0);d.isUndefined(b.caseInsensitiveMatch)&&(b.caseInsensitiveMatch=this.caseInsensitiveMatch);g[a]=d.extend(b,a&&h(a,b));if(a){var f="/"==a[a.length-1]?a.substr(0,a.length-1):a+"/";g[f]=d.extend({redirectTo:a},h(f,b))}return this};this.caseInsensitiveMatch=!1;this.otherwise=function(a){"string"===typeof a&&(a={redirectTo:a});this.when(null,a);return this};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$templateRequest","$sce",function(a,c,b,f,h,k,r){function l(b){var e=
t.current;(w=(p=n())&&e&&p.$$route===e.$$route&&d.equals(p.pathParams,e.pathParams)&&!p.reloadOnSearch&&!x)||!e&&!p||a.$broadcast("$routeChangeStart",p,e).defaultPrevented&&b&&b.preventDefault()}function m(){var v=t.current,e=p;if(w)v.params=e.params,d.copy(v.params,b),a.$broadcast("$routeUpdate",v);else if(e||v)x=!1,(t.current=e)&&e.redirectTo&&(d.isString(e.redirectTo)?c.path(u(e.redirectTo,e.params)).search(e.params).replace():c.url(e.redirectTo(e.pathParams,c.path(),c.search())).replace()),f.when(e).then(function(){if(e){var a=
d.extend({},e.resolve),b,c;d.forEach(a,function(b,e){a[e]=d.isString(b)?h.get(b):h.invoke(b,null,null,e)});d.isDefined(b=e.template)?d.isFunction(b)&&(b=b(e.params)):d.isDefined(c=e.templateUrl)&&(d.isFunction(c)&&(c=c(e.params)),d.isDefined(c)&&(e.loadedTemplateUrl=r.valueOf(c),b=k(c)));d.isDefined(b)&&(a.$template=b);return f.all(a)}}).then(function(c){e==t.current&&(e&&(e.locals=c,d.copy(e.params,b)),a.$broadcast("$routeChangeSuccess",e,v))},function(b){e==t.current&&a.$broadcast("$routeChangeError",
e,v,b)})}function n(){var a,b;d.forEach(g,function(f,g){var q;if(q=!b){var h=c.path();q=f.keys;var l={};if(f.regexp)if(h=f.regexp.exec(h)){for(var k=1,n=h.length;k<n;++k){var m=q[k-1],p=h[k];m&&p&&(l[m.name]=p)}q=l}else q=null;else q=null;q=a=q}q&&(b=s(f,{params:d.extend({},c.search(),a),pathParams:a}),b.$$route=f)});return b||g[null]&&s(g[null],{params:{},pathParams:{}})}function u(a,b){var c=[];d.forEach((a||"").split(":"),function(a,d){if(0===d)c.push(a);else{var f=a.match(/(\w+)(?:[?*])?(.*)/),
g=f[1];c.push(b[g]);c.push(f[2]||"");delete b[g]}});return c.join("")}var x=!1,p,w,t={routes:g,reload:function(){x=!0;a.$evalAsync(function(){l();m()})},updateParams:function(a){if(this.current&&this.current.$$route)a=d.extend({},this.current.params,a),c.path(u(this.current.$$route.originalPath,a)),c.search(a);else throw B("norout");}};a.$on("$locationChangeStart",l);a.$on("$locationChangeSuccess",m);return t}]});var B=d.$$minErr("ngRoute");r.provider("$routeParams",function(){this.$get=function(){return{}}});
r.directive("ngView",w);r.directive("ngView",A);w.$inject=["$route","$anchorScroll","$animate"];A.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map
;(function() {
  'use strict';

  function configure( $routeProvider, $httpProvider, $locationProvider,
                      $logProvider, Constants ) {

    // // Default Routes
    $routeProvider
      .otherwise( '/app/dashboard/' );

    // HTML 5 Mode
    $locationProvider
      .html5Mode({
        enabled: true,
        requireBase: true
      });

  }


  var myApp = angular.module('myApp',[]);


    function AppCtrl( Constants, $scope, $window, $http ) {

      $scope.currentDate = new Date();

      //load tabs

      var init,
          getCustomers;

      getCustomers = function() {
          // $http() returns a $promise that we can add handlers with .then()
          return $http({
              method: 'GET',
              url: 'http://tradeserve.dev/api/v1/1/internal/customer'
           });
       };


      init = function() {

        // getting customers
        getCustomers().then(function(result) {
          $scope.customers = result.data.customer;
          console.log($scope.customers);
        });
      };

      init();    


  }

  angular
    .module( 'tradeServe', [
      'tradeServe.Constants',
      'tradeServe.dashboard',
      'tradeServe.dispatch',
      'tradeServe.services'
    ],
    function ($interpolateProvider) {
      $interpolateProvider.startSymbol('{[');
      $interpolateProvider.endSymbol(']}');
    }
  )
  .config( configure )
  .controller( 'AppCtrl', AppCtrl )
  ;

})();

// jscs:disable
;(function() {
  'use strict';

  angular.module( 'tradeServe.Constants', [] )
    .constant( 'Constants', {
      appName: 'Trade Serve',
      version: '0.1.1',
      apiURL: 'http://tradeserve.dev/api/v1/1/'
    })
  ;

})();

jQuery(document).ready(function($){

       //Navigation Menu Slider
        $('#nav-expander').on('click',function(e){
      		e.preventDefault();
      		$('body').toggleClass('nav-expanded');
      	});
      	$('#nav-close').on('click',function(e){
      		e.preventDefault();
      		$('body').removeClass('nav-expanded');
      	});

      	// Initialize navgoco with default options
        $(".user-menu").navgoco({
            caret: '<span class="caret"></span>',
            accordion: false,
            openClass: 'open',
            save: true,
            cookie: {
                name: 'navgoco',
                expires: false,
                path: '/'
            },
            slide: {
                duration: 300,
                easing: 'swing'
            }
        });


        // action for tabs
        $("#slide-tabs li").on('click', function(e){
          e.preventDefault();
          $('body').toggleClass('tab-expanded');
        });


        // initialize popovers
        $('[data-toggle="popover"]').popover({
            container: 'body'
        });

});
/*
 * jQuery Navgoco Menus Plugin v0.1.5 (2013-09-07)
 * https://github.com/tefra/navgoco
 *
 * Copyright (c) 2013 Chris T (@tefra)
 * BSD - https://github.com/tefra/navgoco/blob/master/LICENSE-BSD
 */
(function($) {

	"use strict";

	/**
	 * Plugin Constructor. Every menu must have a unique id which will either
	 * be the actual id attribute or its index in the page.
	 *
	 * @param {Element} el
	 * @param {Object} options
	 * @param {Integer} idx
	 * @returns {Object} Plugin Instance
	 */
	var Plugin = function(el, options, idx) {
		this.el = el;
		this.$el = $(el);
		this.options = options;
		this.uuid = this.$el.attr('id') ? this.$el.attr('id') : idx;
		this.state = {};
		this.init();
		return this;
	};

	/**
	 * Plugin methods
	 */
	Plugin.prototype = {
		/**
		 * Load cookie, assign a unique data-index attribute to
		 * all sub-menus and show|hide them according to cookie
		 * or based on the parent open class. Find all parent li > a
		 * links add the carent if it's on and attach the event click
		 * to them.
		 */
		init: function() {
			var self = this;
			self._load();
			self.$el.find('ul').each(function(idx) {
				var sub = $(this);
				sub.attr('data-index', idx);
				if (self.options.save && self.state.hasOwnProperty(idx)) {
					sub.parent().addClass(self.options.openClass);
					sub.show();
				} else if (sub.parent().hasClass(self.options.openClass)) {
					sub.show();
					self.state[idx] = 1;
				} else {
					sub.hide();
				}
			});

			if (self.options.caret) {
				self.$el.find("li:has(ul) > a").append(self.options.caret);
			}

			var links = self.$el.find("li > a");
			links.on('click', function(event) {
				event.stopPropagation();
				var sub = $(this).next();
				sub = sub.length > 0 ? sub : false;
				self.options.onClickBefore.call(this, event, sub);
				if (sub) {
					event.preventDefault();
					self._toggle(sub, sub.is(":hidden"));
					self._save();
				} else {
					if (self.options.accordion) {
						var allowed = self.state = self._parents($(this));
						self.$el.find('ul').filter(':visible').each(function() {
							var sub = $(this),
								idx = sub.attr('data-index');

							if (!allowed.hasOwnProperty(idx)) {
								self._toggle(sub, false);
							}
						});
						self._save();
					}
				}
				self.options.onClickAfter.call(this, event, sub);
			});
		},
		/**
		 * Accepts a JQuery Element and a boolean flag. If flag is false it removes the `open` css
		 * class from the parent li and slides up the sub-menu. If flag is open it adds the `open`
		 * css class to the parent li and slides down the menu. If accordion mode is on all
		 * sub-menus except the direct parent tree will close. Internally an object with the menus
		 * states is maintained for later save duty.
		 *
		 * @param {Element} sub
		 * @param {Boolean} open
		 */
		_toggle: function(sub, open) {
			var self = this,
				idx = sub.attr('data-index'),
				parent = sub.parent();

			self.options.onToggleBefore.call(this, sub, open);
			if (open) {
				parent.addClass(self.options.openClass);
				sub.slideDown(self.options.slide);
				self.state[idx] = 1;

				if (self.options.accordion) {
					var allowed = self.state = self._parents(sub);
					allowed[idx] = self.state[idx] = 1;

					self.$el.find('ul').filter(':visible').each(function() {
						var sub = $(this),
							idx = sub.attr('data-index');

						if (!allowed.hasOwnProperty(idx)) {
							self._toggle(sub, false);
						}
					});
				}
			} else {
				parent.removeClass(self.options.openClass);
				sub.slideUp(self.options.slide);
				self.state[idx] = 0;
			}
			self.options.onToggleAfter.call(this, sub, open);
		},
		/**
		 * Returns all parents of a sub-menu. When obj is true It returns an object with indexes for
		 * keys and the elements as values, if obj is false the object is filled with the value `1`.
		 *
		 * @since v0.1.2
		 * @param {Element} sub
		 * @param {Boolean} obj
		 * @returns {Object}
		 */
		_parents: function(sub, obj) {
			var result = {},
				parent = sub.parent(),
				parents = parent.parents('ul');

			parents.each(function() {
				var par = $(this),
					idx = par.attr('data-index');

				if (!idx) {
					return false;
				}
				result[idx] = obj ? par : 1;
			});
			return result;
		},
		/**
		 * If `save` option is on the internal object that keeps track of the sub-menus states is
		 * saved with a cookie. For size reasons only the open sub-menus indexes are stored.		 *
		 */
		_save: function() {
			if (this.options.save) {
				var save = {};
				for (var key in this.state) {
					if (this.state[key] === 1) {
						save[key] = 1;
					}
				}
				cookie[this.uuid] = this.state = save;
				$.cookie(this.options.cookie.name, JSON.stringify(cookie), this.options.cookie);
			}
		},
		/**
		 * If `save` option is on it reads the cookie data. The cookie contains data for all
		 * navgoco menus so the read happens only once and stored in the global `cookie` var.
		 */
		_load: function() {
			if (this.options.save) {
				if (cookie === null) {
					var data = $.cookie(this.options.cookie.name);
					cookie = (data) ? JSON.parse(data) : {};
				}
				this.state = cookie.hasOwnProperty(this.uuid) ? cookie[this.uuid] : {};
			}
		},
		/**
		 * Public method toggle to manually show|hide sub-menus. If no indexes are provided all
		 * items will be toggled. You can pass sub-menus indexes as regular params. eg:
		 * navgoco('toggle', true, 1, 2, 3, 4, 5);
		 *
		 * Since v0.1.2 it will also open parents when providing sub-menu indexes.
		 *
		 * @param {Boolean} open
		 */
		toggle: function(open) {
			var self = this,
				length = arguments.length;

			if (length <= 1) {
				self.$el.find('ul').each(function() {
					var sub = $(this);
					self._toggle(sub, open);
				});
			} else {
				var idx,
					list = {},
					args = Array.prototype.slice.call(arguments, 1);
				length--;

				for (var i = 0; i < length; i++) {
					idx = args[i];
					var sub = self.$el.find('ul[data-index="' + idx + '"]').first();
					if (sub) {
						list[idx] = sub;
						if (open) {
							var parents = self._parents(sub, true);
							for (var pIdx in parents) {
								if (!list.hasOwnProperty(pIdx)) {
									list[pIdx] = parents[pIdx];
								}
							}
						}
					}
				}

				for (idx in list) {
					self._toggle(list[idx], open);
				}
			}
			self._save();
		},
		/**
		 * Removes instance from JQuery data cache and unbinds events.
		 */
		destroy: function() {
			$.removeData(this.$el);
			this.$el.find("li:has(ul) > a").unbind('click');
		}
	};

	/**
	 * A JQuery plugin wrapper for navgoco. It prevents from multiple instances and also handles
	 * public methods calls. If we attempt to call a public method on an element that doesn't have
	 * a navgoco instance, one will be created for it with the default options.
	 *
	 * @param {Object|String} options
	 */
	$.fn.navgoco = function(options) {
		if (typeof options === 'string' && options.charAt(0) !== '_' && options !== 'init') {
			var callback = true,
				args = Array.prototype.slice.call(arguments, 1);
		} else {
			options = $.extend({}, $.fn.navgoco.defaults, options || {});
			if (!$.cookie) {
				options.save = false;
			}
		}
		return this.each(function(idx) {
			var $this = $(this),
				obj = $this.data('navgoco');

			if (!obj) {
				obj = new Plugin(this, callback ? $.fn.navgoco.defaults : options, idx);
				$this.data('navgoco', obj);
			}
			if (callback) {
				obj[options].apply(obj, args);
			}
		});
	};
	/**
	 * Global var holding all navgoco menus open states
	 *
	 * @type {Object}
	 */
	var cookie = null;

	/**
	 * Default navgoco options
	 *
	 * @type {Object}
	 */
	$.fn.navgoco.defaults = {
		caret: '<span class="caret"></span>',
		accordion: false,
		openClass: 'open',
		save: true,
		cookie: {
			name: 'navgoco',
			expires: false,
			path: '/'
		},
		slide: {
			duration: 400,
			easing: 'swing'
		},
		onClickBefore: $.noop,
		onClickAfter: $.noop,
		onToggleBefore: $.noop,
		onToggleAfter: $.noop
	};
})(jQuery);
;
(function () {

  var app = angular.module('tradeServe.services',[]);	

})();