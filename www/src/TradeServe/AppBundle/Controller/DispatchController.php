<?php

namespace TradeServe\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use JMS\Serializer\SerializationContext;

class DispatchController extends TradeServeController
{

    public function getCategoryAction($categorySlug)
    {
        $category = $this->get('category_manager')->getBySlug($categorySlug);
        $products = ''; // get data, in this case list of products.

        $templateData = array('category' => $category);

        $view = $this->view($products, 200)
           ->setTemplate("TradeServeAppBundle:Dispatch:index.html.twig")
           ->setTemplateVar('products')
           ->setTemplateData($templateData);

        return $this->handleView($view);
    }

    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            //return $this->generateUrl('login');
            $view = View::createRouteRedirect('login');
        } else {
            $user = $this->currentUser();
            $accounts = $user->getAccounts();
            $em = $this->getDoctrine()->getManager();
            $org = $em->find('TradeServe\CoreBundle\Entity\Organization', 1);
            $customers = $em->getRepository('TradeServe\CoreBundle\Entity\Customer')->getAllCustomersByOrganization($org->getId());
            //$org = $em->find("TradeServe\CoreBundle\Entity\Organization", 1);

//        $em = $this->getDoctrine()->getManager();
//        $query = $em->createQuery(
//           'SELECT u
//             FROM TradeServeAppBundle:Customer u'
//        );
//        $customers = $query->getArrayResult();

            //$customers = $this->container->get('Doctrine')->getRepository('TradeServeAppBundle:Customer')->findAll();
            //$technicians = $serviceRequest->getTechnicians();
//        $serviceRequest = $this->container->get('Doctrine')->getRepository('TradeServeAppBundle:ServiceRequest')->findBy(array(
//           'organization' => 1
//        ));
            //$output = count($customers);
            //$output = json_encode($customers);
            //echo $output;
            //var_dump($serviceRequest);

//        foreach ($customers as $customer) {
//            foreach ($customer->getServiceRequests() as $request) {
//                $requests[] = $request;
//            }
//            $out[$customer->getFullName()] = $requests;
////            if ($serviceRequest instanceof ServiceRequest) {
////                var_dump($serviceRequest);
//        }
            //$output = json_encode($customers);
            $templateData = array('page_title' => 'Dispatch');
            $view = $this->view($accounts, 200)
               ->setTemplate("TradeServeAppBundle:Dispatch:index.html.twig")
               ->setTemplateVar('customers')
               ->setTemplateData($templateData);
//            $view = View::create(array('page_title' => $output, 'dispatch' => true))
////                ->setEngine('php');
//               ->setTemplate(new TemplateReference('TradeServeAppBundle', 'Dispatch', 'index'));

        }
// else {

//                $page_title = "This didn't work correctly";
//            }
        //}
        return $this->handleView($view);
    }

}
