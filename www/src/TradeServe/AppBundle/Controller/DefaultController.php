<?php

namespace TradeServe\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
   public function indexAction()
   {
      return $this->render('TradeServeAppBundle:Default:index.html.twig');
   }
}
