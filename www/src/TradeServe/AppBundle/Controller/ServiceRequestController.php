<?php

namespace TradeServe\AppBundle\Controller;

use FOS\RestBundle\View\View;

class ServiceRequestController extends TradeServeController
{

    public function indexAction()
    {
        //$serializer = $this->container->get('jms_serializer');

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $view = View::createRouteRedirect('login');
        } else {
//            $user = $this->currentUser();
//            $accounts = $user->getAccounts();
            $em = $this->getDoctrine()->getManager();
            $org = $em->find('TradeServe\CoreBundle\Entity\Organization', 1);
            $requests = $em->getRepository('TradeServe\CoreBundle\Entity\ServiceRequest')->getServiceRequestByOrganization($org);
            //$serializer->serialize($requests, 'json');
            $templateData = array('page_title' => 'Service Requests', 'service' => true);
            $view = $this->view($requests, 200)
               ->setTemplate("TradeServeAppBundle:ServiceRequest:index.html.twig")
               ->setTemplateVar('customers')
               ->setTemplateData($templateData);

        }

        return $this->handleView($view);
    }


//    public function indexAction()
//    {
//        return $this->render('TradeServeAppBundle:ServiceRequest:index.html.twig', array(
//           'page_title' => 'Service Requests',
//           'service'    => true
//        ));
//    }

}
