<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 4:55 PM
 */

namespace TradeServe\AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ServiceRequestController extends Controller
{
    /**
     * @Route("/api/servicerequests")
     * @Method("POST")
     */
    public function newAction()
    {
        return new Response('Let\'s do this');
    }
}