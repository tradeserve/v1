<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 6:52 PM
 */

namespace TradeServe\AppBundle\Controller\Api;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TradeServe\AppBundle\Controller\TradeServeController;
use TradeServe\CoreBundle\Entity\Customer;

class CustomerController extends TradeServeApiController
{
    /**
     * @Route("/api/customers")
     * @Method("GET")
     *
     * @return Response
     */
    public function getAction()
    {
//        $em = $this->getDoctrine()->getManager();
//        $customers = $em->getRepository('TradeServe\CoreBundle\Entity\Customer')->findAll();
//        //$data = $request->getContent();
//        $array = json_decode(json_encode($customers), true);
//        $data = json_encode($array);
//        return new Response($data);
    }


    public function getCustomerAction($id)
    {

        return $this->container->get('doctrine.entity_manager')->getRepository('Customer')->find($id);
//        $customer = $this->getDoctrine()
//           ->getRepository('TradeServeAppBundle:Customer')
//           ->find($id);
//        if ($customer instanceof Customer) {
////            $address = (array)$customer->getAddress();
////            $service_requests = (array)$customer->getServiceRequests();
//
////            $data = array(
////               'first_name'       => $customer->getFirstName(),
////               'last_name'        => $customer->getLastName(),
////               'address'          => $address,
////               'organization'     => $customer->getOrganization(),
////               'service_requests' => $service_requests
////            );
//            return new Response(json_encode((array)$customer), 200);
//        }
//        if (!$customer) {
//            throw $this->createNotFoundException(sprintf(
//               'No customer found with id "%s"',
//               $id
//            ));
//        }


    }

    /**
     * @Route("/api/customers")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $data = $request->getContent();
        return new Response($data);
    }
}