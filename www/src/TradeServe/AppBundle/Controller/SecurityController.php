<?php

namespace TradeServe\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\Security\Core\SecurityContext;
//use Symfony\Component\HttpFoundation\Request;

//use TradeServe\AppBundle\Form\Type\RegistrationType;
//use TradeServe\AppBundle\Form\Model\Registration;
//
//use TradeServe\AppBundle\Form\Type\UserType;
//use TradeServe\CoreBundle\Entity\User;
//
//use TradeServe\CoreBundle\Entity\Account;

class SecurityController extends Controller
{

    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('TradeServeAppBundle:Security:login.html.twig',
           array(
               // last username entered by the user
              'last_username' => $lastUsername,
              'error'         => $error,
           )
        );
    }

//    public function loginAction()
//    {
//        $request = $this->getRequest();
//        $session = $request->getSession();
//
//        $authenticationUtils = $this->get('security.authentication_utils');
//
//        // get the login error if there is one
//        $error = $authenticationUtils->getLastAuthenticationError();
//
//        // last username entered by the user
//        $lastUsername = $authenticationUtils->getLastUsername();
//
//        // get the login error if there is one
//        /*if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
//            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
//        } else {
//            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
//            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
//        }*/
//
//        $user = new User();
//        $form = $this->createForm(new UserType(), $user, array(
//           'action' => $this->generateUrl('login_check'),
//        ));
//
//        return $this->render('TradeServeAppBundle:Security:login.html.twig', array(
//            // last username entered by the user
//           'form'          => $form,
//           'last_username' => $session->get(SecurityContext::LAST_USERNAME),
//           'error'         => $error,
//           'page_title'    => 'Login',
//        ));
//    }
//
//    public function dumpStringAction()
//    {
//        return $this->render('TradeServeAppBundle:Security:dumpString.html.twig', array());
//    }
//
//    public function registerAction()
//    {
//        $navigation = array();
//        $error = array();
//        $registration = new Registration();
//        $form = $this->createForm(new RegistrationType(), $registration, array(
//           'action' => $this->generateUrl('account_create'),
//        ));
//
//        return $this->render('TradeServeAppBundle:Security:register.html.twig', array(
//           'form'       => $form->createView(),
//           'error'      => $error,
//           'navigation' => $navigation,
//           'page_title' => 'Register',
//        ));
//    }
//
//    public function createAction(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $form = $this->createForm(new RegistrationType(), new Registration());
//
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $registration = $form->getData();
//
//            $account = new Account();
//            $account->setName($registration->getFirstName() . $registration->getLastName());
//            $account->setDescription($registration->getUsername() . $registration->getFirstName() . $registration->getLastName());
//            $em->persist($account);
//            $em->flush();
//
//            $user = new User();
//            $josh = $em->getRepository('TradeServeAppBundle:User')->findOneByEmail('josh@tradeserve.com');
//            $acct = $em->getRepository('TradeServeAppBundle:Account')->findOneById($account->getId());
//            $user->addAccount($acct);
//            if ($josh instanceof User) {
//                $josh->addAccount($acct);
//                $em->persist($josh);
//            }
//
//            $roles = 'ROLE_USER';
//            //$acct_role = 'ROLE_ACCOUNT_ADMIN';
//
//            $role = $em->getRepository('TradeServeAppBundle:UserRole')->findOneByRole($roles);
//            $roleAccount = $em->getRepository('TradeServeAppBundle:AccountRole')->find(1);
//            $user->addAccountRole($roleAccount);
//            $user->addRole($role);
//            $user->setUsername($registration->getEmail());
//            $user->setFirstName($registration->getFirstName());
//            $user->setLastName($registration->getLastName());
//            $user->setEmail($registration->getEmail());
//            $user->setLastOnlineDate(new \DateTime('now'));
//            $user->setJoinDate(new \DateTime('now'));
//
//            $acct2 = $em->getRepository('TradeServeAppBundle:Account')->find(1);
//            $email = $em->getRepository('TradeServeAppBundle:SubscriberType')->find(1);
//            $sms = $em->getRepository('TradeServeAppBundle:SubscriberType')->find(1);
//            $sub = new Subscriber();
//            $sub->setAccount($acct2);
//            $sub->setName($registration->getFirstName() . ' ' . $registration->getLastName());
//            $sub->setBlacklisted(0);
//            $sub->setType($email);
//            $sub->setOptIn(1);
//            $sub->setOptInDate(new \DateTime('now'));
//            $sub->setEmail($registration->getEmail());
//            $em->persist($sub);
//            /*if ($registration->getPhoneNumber()) {
//                $sub = new Subscriber();
//                $sub->setAccount($acct2);
//                $sub->setName($registration->getFirstName() . ' ' . $registration->getLastName());
//                $sub->setBlacklisted(0);
//                $sub->setType($sms);
//                $sub->setOptIn(1);
//                $sub->setOptInDate(new \DateTime('now'));
//                $sub->setNumber($registration->getPhoneNumber());
//                $em->persist($sub);
//            }*/
//
//            $factory = $this->container->get('security.encoder_factory');
//            $encoder = $factory->getEncoder($user);
//            $password = $encoder->encodePassword($registration->getPassword(),
//               $user->getSalt()); //where $user->password has been bound in plaintext by the form
//            $user->setPassword($password);
//            $error = array();
//            $em->persist($user);
//
//            $em->flush();
//            $navigation = array();
//
//            $params = array('account' => $acct->getId());
//
//            $this->authenticateUser($user);
//
//            // where "main" is the name of your firewall in security.yml
//            $key = '_security.main.target_path';
//
//            // try to redirect to the last page, or fallback to the homepage
//            if ($this->container->get('session')->has($key)) {
//                $url = $this->container->get('session')->get($key);
//                $this->container->get('session')->remove($key);
//            } else {
//                $url = $this->generateUrl('tradeserve_account_index', $params);
//            }
//
//            return $this->redirect($url);
//            /*return $this->render('TradeServeAppBundle:User:index.html.twig', array(
//                'form'       => $form->createView(),
//                'error'      => $error,
//                'navigation' => $navigation,
//                'page_title' => 'Dashboard',
//            ));*/
//        }
//        $error = array('message' => 'form was invalid');
//        $navigation = array();
//
//        return $this->render('TradeServeAppBundle:Security:register.html.twig', array(
//           'form'       => $form->createView(),
//           'error'      => $error,
//           'navigation' => $navigation,
//           'page_title' => 'Register',
//        ));
//    }
//
//    private function authenticateUser(User $user)
//    {
//        $providerKey = 'secured_area'; // your firewall name
//        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
//
//        $this->container->get('security.context')->setToken($token);
//    }
//
//    /**
//     *  Create the password reset form
//     */
//    public function createPasswordResetForm($token)
//    {
//        $params = array('passwordToken' => $token);
//
//        return $this->createFormBuilder(null, array('attr' => array('class' => 'password-reset-form')))
//           ->setAction($this->generateUrl('remind_cloud_user_password_reset', $params))
//           ->setMethod('POST')
//           ->add('password', 'password', array(
//              'label'    => 'New Password',
//              'required' => true,
//           ))
//           ->add('password_confirm', 'password', array(
//              'label'    => 'Confirm New Password',
//              'required' => true,
//           ))
//           ->add('submit', 'submit', array(
//              'label' => 'Reset password',
//              'attr'  => array('class' => 'btn btn-primary'),
//           ))
//           ->getForm();
//    }
//
//    public function passwordForgotAction(Request $request)
//    {
//        $error = array();
//        $form = $this->createPasswordForgotForm();
//        $form->handleRequest($request);
//        $message = array();
//
//        if ($form->isValid()) {
//
//            $data = $form->getViewData();
//            $email = Email::sanitize($data['email']);
//
//            $em = $this->getDoctrine()->getManager();
//            $user = $em->getRepository('TradeServeAppBundle:User')->findOneBy(array('email' => $email));
//
//            if ($user) {
//
//                $user->setPasswordToken();
//                $em->persist($user);
//                $em->flush();
//
//                $token = $user->getPasswordToken();
//
//                $params = array(
//                   'passwordToken' => $token,
//                );
//
//                $host = $this->get('router')->getContext()->getHost();
//                $route = $this->generateUrl('tradeserve_user_password_reset', $params);
//                $url = 'https://' . $host . $route;
//
//                $body = sprintf("<p>Hi %s,</p></br>
//
//                            <p>You have recently requested your TradeServe account password be reset. If you did not request your password to be reset, please ignore this email.
//
//                            To complete this request please visit the following URL: %s</p>", $user->getFirstName(),
//                   $url);
//
//                $params = array(
//                   'type'       => 'password-reset',
//                   'name'       => $user->getFirstName(),
//                   'recipients' => array($user->getEmail()),
//                   'from'       => array(
//                      'email' => $this->container->getParameter('tradeserve_support_email'),
//                      'name'  => $this->container->getParameter('tradeserve_support_email_name')
//                   ),
//                   'body'       => $body,
//                   'subject'    => 'Reset Your TradeServe Password',
//                   'email'      => $user->getEmail(),
//                   'url'        => $url,
//                   'html'       => true
//                );
//
//                $mailer = $this->container->get('tradeserve_mailer');
//
//                try {
//                    $mailer->sendTransactionalEmail($params);
//                } catch (\Exception $e) {
//                    //do something
//                }
//
//                $message = array(
//                   'type' => 'success',
//                   'body' => sprintf('%s has been sent password reset instructions.', $email),
//                );
//            } else {
//                $message = array(
//                   'type' => 'danger',
//                   'body' => sprintf('No user found for email %s', $email),
//                );
//            }
//        }
//
//        return $this->render('TradeServeAppBundle:Security:forgot.html.twig', array(
//           'message'     => $message,
//           'error'       => $error,
//           'forgot_form' => $form->createView(),
//        ));
//    }
//
//    public function createPasswordForgotForm()
//    {
//        return $this->createFormBuilder(null, array('attr' => array('class' => 'password-forgot-form')))
//           ->setAction($this->generateUrl('tradeserve_user_password_forgot'))
//           ->setMethod('POST')
//           ->add('email', 'email', array(
//              'label'    => 'Email Address',
//              'required' => true,
//              'attr'     => array(
//                 'class'       => 'form-control',
//                 'placeholder' => 'Enter Your Email Address'
//              ),
//           ))
//           ->add('submit', 'submit', array(
//              'label' => 'Send',
//              'attr'  => array('class' => 'btn btn-primary'),
//           ))
//           ->add('back', 'button', array(
//              'label' => 'Go back',
//              'attr'  => array('class' => 'btn btn-default'),
//           ))
//           ->getForm();
//    }
//
//    /**
//     *  Allow a user to reset his/her password
//     */
//    public function passwordResetAction(Request $request)
//    {
//        $message = array();
//        $token = $request->get('passwordToken');
//        $form = $this->createPasswordResetForm($token);
//        $form->handleRequest($request);
//
//        $valid_token = false;
//
//        /**
//         *  Fetch the user with this token and see if the token
//         *  has expired or not..
//         */
//        $em = $this->getDoctrine()->getManager();
//        $user = $em->getRepository('TradeServeAppBundle:User')->findOneBy(array('password_token' => $token));
//
//        if (!$user) {
//            $message = array(
//               'type' => 'danger',
//               'body' => sprintf('Could not find a valid user token, please enter your email address to create another.'),
//            );
//        } else {
//            /**
//             *  Get the token expiration
//             */
//            $valid_token = $user->passwordTokenExpired() ? false : true;
//
//            if ($form->isValid()) {
//
//                $data = $form->getViewData();
//
//                if ($data['password'] === $data['password_confirm']) {
//
//                    $password = $data['password_confirm'];
//
//                    /**
//                     *  Get the password encoder
//                     */
//                    $factory = $this->get('security.encoder_factory');
//                    $user->setPassword($factory->getEncoder($user)->encodePassword($password, $user->getSalt()));
//                    $em->persist($user);
//                    $em->flush();
//
//                    $this->get('session')->getFlashBag()->add('success', 'Password changed successfully!');
//
//                    /**
//                     *  Redirect the user to the login page
//                     */
//
//                    return $this->redirect($this->generateUrl('login'));
//                } else {
//                    $message = array(
//                       'type' => 'danger',
//                       'body' => sprintf('Please make sure you confirm your password twice.'),
//                    );
//                }
//            }
//        }
//
//        return $this->render('TradeServeAppBundle:Security:password_reset.html.twig', array(
//           'message'             => $message,
//           'valid_token'         => $valid_token,
//           'password_reset_form' => $form->createView(),
//           'resend_form'         => $this->createPasswordForgotForm()->createView(),
//        ));
//    }
//
//    /**
//     * @Route("/login_check", name="login_check")
//     */
//    public function loginCheckAction()
//    {
//        // this controller will not be executed,
//        // as the route is handled by the Security system
//    }
}
