<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/8/15
 * Time: 6:52 PM
 */

namespace TradeServe\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use TradeServe\AppBundle\Common\Common as Common;
use FOS\RestBundle\Controller\FOSRestController;

class TradeServeController extends FOSRestController
{

    /**
     * See if user is able to view this section of the application
     */
    public function enforceUserSecurity()
    {

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }


//        $securityContext = $this->container->get('security.context');
//        if (!$securityContext->isGranted('ROLE_USER')) {
//            throw new AccessDeniedException('Need ROLE_USER!');
//        }
    }

    /**
     *  Load the Currently authenticated User
     *
     * @return $user TradeServe\CoreBundle\Entity\User
     */
    public function currentUser()
    {
        return $this->get('security.context')->getToken()->getUser();
    }


    /**
     *  Check for maintenance mode
     */
    public function maintenanceMode()
    {
        $env = $this->container->get('kernel')->getEnvironment();
        if ($env === 'maintenance') {
            return true;
        }

        return false;
    }

    /**
     * Check for test mode
     */
    public function test_mode()
    {
        $env = $this->container->get('kernel')->getEnvironment();

        if ($env == 'prod') {
            $test_mode = false;
        } else {
            $test_mode = true;
        }

        return $test_mode;
    }
}