;(function() {
  'use strict';


  function DispatchFactory ( $http ) {

      var getServiceRequests,
          getSchedule;

      // getServiceRequests = function() {
      //     // $http() returns a $promise that we can add handlers with .then()
      //     return $http({
      //         method: 'GET',
      //         url: 'http://tradeserve.dev/api/v1/1/call/service_request'
      //      });
      //  };

      getServiceRequests = [
            {customer: {address: "4002 Beechwood Dr. Columbia, KS 02135", email: null, first_name: "Peter", home_phone: null, last_name: "Petrelli", mobile_phone: "2345768903"}, estimated_start_time:'1/22/15 10:00'},
            {customer: {address: "123 E Main. Columbia, KS 02135", email: null, first_name: "Erica", home_phone: null, last_name: "Kravit", mobile_phone: "123456781"}, estimated_start_time:'1/22/15 13:00'},
            {customer: {address: "3299 Silversmith Rd. Columbia, KS 02135", email: null, first_name: "Monica", home_phone: null, last_name: "Lewis", mobile_phone: "9829340820"}, estimated_start_time:'1/22/15 16:00'},
            {customer: {address: "111 Millwood. Columbia, KS 02135", email: null, first_name: "Tommy", home_phone: null, last_name: "Lang", mobile_phone: "0812098221"}, estimated_start_time:'1/22/15 17:00'},
            {customer: {address: "24 Rivers Edge. Columbia, KS 02135", email: null, first_name: "Hiro", home_phone: null, last_name: "Nakamora", mobile_phone: "0829038420"}, estimated_start_time:'1/22/15 08:00'},
        ];

       getSchedule =  [
            {name: 'Hutch White', schedules:[{start_date:'1/17/2015 8:00', time: '8', client_name: 'Jane Jones'}]},
            {name: 'Jake Litwicki', schedules:[{start_date:'1/17/2015 8:00', time: '9', client_name: 'Jane Jones'}]},
            {name: 'Josh Stevens', schedules:[{start_date:'1/17/2015 8:00', time: '13', client_name: 'Jane Jones'}]}
          ];

       return {
        getServiceRequests: getServiceRequests,
        getSchedule: getSchedule
       }

  };

  angular
    .module( 'tradeServe.dispatchFactory', [
    ])
    .factory( 'DispatchFactory', DispatchFactory )
    ;

})();
