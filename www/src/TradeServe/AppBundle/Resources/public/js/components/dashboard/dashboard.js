;(function() {
  'use strict';

  function configure ( $routeProvider ) {

    $routeProvider.when('/app/dashboard', {
      templateUrl: 'dashboard/dashboard.html',
      controller: 'DashboardCtrl'
    });      

  }


  function DashboardCtrl ( ) {

  }

  angular
    .module( 'tradeServe.dashboard', [
      'ngRoute'
    ])
    .config( configure )
    .controller( 'DashboardCtrl', DashboardCtrl )
    ;

})();
