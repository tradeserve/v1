;(function() {
  'use strict';

  function configure ( $routeProvider ) {

    $routeProvider.when('/app/dispatch', {
      templateUrl: 'dispatch/dispatchBoard.html',
      controller: 'DispatchCtrl'
    });

  };


  function DispatchCtrl ( $scope, $http, DispatchFactory ) {

      var init,
          addToSchedule;

      $scope.viewDate = new Date();

      init = function() {

        // getting customers
        // DispatchFactory.getServiceRequests().then(function(result) {
        //   $scope.serviceRequests = result.data.service_request;
        // });
        $scope.serviceRequests = DispatchFactory.getServiceRequests;

        // getting schedule
        $scope.schedule = DispatchFactory.getSchedule;

      };

      init();

      $scope.addToSchedule = function(sr) {
        console.log('adding...');
        $scope.schedule.push(
          {name: 'Hutch White', schedules:[{start_date:'1/22/2015 10:00', time: '10', client_name: 'Jane Jones'}]}
        );
      };

  };

  angular
    .module( 'tradeServe.dispatch', [
      'tradeServe.dispatchFactory',
      'ngRoute',
      'angular.filter'
    ])
    .config( configure )
    .controller( 'DispatchCtrl', DispatchCtrl )
    ;

})();
