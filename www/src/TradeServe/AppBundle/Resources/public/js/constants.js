// jscs:disable
;(function() {
  'use strict';

  angular.module( 'tradeServe.Constants', [] )
    .constant( 'Constants', {
      appName: 'Trade Serve',
      version: '0.1.1',
      apiURL: 'http://tradeserve.dev/api/v1/1/'
    })
  ;

})();
