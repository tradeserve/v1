;
(function () {

  var app = angular.module('tradeServe.services');

	app.factory('AppCRUD', function($http) {
		return {
			get: function(path, entity) {
				return $http.get(Constants.apiURL + path + '/' + entity);
			}
		}
	}); 

})();