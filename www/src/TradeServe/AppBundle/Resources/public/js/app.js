;(function() {
  'use strict';

  function configure( $routeProvider, $httpProvider, $locationProvider,
                      $logProvider, Constants ) {

    // // Default Routes
    $routeProvider
      .otherwise( '/app/dashboard/' );

    // HTML 5 Mode
    $locationProvider
      .html5Mode({
        enabled: true,
        requireBase: true
      });

  }


  var myApp = angular.module('myApp',[]);


    function AppCtrl( Constants, $scope, $window, $http ) {

      $scope.currentDate = new Date();

      //load tabs

      var init,
          getCustomers;

      getCustomers = function() {
          // $http() returns a $promise that we can add handlers with .then()
          return $http({
              method: 'GET',
              url: 'http://tradeserve.dev/api/v1/1/internal/customer'
           });
       };


      init = function() {

        // getting customers
        getCustomers().then(function(result) {
          $scope.customers = result.data.customer;
          console.log($scope.customers);
        });
      };

      init();    


  }

  angular
    .module( 'tradeServe', [
      'tradeServe.Constants',
      'tradeServe.dashboard',
      'tradeServe.dispatch',
      'tradeServe.services'
    ],
    function ($interpolateProvider) {
      $interpolateProvider.startSymbol('{[');
      $interpolateProvider.endSymbol(']}');
    }
  )
  .config( configure )
  .controller( 'AppCtrl', AppCtrl )
  ;

})();
