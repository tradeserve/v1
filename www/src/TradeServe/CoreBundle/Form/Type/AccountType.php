<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 8/13/14
 * Time: 3:57 PM
 */

namespace TradeServe\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use TradeServe\CoreBundle\Form\Type\EntitiesSelectorType;
use TradeServe\CoreBundle\Form\Type\EntitySelectorType;

class AccountType extends AbstractType
{

    /**
     * var Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Default constructor.
     *
     * @param EntityManager $em
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => TRUE))
            ->add('description', 'text', array('required' => TRUE))
            ->add('account_users', new EntitiesSelectorType($this->em->getRepository('TradeServeAppBundle:Account')), array('required' => FALSE));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TradeServe\CoreBundle\Entity\Account',
            'intention' => 'account',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'account_type';
    }

}