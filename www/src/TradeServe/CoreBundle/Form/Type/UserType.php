<?php
// src/Acme/AccountBundle/Form/Type/UserType.php
namespace TradeServe\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', ['attr' => ['placeholder' => 'First Name']]);
        $builder->add('lastName', 'text', ['attr' => ['placeholder' => 'Last Name']]);
        $builder->add('email', 'email', ['attr' => ['placeholder' => 'email@example.com']]);
        $builder->add('password', 'password', ['attr' => ['placeholder' => 'password']]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'TradeServe\CoreBundle\Entity\User'
            )
        );
    }

    public function getName()
    {
        return 'user';
    }
}