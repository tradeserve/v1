<?php

namespace remindcloud\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use remindcloud\AppBundle\Form\DataTransformer\EntityToIdTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of EntitiesSelectorType
 *
 * @author Michael
 */
class EntitiesSelectorType extends AbstractType
{

    /**
     * @var Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param Doctrine\ORM\EntityRepository $repository
     */
    public function __construct(\Doctrine\ORM\EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIdTransformer($this->repository);
        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $entities = $this->repository->findAll();
        foreach ($entities as $entity) {
            $choices[$entity->getId()] = method_exists($entity, 'getName') ? $entity->getName() : $entity->getId();
        }
        $defaults = array(
            'invalid_message' => sprintf('No %s entities found.', get_class($entity)),
            'choices' => $choices,
            'expanded' => FALSE,
            'multiple' => TRUE,
        );
        $resolver->setDefaults($defaults);
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'entities_selector';
    }

}
