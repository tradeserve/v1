<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 8/13/14
 * Time: 3:05 PM
 */

namespace TradeServe\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ApplicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('address', 'collection', array('type' => new AddressType()));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           'data_class' => 'TradeServe\CoreBundle\Entity\Application',
        ));
    }

    public function getName()
    {
        return 'application';
    }
} 