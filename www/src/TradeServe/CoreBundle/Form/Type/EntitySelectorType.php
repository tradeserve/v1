<?php

namespace TradeServe\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TradeServe\CoreBundle\Form\DataTransformer\EntityToIdTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of GroupSelectorType
 *
 * @author Michael
 */
class EntitySelectorType extends AbstractType
{

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param \Doctrine\ORM\EntityRepository $repository
     */
    public function __construct(\Doctrine\ORM\EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIdTransformer($this->repository);
        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           'invalid_message' => 'The selected group does not exist',
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'entity_selector';
    }

}
