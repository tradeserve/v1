<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 7/16/14
 * Time: 9:22 AM
 */

namespace TradeServe\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'firstName',
            'text',
            [
                'attr' => ['placeholder' => 'First Name', 'class' => 'form-control', 'name' => '_firstName'],
                'label' => false
            ]
        );
        $builder->add(
            'lastName',
            'text',
            [
                'attr' => ['placeholder' => 'Last Name', 'class' => 'form-control', 'name' => '_lastName'],
                'label' => false
            ]
        );
        $builder->add(
            'email',
            'email',
            ['attr' => ['placeholder' => 'Email', 'class' => 'form-control', 'name' => '_email'], 'label' => false]
        );
        $builder->add(
            'password',
            'password',
            [
                'attr' => ['placeholder' => 'Password', 'class' => 'form-control', 'name' => '_password'],
                'label' => false
            ]
        );
        $builder->add('Register', 'submit', ['attr' => ['class' => 'btn btn-primary block full-width m-b']]);
    }

    public function getName()
    {
        return 'registration';
    }
}