<?php

# src/TradeServe/AppBundle/Form/PhotoType.php

namespace TradeServe\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('photo', 'file', array(
            'label' => 'Photo',
        ));
    }

    public function getName()
    {
        return 'photo';
    }
}