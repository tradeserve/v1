<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/22/14
 * Time: 11:14 PM
 */

namespace TradeServe\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageLayoutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('account_id')
                ->add('message_type_id')
                ->add('contact_type_id')
                ->add('name')
                ->add('description')
                ->add('message');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TradeServe\CoreBundle\Entity\MessageLayout'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'remindcloud_appbundle_message_layout';
    }
} 