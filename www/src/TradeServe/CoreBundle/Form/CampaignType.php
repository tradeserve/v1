<?php

namespace TradeServe\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CampaignType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('account')
                ->add('name')
                ->add('description')
                ->add('send_all')
                ->add('track_opens')
                ->add('from_email')
                ->add('from_name')
                ->add('subject')
                ->add('send_date')
                ->add('published')
                ->add('created');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TradeServe\CoreBundle\Entity\Campaign'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'remindcloud_appbundle_campaign';
    }
}
