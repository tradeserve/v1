<?php

// src/TradeServe/UserBundle/Security/Authenticator/ApiKeyAuthenticator.php

namespace TradeServe\CoreBundle\Security\Authenticator;

use TradeServe\CoreBundle\Security\WebserviceUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserAuthenticator implements SimpleFormAuthenticatorInterface
{

    private $provider;
    private $encoderFactory;

    public function __construct(
      EncoderFactoryInterface $encoderFactory,
      WebserviceUserProvider $provider
    ) {
        $this->encoderFactory = $encoderFactory;
        $this->provider = $provider;
    }

    public function authenticateToken(
      TokenInterface $token,
      UserProviderInterface $userProvider,
      $providerKey
    ) {
        try {
            $user = $this->provider->loadUserByUsername($token->getUsername());
        } catch (UsernameNotFoundException $e) {
            throw new AuthenticationException(
              sprintf(
                'Unable to find an active user with email "%s"',
                $token->getUsername()
              )
            );
        }

        if ($user->getStatus() == 0) {
            throw new AuthenticationException(
              sprintf('User "%s" is currently disabled!', $token->getUsername())
            );
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $passwordValid = $encoder->isPasswordValid(
          $user->getPassword(),
          $token->getCredentials(),
          $user->getSalt()
        );

        //$user->setLastOnlineDate(new \DateTime('now'));
        if ($passwordValid) {
            return new UsernamePasswordToken(
              $user, $user->getPassword(), $providerKey, $user->getRoles()
            );
        }

        //@TODO: do we want to make this explicit or be vague on purpose?
        //Possibly return "Invalid username or password" so users don't know they guessed a proper username?
        throw new AuthenticationException(
          sprintf('Invalid password entered for "%s"', $token->getUsername())
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken && $token->getProviderKey(
        ) === $providerKey;
    }

    public function createToken(
      Request $request,
      $username,
      $password,
      $providerKey
    ) {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }
}
