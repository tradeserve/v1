<?php

// src/TradeServe/AppBundle/Security/Authenticator/ApiKeyAuthenticator.php

namespace TradeServe\CoreBundle\Security\Authenticator;

use TradeServe\CoreBundle\Security\WebserviceUserProvider;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\HttpFoundation\AcceptHeader;
use Symfony\Component\Security\Core\Authentication\SimpleFormAuthenticatorInterface;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface
{

    protected $provider;
    private $api_key_name;

    public function __construct(WebserviceUserProvider $provider, $api_key_name)
    {
        $this->provider = $provider;
        $this->api_key_name = $api_key_name;
    }

    public function createToken(Request $request, $providerKey)
    {
        if (!$request->headers->get($this->api_key_name)) {
            throw new BadCredentialsException('No API key found');
        }

        return new PreAuthenticatedToken(
          'anon.', $request->headers->get($this->api_key_name), $providerKey
        );
    }

    public function authenticateToken(
      TokenInterface $token,
      UserProviderInterface $userProvider,
      $providerKey
    ) {
        $apiKey = $token->getCredentials();
        $username = $this->provider->getUsernameForApiKey($apiKey);

        if (!$username) {
            throw new AuthenticationException(
              sprintf('API Key "%s" does not exist.', $apiKey)
            );
        }

        $user = $this->provider->loadUserByUsername($username);

        if ($user->getStatus() == 0) {
            throw new AuthenticationException(
              sprintf('API Key %s is currently disabled!', $apiKey)
            );
        }

        return new PreAuthenticatedToken(
          $user, $apiKey, $providerKey, $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey(
        ) === $providerKey;
    }
}
