<?php

// src/TradeServe/AppBundle/Security/Authenticator/TradeServeAuthenticator.php

namespace TradeServe\CoreBundle\Security\Authenticator;

use TradeServe\CoreBundle\Security\WebserviceUserProvider;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\HttpFoundation\AcceptHeader;
use Symfony\Component\Security\Core\Authentication\SimpleFormAuthenticatorInterface;

class TradeServeAuthenticator implements SimplePreAuthenticatorInterface
{

    protected $username;
    protected $password;

    public function __construct(
      WebserviceUserProvider $provider,
      $username,
      $password
    ) {
        $this->username = $username;
        $this->password = $password;
    }

    public function createToken(Request $request, $providerKey)
    {
        $username = $request->server->get('PHP_AUTH_USER');
        $password = $request->server->get('PHP_AUTH_PW');

        if ($username == $this->username && $password == $this->password) {
            return new PreAuthenticatedToken(
              $username, $password, $providerKey
            );
        } else {
            throw new BadCredentialsException(
              'Invalid TradeServe Webservice Credentials!'
            );
        }
    }

    public function authenticateToken(
      TokenInterface $token,
      UserProviderInterface $userProvider,
      $providerKey
    ) {
        return new PreAuthenticatedToken(
          $this->username, $this->password, $providerKey, array(
            'ROLE_USER',
            'ROLE_ADMIN',
          )
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey(
        ) === $providerKey;
    }
}
