<?php
namespace TradeServe\CoreBundle\Security\Authentication\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $router, $security;


    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(
      Request $request,
      TokenInterface $token
    ) {
        $session = $request->getSession();
        $key = '_security.secured_area.target_path'; #where "main" is your firewall name

        //check if the referer session key has been set
        if ($session->has($key)) {
            //set the url based on the link they were trying to access before being authenticated
            $url = $session->get($key);

            //remove the session key
            $session->remove($key);
            $response = new RedirectResponse($url);
        } //if the referer key was never set, redirect to a default route
        else {
            if ($this->security->isGranted(
                'ROLE_USER'
              ) && ($this->security->isGranted(
                  'ROLE_SUPER_ADMIN'
                ) || $this->security->isGranted(
                  'ROLE_OWNER'
                ) || $this->security->isGranted(
                  'ROLE_MANAGER'
                ) || $this->security->isGranted('ROLE_MEMBER'))
            ) {
                $url = 'TradeServe_corporate_index';
                $response = new RedirectResponse($this->router->generate($url));
            } elseif ($this->security->isGranted('ROLE_USER')) {
                $url = 'TradeServe_account_select';
                $response = new RedirectResponse($this->router->generate($url));
            } else {
                $url = $this->router->generate('TradeServe_account_campaigns');
                $response = new RedirectResponse($this->router->generate($url));
            }
        }

        return $response;
    }
}