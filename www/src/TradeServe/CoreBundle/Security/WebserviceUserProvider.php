<?php

// src/TradeServe/AppBundle/Security/User/WebserviceUserProvider.php

namespace TradeServe\CoreBundle\Security;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\EntityManager;

use TradeServe\CoreBundle\Entity\AccountUser;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class WebserviceUserProvider implements UserProviderInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     *  Load a User by APIKey, which is tied to the AccountUser
     *  Entity relationship for the User.
     *
     * @param $api_key string AccountUser.api_key (decrypted)
     *
     * @return $user TradeServe\CoreBundle\Entity\User
     */
    public function getUserForApiKey($key_raw)
    {

        $api_key = AccountUser::encryptParameter($key_raw);

        try {
            $accountUser = $this->repository->findOneBy(
              array('api_key' => $api_key)
            );
        } catch (NoResultException $e) {
            $message = sprintf(
              'Unable to find an active user with api key "%s".',
              $api_key
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $accountUser->getUser();
    }

    /**
     *  Load a user by their username or email
     *  This allows users to enter either their username or email address as
     *  both are unique when they want to login to the application.
     *
     *  TradeServe "username" is an alias displayed internally to the
     *  application only. For authentication purposes, the user's email is the
     *  only unique identifier globally throughout all of TradeServe.com and
     *  therefore is the true USERNAME.
     *
     * @param $value string
     *
     * @return $user TradeServe\CoreBundle\Entity\User
     *
     */
    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('TradeServeAppBundle:User')->findOneBy(
          array('username' => $username)
        );

        if (!$user) {
            $message = sprintf(
              'Error loading %s from TradeServeAppBundle:User',
              $username
            );
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    /**
     *  Reload a user Entity
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
              sprintf('Instances of "%s" are not supported.', $class)
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return 'TradeServe\CoreBundle\Entity\User' === $class;
    }
}
