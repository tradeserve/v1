<?php

// src/TradeServe/AppBundle/Security/User/WebserviceUser.php

namespace TradeServe\CoreBundle\Security;

use TradeServe\CoreBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\ORM\EntityManager;

/**
 * @ExclusionPolicy("all")
 */
class WebserviceUser extends User implements EquatableInterface
{

    protected $entityManager;

    public function __construct(
      $username,
      $password,
      $salt,
      array $roles,
      EntityManager $entityManager
    ) {
        $this->em = $entityManager;
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            return false;
        }

        if ($this->getPassword() !== $user->getPassword()) {
            return false;
        }

        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }

        if ($this->getUsername() !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}
