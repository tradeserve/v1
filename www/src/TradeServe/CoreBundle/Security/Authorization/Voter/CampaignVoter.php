<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/26/14
 * Time: 1:56 PM
 */

namespace TradeServe\CoreBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use TradeServe\CoreBundle\Entity\User;
use TradeServe\CoreBundle\Entity\Campaign;

class CampaignVoter extends ContainerAware implements VoterInterface
{

    /**
     * Allows view access to members belonging to the account, and edit to
     * account admins.
     *
     * @param User $user
     * @param Campaign $campaign
     * @param string $attribute
     *
     * @return int
     */
    private function checkAccess(User $user, Campaign $campaign, $attribute)
    {
        // All associated users can view

        $accounts = $user->getAccounts();
        foreach ($accounts as $account) {
            foreach ($account->getCampaigns() as $campaigns) {
                if ($campaigns->getId() == $campaign->getId()) {
                    return VoterInterface::ACCESS_GRANTED;
                }
            }
        }
        /*$account = $campaign->getAccount();
        $users = $account->getUsers();
        foreach ($users as $usr) {
            if ($usr->getId() == $user->getId()) {
                return VoterInterface::ACCESS_GRANTED;
            }
        }*/

        // All else denied
        return VoterInterface::ACCESS_DENIED;
    }

    const CREATE = 'create';
    const VIEW   = 'view';
    const EDIT   = 'edit';
    const DELETE = 'delete';

    /**
     * Returns true if the attribute matches known attributes.
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return in_array(
          $attribute,
          array(
            self::CREATE,
            self::VIEW,
            self::EDIT,
            self::DELETE,
          )
        );
    }

    /**
     * Returns true of object is an instance of Account.
     *
     * @param object $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class instanceof Campaign;
    }

    /**
     * Returns if the user should have access to the entity.
     *
     * @param TokenInterface $token
     * @param object $entity
     * @param array $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $entity, array $attributes)
    {

        if (!$this->supportsClass($entity)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        if (1 !== count($attributes)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $attribute = $attributes[0];

        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();

        return $this->checkAccess($user, $entity, $attribute);
    }

    public function __construct($container)
    {
        $this->container = $container;
    }
}
