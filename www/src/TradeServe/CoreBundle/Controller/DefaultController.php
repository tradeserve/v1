<?php

namespace TradeServe\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TradeServeCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
