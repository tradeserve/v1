<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 8:40 PM
 */

namespace TradeServe\CoreBundle\Exception;


class InvalidFormException extends \RuntimeException
{
    protected $form;

    public function __construct($message, $form = null)
    {
        parent::__construct($message);
        $this->form = $form;
    }

    /**
     * @return array|null
     */
    public function getForm()
    {
        return $this->form;
    }
}