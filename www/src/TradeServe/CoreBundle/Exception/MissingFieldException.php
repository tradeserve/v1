<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 8:10 PM
 */

namespace TradeServe\CoreBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class MissingFieldException extends HttpException
{
    /**
     * Constructor.
     *
     * @param string $message The internal exception message
     * @param \Exception $previous The previous exception
     * @param int $code The internal exception code
     */
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct(500, $message, $previous, array(), $code);
    }
}
