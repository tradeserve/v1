<?php

namespace TradeServe\CoreBundle\EventListener;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SupportLogoutListener implements LogoutSuccessHandlerInterface
{

    private $securityContext;
    private $impersonationHandler;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     *  On Logout, terminate all Impersonation Sessions for this User.
     */
    public function onLogoutSuccess(Request $request)
    {
        $token = $this->securityContext->getToken();
        if (!$token) {
            return new RedirectResponse('/');
        }
        $user = $token->getUser();
        //$message = sprintf('Session terminated by logout session.');
        //$this->impersonationHandler->endUser($user, $message, $redirect = false);

        return new RedirectResponse('/');
    }
}