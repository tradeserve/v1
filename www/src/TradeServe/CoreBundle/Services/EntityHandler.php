<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 8:01 PM
 */

namespace TradeServe\CoreBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\FormFactoryInterface;
use TradeServe\CoreBundle\Common\Common;
use TradeServe\CoreBundle\Handler\TradeServeHandler;
use TradeServe\CoreBundle\Model\HandlerInterface;
use TradeServe\CoreBundle\Model\EntityInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Form\Exception\FormException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Exception\MissingFieldException;

class EntityHandler extends TradeServeHandler implements HandlerInterface
{
    protected $om;

    protected $entityClass;

    protected $repository;

    protected $formFactory;

    protected $encoder;

    protected $security_context;

    public function __construct(Container $container, ObjectManager $om, $entityClass)
    {
        $this->container = $container;
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $container->get('form.factory');
        $this->encoder = $container->get('security.encoder_factory');
        $this->security_context = $container->get('security.context');

    }

    /**
     * Check if the $data updates being applied to the entity are valid by calling the function specified
     * for $validation (may be either a string or anonymous function).
     *
     * @param $entity
     * @param array $data
     * @param string|Closure $validation
     */
    public function validate($entity, $data, $validation)
    {
        if (is_string($validation)) {
            $this->$validation($entity, $data);
        }
        if ($validation instanceof \Closure) {
            $validation($entity, $data);
        }
    }

    /**
     * Checks if the entity has the specified operational accessor for the property (ie. growth_case => setOrganization)
     *
     * @param $entity
     * @param $key
     * @param string $op
     *
     * @return bool
     */
    public function checkAccessorExists($entity, $key, $op = 'set')
    {
        $name = $op . Common::underscoreToCamelCase($key, true);
        if (method_exists($entity, $name)) {
            return $name;
        }

        return false;
    }

    /**
     * Checks $data for all $keys and if set, checks for existence of $accessor, calling with $data[$key] if exists
     *
     * @param $entity
     * @param $data
     * @param $keys
     */
    public function callPropertySetters($entity, $data, $keys)
    {
        foreach ($keys as $key) {
            if (isset($data[$key]) && ($accessor = $this->checkAccessorExists($entity, $key))) {
                $entity->$accessor($data[$key]);
            }
        }
    }

    /**
     * Check $data for all $keys and if set, loads by id if numeric, then checks for existence of $accessor, calling
     * with $data[$key] if exists
     *
     * @param $entity
     * @param $data
     * @param $keys
     */
    public function callEntitySetters($entity, $data, $keys)
    {
        foreach ($keys as $key) {
            if (isset($data[$key]) && ($accessor = $this->checkAccessorExists($entity, $key))) {
//                if (is_numeric($data[$key])) {
//                    $entity->$accessor($this->container->get('trade_serve.' . str_replace('_', '',
//                            $key) . '.handler')->get($data[$key]));
//                } else {
                    $entity->$accessor($data[$key]);
                //}
            }
        }
    }

    /**
     * Checks the $entity for all fields
     *
     * @param $entity
     * @param $data
     * @param $keys
     *
     * @throws MissingFieldException
     */
    public function checkRequiredFields($entity, $data, $keys)
    {
        foreach ($keys as $key) {
            $accessor = $this->checkAccessorExists($entity, $key, 'get');
            if (($accessor && ($entity->$accessor() === null || $entity->$accessor() === '')) && (!isset($data[$key]) || $data[$key] === null || is_string($data[$key]) && $data[$key] === '')) {
                $class = get_class($entity);
                throw new MissingFieldException(substr($class,
                        strrpos($class, '\\') + 1) . ' requires a ' . str_replace('_', ' ', $key) . '.');
            }
        }
    }

    /**
     * Loads the related entity of type $class
     *
     * @param $entity
     * @param $data
     * @param $key
     * @param $class
     *
     * @return null
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Config\Definition\Exception\InvalidTypeException
     */
    public function checkRelatedEntity($entity, $data, $key, $class = false)
    {
        if (!$class) {
            $class = Common::underscoreToCamelCase($key, true);
        }

        // Fetch the related growth case
        if (isset($data[$key])) {
            if (is_numeric($data[$key])) {
                $output = $this->container->get('trade_serve.' . str_replace('_', '', $key) . '.handler')->get($data[$key]);
            } else {
                if (is_object($data[$key])) {
                    $output = $data[$key];
                } else {
                    throw new InvalidTypeException("Invalid type when loading $class.");
                }
            }
        } else {
            if ($accessor = $this->checkAccessorExists($entity, $key, 'get')) {
                $output = $entity->$accessor();
            } else {
                throw new NotFoundHttpException("Unable to find that $class.");
            }
        }

        $fullName = get_class($output);
        if (strpos($fullName, 'TradeServe')) {
            $fullName = strstr($fullName, 'TradeServe');
        }

        if ($fullName != $this->getFullyQualifiedClassName($class)) {
            //            $string = $class . ' : ' . get_class($output) . ' : ' . $fullName . ' : ' . $this->getFullyQualifiedClassName($class);
            //            die($string);
            throw new NotFoundHttpException("Invalid type while loading $class.");
        }

        return $output;
    }

    public function getFullyQualifiedClassName($class)
    {
        $classes = array(
//           'Organization'    => 'TradeServe\ApplicationModelBundle\Entity\Organization',
//           'Product'          => 'TradeServe\ApplicationModelBundle\Entity\Product',
//           'Survey'           => 'TradeServe\SurveyModelBundle\Entity\Survey',
//           'Factor'           => 'TradeServe\ApplicationModelBundle\Entity\Factor',
//           'FactorMap'        => 'TradeServe\ApplicationModelBundle\Entity\FactorMap',
//           'Question'         => 'TradeServe\SurveyModelBundle\Entity\Question',
//           'QuestionType'     => 'TradeServe\SurveyModelBundle\Entity\QuestionType',
//           'Member'           => 'TradeServe\ApplicationModelBundle\Entity\Member',
//           'Segment'          => 'TradeServe\ApplicationModelBundle\Entity\Segment',
//           'Option'           => 'TradeServe\SurveyModelBundle\Entity\Option',
//           'ApplicationEmail' => 'TradeServe\ApplicationModelBundle\Entity\ApplicationEmail',
//           'InitialEmail'     => 'TradeServe\ApplicationModelBundle\Entity\ApplicationEmail',
//           'ReminderEmail'    => 'TradeServe\ApplicationModelBundle\Entity\ApplicationEmail',
//           'SurveySection'    => 'TradeServe\SurveyModelBundle\Entity\SurveySection',
//           'TokenType'        => 'TradeServe\SurveyModelBundle\Entity\TokenType',
        );

        if (isset($classes[$class])) {
            return $classes[$class];
        }

        return '';
    }

    /**
     * Get a Entity.
     *
     * @param mixed $id
     *
     * @return EntityInterface
     */
    public function get($id)
    {
        if (is_numeric($id)) {
            $id = (int)$id;

            return $this->repository->find($id);
        }

        throw new InvalidTypeException('Invalid ID specified: must be integer value');
    }

    /**
     * @param $id
     *
     * @return \TradeServe\CoreBundle\Model\EntityInterface
     */
    public function find($id)
    {
        return $this->get($id);
    }

    /**
     * Get a Entity from doctrine using the array of parameters passed in.
     *
     * @param mixed $data
     *
     * @return EntityInterface
     */
    public function findOneBy($data)
    {
        if (is_array($data)) {
            return $this->repository->findOneBy($data);
        }

        throw new InvalidTypeException('Invalid data specified: findOneBy requires an array');
    }

    /**
     * Gets all Entities from doctrine using the array of parameters passed in.
     *
     * @param mixed $data
     *
     * @return EntityInterface
     */
    public function findBy($data)
    {
        if (is_array($data)) {
            return $this->repository->findBy($data);
        }

        throw new InvalidTypeException('Invalid data specified: findOneBy requires an array');
    }

    /**
     * Returns the entity by id as long as that entity is accessible by the provided growthcase
     *
     * @param $id
     * @param Organization $organization
     *
     * @return null|EntityInterface
     */
    public function findByOrganization($id, Organization $organization)
    {
        $entity = $this->get($id);

        if ($entity->getOrganizationId() == $organization->getId()) {
            return $entity;
        }

        return null;
    }

    /**
     * Get a list of Entities.
     *
     * @param int[] $ids the ids to return (returns all if null)
     *
     * @return array
     */
    public function all($ids = null, $size = null, $page = null)
    {

        $class = $this->entityClass;

        $query = "SELECT x FROM $class x";

        if (!is_null($ids)) {
            $ids = implode(',', $ids);
            $query = $query . " WHERE x.id IN ($ids)";
        }

        $query = $query . " ORDER BY x.id ASC";

        $query = $this->om->createQuery($query);
        if (!is_null($size) && !is_null($page)) {
            $query->setFirstResult($size * ($page - 1));
            $query->setMaxResults($size);
        }

        return $query->getResult();
    }

    /**
     * Returns all entities of this type related to the growth case
     *
     * @param Organization $organization
     *
     * @return array
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {
        $entities = $this->all();
        $items = array();

        foreach ($entities as $entity) {
            if ($entity->getOrganizationId() == $organization->getId()) {
                $items[] = $entity;
            }
        }

        return $items;
    }

    /**
     * Create a new Entity.
     *
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function post(array $parameters)
    {
        return $this->create($parameters);
    }

    /**
     * Edit a Entity.
     *
     * @param EntityInterface $entity
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function put(EntityInterface $entity, array $parameters)
    {
        return $this->edit($entity, $parameters);
    }

    /**
     * Delete a Entity.
     *
     * @param EntityInterface $entity
     */
    public function delete(EntityInterface $entity)
    {
        $this->om->remove($entity);
        $this->om->flush();
    }

    /**
     * Delete a Entity (this may sometimes lead to an archive rather than a true delete).
     *
     * @param EntityInterface $entity
     */
    public function remove(EntityInterface $entity)
    {
        $this->delete($entity);
    }

    /**
     * Partially update a Entity.
     *
     * @param EntityInterface $entity
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function patch(EntityInterface $entity, array $parameters)
    {
        return $this->processForm($entity, $parameters, 'PATCH');
    }

    /**
     * Processes the form.
     *
     * @param EntityInterface $entity
     * @param array $parameters
     * @param String $method
     *
     * @return EntityInterface
     *
     * @throws \Symfony\Component\Form\Exception\FormException
     */
    protected function processForm(EntityInterface $entity, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create($this->mapEntityToForm($this->entityClass), $entity,
            array('method' => $method));
        $form->submit($parameters);

        if ($form->isValid()) {

            $entity = $form->getData();
            switch ($method) {
                case 'POST':
                    if (!($this->security_context->isGranted('create', $entity))) {
                        throw new AccessDeniedException('new');
                    }
                    break;
                case 'PUT':
                    if (!($this->security_context->isGranted('edit', $entity))) {
                        throw new AccessDeniedException($entity->getId());
                    }
                    break;
            }
            $this->om->persist($entity);
            $this->om->flush($entity);

            return $entity;
        }

        throw new \Exception('Invalid or missing form data, verify the form fields for this Entity.');
    }

    /**
     * Returns a new object based on the entity parameter of constructor
     *
     * @return object
     */
    public function createEntity()
    {
        return new $this->entityClass();
    }

    /**
     * @param $entity
     *
     * @return mixed
     * @throws \Exception
     */
    protected function mapEntityToForm($entity)
    {

        $array = array(
//           'TradeServe\ApplicationModelBundle\Entity\Group'            => 'group_type',
//           'TradeServe\ApplicationModelBundle\Entity\Factor'           => 'factor_type',
//           'TradeServe\ApplicationModelBundle\Entity\FactorMap'        => 'factor_map_type',
//           'TradeServe\ApplicationModelBundle\Entity\FactorRating'     => 'factor_rating_type',
//           'TradeServe\ApplicationModelBundle\Entity\UberFactor'       => 'uber_factor_type',
//           'TradeServe\ApplicationModelBundle\Entity\Organization'     => 'growth_case_type',
//           'TradeServe\ApplicationModelBundle\Entity\Context'          => 'context_type',
//           'TradeServe\ApplicationModelBundle\Entity\Idea'             => 'idea_type',
//           'TradeServe\ApplicationModelBundle\Entity\Member'           => 'member_type',
//           'TradeServe\ApplicationModelBundle\Entity\Role'             => 'role_type',
//           'TradeServe\ApplicationModelBundle\Entity\Note'             => 'note_type',
//           'TradeServe\ApplicationModelBundle\Entity\Notification'     => 'notification_type',
//           'TradeServe\ApplicationModelBundle\Entity\NotificationType' => 'notification_type_type',
//           'TradeServe\ApplicationModelBundle\Entity\Subscription'     => 'subscription_type',
//           'TradeServe\ApplicationModelBundle\Entity\SubscriptionType' => 'subscription_type_type',
//           'TradeServe\ApplicationModelBundle\Entity\Participant'      => 'participant_type',
//           'TradeServe\ApplicationModelBundle\Entity\Survey'           => 'survey_type',
//           'TradeServe\ApplicationModelBundle\Entity\SurveyInstance'   => 'survey_instance_type',
//           'TradeServe\ApplicationModelBundle\Entity\SurveyQuestion'   => 'survey_question_type',
//           'TradeServe\ApplicationModelBundle\Entity\SurveyResponse'   => 'survey_response_type',
//           'TradeServe\AccountModelBundle\Entity\AccountType'          => 'account_type',
//           'TradeServe\AccountModelBundle\Entity\Help'                 => 'help_type',
//           'TradeServe\AccountModelBundle\Entity\HelpCategory'         => 'help_category_type',
//           'TradeServe\AccountModelBundle\Entity\HelpType'             => 'help_type_type',
//           'TradeServe\AccountModelBundle\Entity\User'                 => 'user_type',
        );

        if (array_key_exists($entity, $array)) {
            return $array[$entity];
        }

        throw new \Exception(sprintf('Invalid entity form type: %s ', $entity));

    }
}