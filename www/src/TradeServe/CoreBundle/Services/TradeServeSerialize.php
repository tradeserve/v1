<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 9:31 PM
 */

namespace TradeServe\CoreBundle\Services;


use JMS\Serializer\SerializationContext;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Common\Common;

class TradeServeSerialize
{

    private $serializer;

    public function __construct($serializer)
    {
        $this->serializer = $serializer;

    }

    /**
     *  Serialize an object and default the serialization Group
     *
     * @param $object the fully formed Entity
     * @param $format json|xml
     * @param $group  string the group to serialize as
     *
     * @return $string formatted string based upon specified $format
     */
    public function serialize($object, $format, $group = 'api')
    {
        $string = null;

        $string = $this->serializer->serialize(
           $object,
           $format,
           SerializationContext::create()->setGroups(array($group))->setSerializeNull(true)
        );

        return $string;

    }

    /**
     *  Deserialize an object
     *
     * @param $string
     * @param $format   string json|xml
     * @param $typeName full namespace to entity
     *
     * @return $data
     */
    public function deserialize($string, $format, $typeName)
    {
        $data = $this->serializer->deserialize($string, $typeName, $format);
        return $data;

    }
}