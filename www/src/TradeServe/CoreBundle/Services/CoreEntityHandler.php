<?php

/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 7:58 PM
 */


namespace TradeServe\CoreBundle\Services;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;

class CoreEntityHandler extends EntityHandler
{
    /**
     * @param $id
     * @param \TradeServe\CoreBundle\Entity\Organization $organization
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     * @throws \Exception
     */
    public function findByOrganization($id, Organization $organization)
    {
        throw new \Exception('Invalid method for Core entity: findByOrganization');
        return null;
    }

    /**
     * @param \TradeServe\CoreBundle\Entity\Organization $organization
     * @param bool $activeOnly
     *
     * @return array|null
     * @throws \Exception
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {
        throw new \Exception('Invalid method for Core entity: allByOrganization');
        return null;
    }

}
