<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 8:06 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\DependencyInjection\Container;
use TradeServe\CoreBundle\Entity\User;

/**
 * Class TradeServeHandler
 *
 * @package TradeServe\CoreBundle\Handler
 */
class TradeServeHandler
{

    /**
     * @var Container
     */
    protected $container;

    /**
     * Construct
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Retrieves the current user using the security context
     *
     * @return \TradeServe\CoreBundle\Entity\User|null
     */
    public function getCurrentUser()
    {
        $token = $this->container->get('security.context')->getToken();
        if ($token instanceof AbstractToken) {
            $user = $token->getUser();
            if ($user) {
                if ($user instanceof User) {
                    return $user;
                }
            }
        }
        return null;
    }
}