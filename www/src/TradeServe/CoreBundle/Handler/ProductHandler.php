<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 1/20/16
 * Time: 3:09 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Exception\DuplicateEntityException;
use TradeServe\CoreBundle\Exception\ReadOnlyFieldException;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\Product;

class ProductHandler extends EntityHandler
{
    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     * @param Organization $organization
     *
     * @return null TradeServe\CoreBundle\Model\EntityInterface
     */
    public function findByOrganization($id, Organization $organization)
    {
        $entity = $this->get($id);
        return $entity;
    }

    /**
     * Returns all entities of this type related to organization
     *
     * @param Organization $organization
     * @param bool $activeOnly
     *
     * @return array Doctrine\Common\Collections\Collection
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {
        return $organization->getProducts();
    }

    /**
     * Create a new product with the specified values.
     *
     * @param array $data
     * @param string|Closure $validate
     *
     * @return Product
     */
    public function create($data, $validate = 'validation')
    {
        $product = new Product();

        if (!isset($data['active'])) {
            $data['active'] = 1;
        }

        // Validate if requested, accepting string to local function OR anonymous function
        $this->validate($product, $data, $validate);

        // Pass to edit (no reason to duplicate logic)
        $product = $this->edit($product, $data, false);

        // Create activity log entry
        $label = $product->logCreate($this->getCurrentUser() ? $this->getCurrentUser()->getFullName() : null, $product);
        if ($label != '{null}') {

            $activity = array(
                'description' => $label,
                'member' => $this->getCurrentMember() ? $this->getCurrentMember() : null,
                'organization' => $product->logCase($product),
                'entity_id' => $product->getId() ? $product->getId() : 0,
                'entity_class' => get_class($product),
            );

            $this->container->get('trade_serve.activity.handler')->create($activity, true);
        }

        return $product;
    }

    /**
     * Edit an existing product with the specified values.
     *
     * @param Product $product
     * @param array $data
     * @param string|Closure $validate
     *
     * @return Product
     */
    public function edit(Product $product, $data, $validate = 'validation')
    {
        // Validate if requested, accepting string to local function OR anonymous function
        $this->validate($product, $data, $validate);

        // Create activity log entry
        if ($validate == true) {

            $newName = null;
            if (isset($data['name'])) {
                $newName = $data['name'];
                if (isset($data['company_name'])) {
                    $newName = $data['company_name'] . " " . $data['name'];
                }
            }

            $label = $product->logEdit($this->getCurrentUser() ? $this->getCurrentUser()->getFullName() : null, $product, $newName);

            if ($label != '{null}') {

                $activity = array(
                    'description' => $label,
                    'member' => $this->getCurrentMember() ? $this->getCurrentMember() : null,
                    'organization' => $product->logCase($product),
                    'entity_id' => $product->getId() ? $product->getId() : 0,
                    'entity_class' => get_class($product),
                );

                $this->container->get('trade_serve.activity.handler')->create($activity, true);
            }
        }

        // Call the standard setters with these property values, if they exist in $data
        $propertyKeys = array(
            'name',
            'company_name',
            'url',
            'status',
            'market_share',
            'price',
        );

        $this->callPropertySetters($product, $data, $propertyKeys);

        // Call the standard related entity setters with these entity values or ids, if they exist in $data
        $entityKeys = array(
            'organization',
        );

        $this->callEntitySetters($product, $data, $entityKeys);

        // Set self if updated
        if (isset($data['self'])) {
            // If setting self to TRUE, make sure all other products are FALSE
            if ($data['self']) {
                $case = $product->getOrganization();
                foreach ($case->getProducts() as $prod) {
                    if ($prod->getSelf()) {
                        $this->edit($prod, array('self' => false), false);
                    }
                }
            }
            $product->setSelf($data['self']);
        }

        // Save the product
        $this->om->persist($product);
        $this->om->flush();

        // Add or remove the product from the growth case if updated
        if (isset($data['active'])) {
            if ($data['active'] && !($product->getActive())) {
                foreach ($product->getOrganization()->getSurveys() as $survey) {
                    // Create survey product
                    $params = array(
                        'survey' => $survey,
                        'product' => $product,
                    );
                    $sp = $this->container->get('trade_serve.surveyproduct.handler')->create($params);
                    $product->addSurveyProduct($sp);
                    break;
                }
            } else if (!($data['active']) && $product->getActive()) {
                foreach ($product->getSurveyProducts() as $surveyProduct) {
                    // Remove survey product
                    $product->removeSurveyProduct($surveyProduct);
                    $this->container->get('trade_serve.surveyproduct.handler')->remove($surveyProduct);
                }
            }
        }

        return $product;
    }

    /**
     * Validates the changes specified by the $data array to the Product
     *
     * @param Product $product
     * @param $data
     *
     * @throws ReadOnlyFieldException
     * @throws DuplicateEntityException
     */
    public function validation(Product $product, $data)
    {
        // Check all required fields were either already set or are being set
        $requiredKeys = array(
            'name',
            'organization',
        );

        $this->checkRequiredFields($product, $data, $requiredKeys);

        // Fetch the related growth case
        $case = $this->checkRelatedEntity($product, $data, 'organization');

        // Product may not be assigned to a new case
        if ($product->getOrganization() && $product->getSerializedOrganizationId() != $case->getId()) {
            throw new ReadOnlyFieldException('Attempting to update the read only Organization on a Product.');
        }

        // Product full name must be unique to the growth case
        $companyName = $product->getCompanyName();
        if (isset($data['company_name'])) {
            $companyName = $data['company_name'];
        }

        $productName = $product->getName();
        if (isset($data['name'])) {
            $productName = $data['name'];
        }

        $name = Product::compareName($companyName . ' ' . $productName);
        foreach ($case->getProducts() as $prod) {
            if ($prod->getComparisonName() == $name && $product->getId() != $prod->getId()) {
                throw new DuplicateEntityException('A product with that name already exists.');
            }
        }

        // Validate the surveyproduct that might be created as a result
        if (isset($data['active']) && $data['active'] && $product->getId() && !($product->getActive())) {
            foreach ($product->getOrganization()->getSurveys() as $survey) {
                // Create survey factor
                $params = array(
                    'survey' => $survey,
                    'product' => $product,
                );
                $sp = new SurveyProduct();
                try {
                    $this->container->get('trade_serve.surveyproduct.handler')->validation($sp, $params);
                } catch (DuplicateEntityException $e) {
                    foreach ($product->getSurveyProducts() as $sp) {
                        if ($sp->getSurvey()->getId() == $survey->getId()) {
                            $this->container->get('trade_serve.surveyproduct.handler')->remove($sp);
                        }
                    }
                }
                break;
            }
        }
    }


}