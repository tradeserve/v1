<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/15/15
 * Time: 6:57 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Entity\DispatchType;
use TradeServe\CoreBundle\Entity\ServiceRequest;
use TradeServe\CoreBundle\Entity\Technician;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\Dispatch;
use TradeServe\CoreBundle\Entity\User;
use DateTime;

class DispatchHandler extends EntityHandler
{
    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     * @param Organization $organization
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function findByOrganization($id, Organization $organization)
    {
        $entity = $this->get($id);
        return $entity;
    }

    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function find($id)
    {
        $entity = $this->get($id);
        return $entity;
    }

    /**
     * Returns all entities of this type related to organization
     *
     * @param Organization $organization
     * @param bool $activeOnly
     *
     * @return array|\Doctrine\Common\Collections\Collection
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {

        $items = array();
        $serviceRequests = $organization->getServiceRequests();

        foreach ($serviceRequests as $serviceRequest) {
            $items[] = $serviceRequest->getDispatches();
        }

        return $items;

    }

    /**
     * Create a new factor with the specified values.
     *
     * @param array $data
     *
     * @return Dispatch
     */
    public function create($data)
    {
        $dispatch = new Dispatch();

        if (isset($data['service_request']) && isset($data['technician']) && isset($data['organization'])) {

            $service_request = $this->om->getRepository('TradeServe\CoreBundle\Entity\ServiceRequest')->find($data['service_request']);
            if ($service_request instanceof ServiceRequest) {
                $data['service_request'] = $service_request;
            }

            $propertyKeys = array(
                'created',
                'updated',
                'primary_tech',
                'scheduled_time',
                'scheduled_date'
            );
            $incoming_date = $data['scheduled_date'];
            $data['scheduled_date'] = new DateTime($incoming_date);
            $data['scheduled_time'] = new DateTime($incoming_date);

            $this->callPropertySetters($dispatch, $data, $propertyKeys);


            // Call the standard related entity setters with these entity values or ids, if they exist in $data
            $entityKeys = array(
                'type',
                'technician',
                'service_request',
                'organization'
            );


            if (isset($data['type'])) {
                $type = $this->om->getRepository('TradeServe\CoreBundle\Entity\DispatchType')->find($data['type']);
                if ($type instanceof DispatchType) {
                    $data['type'] = $type;
                }
            } else {
                $type = $this->om->getRepository('TradeServe\CoreBundle\Entity\DispatchType')->find(4);
                if ($type instanceof DispatchType) {
                    $data['type'] = $type;
                }
            }
            if (isset($data['technician'])) {
                $technician = $this->om->getRepository('TradeServe\CoreBundle\Entity\Technician')->find($data['technician']);
                if ($technician instanceof Technician) {
                    $data['technician'] = $technician;
                }
            }
            if (isset($data['organization'])) {
                $organization = $this->om->getRepository('TradeServe\CoreBundle\Entity\Organization')->findOneBy(array('id' => (string)$data['organization']));
                if ($organization instanceof Organization) {
                    $data['organization'] = $organization;
                }
            }


            //'format' => 'yyyy/MM/dd HH:mm'


            $this->callEntitySetters($dispatch, $data, $entityKeys);
            // Save the dispatch
            $this->om->persist($dispatch);
            $this->om->flush();


            $label = $this->getCurrentUser() ? $this->getCurrentUser()->getFullName() : null . ' created a DISPATCH for SERVICE REQUEST: ' . $service_request->getId();

            $activity = array(
                'description' => $label,
                'user' => $this->getCurrentUser() ? $this->getCurrentUser() : null,
                'organization' => $organization,
                'entity_id' => $dispatch->getId(),
                'entity_class' => get_class($dispatch),
            );

            $this->container->get('trade_serve.activity.handler')->create($activity, true);

        }
        return $dispatch;

    }

    /**
     * Edit an existing factor with the specified values.
     *
     * @param Dispatch $dispatch
     * @param array $data
     *
     * @return Dispatch
     */
    public function edit(Dispatch $dispatch, $data)
    {

        $newName = null;
//        $label = $dispatch->logEdit($this->getCurrentUser() ? $this->getCurrentUser()->getFullName() : null, 'dispatch',
//            $newName);
//
//
//        $activity = array(
//            'description' => $label,
//            'user' => $this->getCurrentUser() ? $this->getCurrentUser() : null,
//            'organization' => $dispatch->logOrganization($dispatch),
//            'entity_id' => $dispatch->getId(),
//            'entity_class' => get_class($dispatch),
//        );
//        foreach ($data[0] as $key => $value) {
//            $data[$key] = $value;
//        }
//        unset($data[0]);
//
//        $this->container->get('trade_serve.activity.handler')->create($activity, true);
        $propertyKeys = array('created', 'updated', 'primary_tech');
        $data['updated'] = new \DateTime();

//        print_r($data);
//        die();

        $this->callPropertySetters($dispatch, $data, $propertyKeys);
        //}

//
//
//        // Call the standard setters with these property values, if they exist in $data
//        $propertyKeys = array(
//            'create_date',
//            'time_frame',
//            'time_frame_label',
//            'growth_clause',
//            'growth_dimension',
//            'growth_value',
//            'growth_value_label',
//            'market_segment',
//        );

//
//
//        // Call the standard related entity setters with these entity values or ids, if they exist in $data
//        $entityKeys = array(
//            'growth_case',
//        );
//
//        $this->callEntitySetters($context, $data, $entityKeys);
//
//        // Save the context
//        $this->om->persist($context);
//        $this->om->flush();
//
//        return $context;


        // Call the standard setters with these property values, if they exist in $data
//        $propertyKeys = array(
//            'name',
//            'body',
//            'selected',
//            'status',
//        );
//
//        $this->callPropertySetters($dispatch, $data, $propertyKeys);

        // Call the standard related entity setters with these entity values or ids, if they exist in $data
        $entityKeys = array(
            'type',
        );

        if (isset($data['type'])) {
            $type = $this->om->getRepository('TradeServe\CoreBundle\Entity\DispatchType')->find($data['type']);
            if ($type instanceof DispatchType) {
                $data['type'] = $type;
            }
        }


        $this->callEntitySetters($dispatch, $data, $entityKeys);

        // Save the factor
        $this->om->persist($dispatch);
        $this->om->flush();

        return $dispatch;
    }
}