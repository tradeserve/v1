<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 1/20/16
 * Time: 8:15 PM
 */

namespace TradeServe\CoreBundle\Handler;

use TradeServe\CoreBundle\Entity\Activity;
use TradeServe\CoreBundle\Services\EntityHandler;

/**
 * Class ActivityHandler
 *
 * @package Vennli\ApplicationModelBundle\Handler
 */
class ActivityHandler extends EntityHandler
{
    /**
     * Create a new Activity with the specified values.
     *
     * @param array $data
     * @param string|Closure $validate
     *
     * @return Activity
     */
    public function create($data, $validate = 'validation')
    {
        $activity = new Activity();

        // Validate if requested, accepting string to local function OR anonymous function
        $this->validate($activity, $data, $validate);

        // Pass to edit (no reason to duplicate logic)
        $activity = $this->edit($activity, $data, false);

        return $activity;
    }

    /**
     * Edit an existing Activity with the specified values.
     *
     * @param Activity $activity
     * @param array $data
     * @param string|Closure $validate
     *
     * @return Activity
     */
    public function edit(Activity $activity, $data, $validate = 'validation')
    {
        // Validate if requested, accepting string to local function OR anonymous function
        $this->validate($activity, $data, $validate);

        $data['activity_date'] = new \DateTime();

        // Call the standard setters with these property values, if they exist in $data
        $propertyKeys = array(
            'activity_date',
            'description',
            'entity_id',
            'entity_class',
        );

        $this->callPropertySetters($activity, $data, $propertyKeys);

        // Call the standard related entity setters with these entity values or ids, if they exist in $data
        $entityKeys = array(
            'organization',
            'user',
        );

        $this->callEntitySetters($activity, $data, $entityKeys);

        // Save the activity
        $this->om->persist($activity);
        $this->om->flush();

        return $activity;
    }

    /**
     * Validates the changes specified by the $data array to the Activity
     *
     * @param Activity $activity
     * @param $data
     */
    public function validation(Activity $activity, $data)
    {
        // Check all required fields were either already set or are being set
        $requiredKeys = array(
            'activity_date',
            'description',
        );

        $this->checkRequiredFields($activity, $data, $requiredKeys);

    }
}