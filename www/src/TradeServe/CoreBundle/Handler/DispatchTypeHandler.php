<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 1/20/16
 * Time: 10:56 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\Dispatch;
use TradeServe\CoreBundle\Entity\User;

class DispatchTypeHandler extends EntityHandler
{
    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function find($id)
    {
        $entity = $this->get($id);
        return $entity;
    }
}