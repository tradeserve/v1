<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 1/20/16
 * Time: 11:16 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Entity\DispatchType;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\Dispatch;
use TradeServe\CoreBundle\Entity\User;

class TechnicianHandler extends EntityHandler
{
    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     * @param Organization $organization
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function findByOrganization($id, Organization $organization)
    {
        $entity = $this->get($id);

        $technicians = $organization->getTechnicians();

        foreach ($technicians as $technician) {
            if ($technician->getId() == $entity->getId()) {
                return $entity;
            }
        }

        return null;
    }

    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function find($id)
    {
        $entity = $this->get($id);
        return $entity;
    }

    /**
     * Returns all entities of this type related to organization
     *
     * @param Organization $organization
     * @param bool $activeOnly
     *
     * @return array|\Doctrine\Common\Collections\Collection
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {
        $technicians = $organization->getTechnicians();

        return $technicians;

    }
}