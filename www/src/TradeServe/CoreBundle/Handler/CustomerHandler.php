<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 8:17 PM
 */

namespace TradeServe\CoreBundle\Handler;

use Symfony\Component\Form\Exception\FormException;
use TradeServe\CoreBundle\Services\EntityHandler;
use TradeServe\CoreBundle\Entity\Organization;

/**
 * Class IdeaHandler
 *
 * @package TradeServe\ApplicationModelBundle\Handler
 */
class CustomerHandler extends EntityHandler
{
    /**
     * Returns the entity with specified id if related toorganization
     *
     * @param $id
     * @param Organization $organization
     *
     * @return null|\TradeServe\CoreBundle\Model\EntityInterface
     */
    public function findByOrganization($id, Organization $organization)
    {
        $entity = $this->get($id);
        return $entity;
    }

    /**
     * Returns all entities of this type related to organization
     *
     * @param Organization $organization
     * @param bool $activeOnly
     *
     * @return array|\Doctrine\Common\Collections\Collection
     */
    public function allByOrganization(Organization $organization, $activeOnly = true)
    {

        return $organization->getCustomers();

    }
}