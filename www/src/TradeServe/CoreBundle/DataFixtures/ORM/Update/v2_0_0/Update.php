<?php

namespace TradeServe\CoreBundle\DataFixtures\ORM\Update\v2_0_0;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Subscriber;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\User;
use TradeServe\CoreBundle\Entity\Organization;

/**
 * Defines all predefined installed types created during install.
 *
 * @author Josh
 */
class Update extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $em = $this->container->get('doctrine')->getManager();
//        $accounts = $em->getRepository('TradeServeAppBundle:Account')->findAll();
//        $errors = null;
//        if ($accounts) {
//            foreach ($accounts as $account) {
//                if ($account instanceof Account) {
//                    $organization = $account->getOrganization();
//                    if ($organization instanceof Organization) {
//                        try {
//                            $mandrill = new \Mandrill('f92h3CLiJN_obZRuqqE_OQ');
//                            $id = $organization->getId();
//                            $name = $organization->getName();
//                            $notes = '';
//                            $custom_quota = 12000;
//                            $result = $mandrill->subaccounts->add($id, $name, $notes, $custom_quota);
//                        } catch (\Mandrill_Error $e) {
//                            // Mandrill errors are thrown as exceptions
//                            //print_r('A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage());
//                            // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
//                            //throw $e;
//                            $errors[] = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
//                        }
//                    }
//                }
//                //$user->setUsername(strtolower($user->getEmail()));
//                //$user->setEmail(strtolower($user->getEmail()));
//                //$this->manager->merge($user);
//            }
//            //$this->manager->flush();
//            if (is_array($errors)) {
//                print_r($errors);
//            }
//        }
//
//        if ($smsSubscribers = $em->getRepository('TradeServeAppBundle:SmsSubscriber')->findAll()) {
//            foreach ($smsSubscribers as $sms) {
//                $subscriber = new Subscriber();
//                $account = $em->getRepository('TradeServeAppBundle:Account')->find($sms->getAccount());
//                $type = $em->getRepository('TradeServeAppBundle:SubscriberType')->find(2);
//                $subscriber->setBlacklisted($sms->getBlacklisted());
//                $subscriber->setAccount($account);
//                $subscriber->setName($sms->getName());
//                $subscriber->setType($type);
//                $subscriber->setEmail(null);
//                $subscriber->setNumber($sms->getNumber());
//                $subscriber->setOptOut($sms->getOptOut());
//                $subscriber->setOptOutDate($sms->getOptOutDate());
//                $subscriber->setOptIn($sms->getOptIn());
//                $subscriber->setOptInDate($sms->getOptInDate());
//                $subscriber->setStatus($sms->getStatus());
//                $em->persist($subscriber);
//            }
//            //$em->flush();
//        }
//
//        $subscribers = $em->getRepository('TradeServeAppBundle:Subscriber')->findAll();
//        foreach ($subscribers as $sub) {
//            //$email = $em->getRepository('TradeServeAppBundle:SubscriberType')->find(1);
//            $sms = $em->getRepository('TradeServeAppBundle:SubscriberType')->find(2);
//            if ($sub->getEmail() == null && $sub->getNumber() != null) {
//                $sub->setType($sms);
//            } /*else if ($sub->getEmail() != null && $sub->getNumber() == null) {
//                $sub->setType($email);
//            }*/
//            $em->merge($sub);
//        }
//        //$em->flush();
//
//        $users = $em->getRepository('TradeServeAppBundle:User')->findAll();
//        $endUser = $em->getRepository('TradeServeAppBundle:UserRole')->find(1);
//        $staff = $em->getRepository('TradeServeAppBundle:UserRole')->find(3);
//        $superAdmin = $em->getRepository('TradeServeAppBundle:UserRole')->find(2);
//        foreach ($users as $josh) {
//            if ($josh instanceof User) {
//                $factory = $this->container->get('security.encoder_factory');
//                $encoder = $factory->getEncoder($josh);
//                if ($josh->getSalt() == null) {
//                    $josh->setSalt(md5(uniqid()));
//                }
//                $password = $encoder->encodePassword('password', $josh->getSalt());
//                $josh->setPassword($password);
//
//                $roles = $josh->getRoles();
//
//                $email = $josh->getUsername();
//                $pieces = explode("@", $email);
//                $domain = $pieces['1'];
//                if (!$roles) {
//                    if ($domain == 'remindcloud.com') {
//                        $josh->addRole($superAdmin);
//                        $josh->addRole($staff);
//                        $josh->addRole($endUser);
//                    } else {
//                        $josh->addRole($endUser);
//                    }
//                }
//                $em->merge($josh);
//            }
//        }
//        $em->flush();
    }


    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}

