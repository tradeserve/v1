<?php

namespace TradeServe\CoreBundle\DataFixtures\ORM\Update;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines all predefined installed types created during install.
 *
 * @author Josh
 */
class Update extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $this->manager = $this->container->get('doctrine')->getManager();
//        $users = $this->manager->getRepository('TradeServeAppBundle:User')->findAll();
//
//        if ($users) {
//            foreach ($users as $user) {
//                $user->setUsername(strtolower($user->getEmail()));
//                $user->setEmail(strtolower($user->getEmail()));
//                $this->manager->merge($user);
//            }
//            $this->manager->flush();
//        }
    }


    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}

