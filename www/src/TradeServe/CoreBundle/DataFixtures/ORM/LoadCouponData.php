<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/2/15
 * Time: 7:51 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Department;
use TradeServe\CoreBundle\Entity\Discount;
use TradeServe\CoreBundle\Entity\DiscountType;
use TradeServe\CoreBundle\Entity\Coupon;

class LoadCouponData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $discountTypes = array(
           'percent'    => array(
              'description' => 'percentage discount',
              'symbol'      => '%'
           ),
           'dollars'    => array(
              'description' => 'dollars discount',
              'symbol'      => '$'
           ),
           'non-profit' => array(
              'description' => 'non profit discount (no sales tax or applicable taxes)',
              'symbol'      => null
           )
        );

        $discouns = array(
           0 => array(
              'value' => '15',
              'type'  => 'dollars'
           ),
           1 => array(
              'value' => '25',
              'type'  => 'cents'
           ),
           2 => array(
              'value' => '35',
              'type'  => 'percent'
           )
        );

        $departments = array(
           'HVAC SERVICE',
           'HVAC INSTALL',
           'HVAC MAINTENANCE',
           'PLUMBING SERVICE',
           'PLUMBING INSTALL',
           'PLUMBING MAINTENANCE'
        );


        $coupons = array(
           'FIRE FIGHTER SPECIAL' => array(
              'discount'    => '1',
              'description' => 'An awesome special called FIRE FIGHTER SPECIAL',
              'value'       => '25',
              'type'        => 'dollars',
              'active'      => true,
              'expires'     => null
           ),
           'FIRST SERVICE'        => array(
              'discount'    => '2',
              'description' => 'An awesome special called FIRST SERVICE special',
              'value'       => '15',
              'type'        => 'dollars',
              'active'      => true,
              'expires'     => null
           ),
           'TANKLESS $100 OFF'    => array(
              'discount'    => '3',
              'description' => 'An awesome special called TANKLESS $100 OFF special',
              'value'       => '100',
              'type'        => 'dollars',
              'active'      => true,
              'expires'     => null
           ),
        );


        $org = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Organization')->findOneByName('TradeServe');

        foreach ($discountTypes as $key => $insert) {
            $discount = new DiscountType();
            $discount->setName($key);
            $discount->setDescription($insert['description']);
            $discount->setSymbol($insert['symbol']);
            $manager->persist($discount);
        }
        $manager->flush();

//        foreach ($discouns as $key => $insert) {
//            $discount = new Discount();
//            $type = $this->container->get('Doctrine')->getRepository('TradeServeAppBundle:DiscountType')->findOneByName($key);
//            $discount->setOrganization($org);
//            $discount->setValue($insert['value']);
//            $discount->setType($type);
//            $manager->persist($discount);
//        }
//        $manager->flush();


        foreach ($coupons as $key => $value) {
            $coupon = new Coupon();
            $coupon->setOrganization($org);
            $coupon->setName($key);
            $coupon->setDescription($value['description']);
            $coupon->setActive($value['active']);
            $coupon->setExpires($value['expires']);
            $coupon->setValue($value['value']);
            $type = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:DiscountType')->findOneByName($value['type']);
            $coupon->setType($type);
            $manager->persist($coupon);
        }
        $manager->flush();
//        $article = new Article();
//        $article->setTitle($articles['title']);
//        $manager->persist($article);
//        $manager->flush();
//        foreach ($articles['attributes'] as $attribute) {
//            $article->addAttribute($attribute, $attribute);
//        }
//        $manager->persist($article);

        foreach ($departments as $dept) {
            $department = new Department();
            $department->setName($dept);
            $department->setDescription($dept);
            $department->setOrganization($org);
            $manager->persist($department);
        }

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }
}