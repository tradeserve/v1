<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/10/15
 * Time: 10:18 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\MessageType;

class LoadMessageTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $types = array(
           'Dispatch',
           'Survey',
           'Follow Up',
           'Marketing',
           'Other'
        );


        foreach ($types as $type) {
            $account = new MessageType();
            $account->setName($type);
            $manager->persist($account);
        }
        $manager->flush();

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }
}