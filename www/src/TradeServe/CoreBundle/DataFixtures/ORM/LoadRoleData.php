<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 8/8/14
 * Time: 10:07 AM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TradeServe\CoreBundle\Entity\Role;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $roles = array(

            /*
             * User level roles
             */
           'ROLE_USER'            => 'User',
           'ROLE_SUPER_ADMIN'     => 'Super Admin',
           'ROLE_STAFF'           => 'Staff',
           'ROLE_TECH'            => 'Technician',
           'ROLE_ADMIN'           => 'Admin',
           'ROLE_CONSULTANT'      => 'Consultant',
           'ROLE_SUPPORT'         => 'Customer Support',
            /*
             * Account level roles
             */
           'ROLE_ACCOUNT_ADMIN'   => 'Administrator',
           'ROLE_ACCOUNT_OWNER'   => 'Owner',
           'ROLE_ACCOUNT_MANAGER' => 'Manager',
           'ROLE_ACCOUNT_MEMBER'  => 'Member',
        );

        foreach ($roles as $roleName => $name) {
            $role = new Role();
            $role->setName($name);
            $role->setRole($roleName);
            $manager->persist($role);
        }

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9;
    }
} 