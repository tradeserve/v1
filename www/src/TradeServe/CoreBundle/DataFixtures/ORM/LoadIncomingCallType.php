<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/10/15
 * Time: 10:05 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use TradeServe\CoreBundle\Entity\Address;
use TradeServe\CoreBundle\Entity\IncomingCallType;

class LoadIncomingCallType extends AbstractFixture implements OrderedFixtureInterface
{


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $types = array(
           'Opportunity',
           'Customer Support',
           'Vendor',
           'Personal',
           'Other'
        );


        foreach ($types as $type) {
            $account = new IncomingCallType();
            $account->setName($type);
            $manager->persist($account);
        }
        $manager->flush();

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}