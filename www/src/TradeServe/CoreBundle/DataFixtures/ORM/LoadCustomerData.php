<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/20/14
 * Time: 1:54 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Customer;
use TradeServe\CoreBundle\Entity\Organization;

class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $customers = array(
            0 => array(
                'firstName' => 'Josh',
                'lastName' => 'Stevens'
            ),
            1 => array(
                'firstName' => 'Jake',
                'lastName' => 'Litwicki'
            ),
            2 => array(
                'firstName' => 'Hutch',
                'lastName' => 'White'
            ),
            3 => array(
                'firstName' => 'Nathaniel',
                'lastName' => 'Duker'
            ),
            4 => array(
                'firstName' => 'Russ',
                'lastName' => 'Duker'
            ),
        );


        $phone_numbers = array(
            '6412262329',
            '5555555555',
            '5737777777',
            '6412260256',
            '2345768903'
        );

        $acct1 = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Organization')->findOneByName('Global');


        for ($i = 1; $i <= 50; $i++) {
            foreach ($customers as $customer) {
                $phoneId = mt_rand(0, 4);
                $address = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Address')->find(mt_rand(1, 100));
                $account = new Customer();
                $account->setOrganization($acct1);
                $account->setFirstName($customer['firstName']);
                $account->setLastName($customer['lastName']);
                $account->setMobilePhone($phone_numbers[$phoneId]);
                $account->setAddress($address);
                $manager->persist($account);
            }
        }
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }
} 