<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/2/15
 * Time: 7:51 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\Department;
use TradeServe\CoreBundle\Entity\Product;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $products = array(
           'Water Alarm accessory' => array(
              'image'             => 'service_system_software_logo_temp.jpg',
              'description'       => 'Water Alarm. 1 year warranty 12/11/12 might consider flat rating this product. in theory, should always be an add on product and thus not need to cover standard price, if we get this to the price of bio-clean might sell more. 1/8/13 changed price to price of bio-clean. ed ',
              'product_video_url' => 'http://www.youtube.com/watch?v=2nPPLNEsGU8&list=PL507CAB11A7622FAF&index=19&feature=plpp_video',
              'note'              => 'This is combined with sewer and drain clearing to help prevent future water damage. 1 year warranty',
              'tech_video'        => 'http://www.youtube.com/watch?v=hDwDyAA7FSI&feature=bf_next&list=UUGbXA-Z3k-fNoBNug8lgSoQ',
              '1',
              '27',
              '49',
              '0',
              '37.5',
              '1',
              '0',
              '0',
              '5',
              '15.48',
              '0',
              '0',
              '0',
              '0',
              '0',
              '0',
              '0',
              '3',
              '1',
              '4',
              '6/24/13 15:38',
              '1',
              '21,0',
              'a',
              '0',
              '0',
              '64',
              '0'
           )
        );
        $org = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Organization')->findOneByName('Global');
        $depts = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Department')->findOneByName('HVAC SERVICE');
        foreach ($products as $name => $insert) {
            $product = new Product();
            $product->setOrganization($org);
            $product->setName($name);
            $product->setDescription($insert['description']);
            $product->setProductVideoUrl($insert['product_video_url']);
            $product->setImage($insert['image']);
            $product->setTechnicianVideoUrl($insert['tech_video']);
            $product->setNote($insert['note']);
            $product->setDepartments(array($depts));
            $manager->persist($product);
        }

        $manager->flush();
//        $article = new Article();
//        $article->setTitle($articles['title']);
//        $manager->persist($article);
//        $manager->flush();
//        foreach ($articles['attributes'] as $attribute) {
//            $article->addAttribute($attribute, $attribute);
//        }
//        $manager->persist($article);

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }
}