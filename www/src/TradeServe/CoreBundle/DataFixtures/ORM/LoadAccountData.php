<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 8/12/14
 * Time: 8:40 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\Article;

class LoadAccountData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $acct = array(
           'Global'   => array(
              'description' => 'Global TradeServe Account',
           ),
           'TradeServe Test 2' => array(
              'description' => 'TradeServe Test Account',
           )
        );


        $articles = array(
           'title'      => 'first article',
           'attributes' => array(
              'fast',
              'slow',
              'little',
              'big'
           )
        );


        foreach ($acct as $acct_name => $data) {
            $account = new Account();
            $account->setName($acct_name);
            $account->setDescription($data['description']);
            $manager->persist($account);
            $manager->flush();
        }

        $article = new Article();
        $article->setTitle($articles['title']);
        $manager->persist($article);
        $manager->flush();
        foreach ($articles['attributes'] as $attribute) {
            $article->addAttribute($attribute, $attribute);
        }
        $manager->persist($article);

        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
} 