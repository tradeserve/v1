<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/20/14
 * Time: 1:54 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Technician;
use TradeServe\CoreBundle\Entity\Specialty;
use TradeServe\CoreBundle\Entity\Organization;

class LoadTechnicianData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $names = array('Standard', 'Josh', 'Hutch', 'Jake', 'Support');

        $acct1 = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Organization')->findOneByName('Global');

        $speciality = new Specialty();
        $speciality->setName('Plumbing');
        $manager->persist($speciality);
        $manager->flush();

        foreach ($names as $name) {
            $user = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:User')->findOneBy(array('first_name' => $name));
            $account = new Technician();
            $account->setOrganization($acct1);
            $account->setfirstName($user->getFirstName());
            $account->setlastName($user->getLastName());
            $account->setSpecialities(array($speciality));
            $account->setUser($user);
//            $account->setIndustry($data['industry']);
//            $account->setFromEmail('noreply@trade-serve.com');
//            $account->setFromName('TradeServe');
//            $account->setAccount($acct1);
            $manager->persist($account);
        }
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12;
    }
} 