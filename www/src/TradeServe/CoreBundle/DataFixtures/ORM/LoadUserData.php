<?php
namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use TradeServe\CoreBundle\Entity\User;
use TradeServe\CoreBundle\Entity\Role;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = array(
           'user'    => array(
              'firstName'    => 'Standard',
              'lastName'     => 'User',
              'email'        => 'user@tradeserve.com',
              'role'         => array(
                 'ROLE_SUPER_ADMIN',
                 'ROLE_STAFF',
                 'ROLE_USER'
              ),
              'accounts'     => array(
                 'Global'
              ),
              'account_role' => array(
                 'ROLE_ACCOUNT_ADMIN',
                 'ROLE_OWNER'
              )
           ),
           'josh'    => array(
              'firstName'    => 'Josh',
              'lastName'     => 'Stevens',
              'email'        => 'josh@tradeserve.com',
              'role'         => array(
                 'ROLE_SUPER_ADMIN',
                 'ROLE_STAFF',
                 'ROLE_USER',
                 'ROLE_ADMIN'
              ),
              'accounts'     => array(
                 'Global'
              ),
              'account_role' => array(
                 'ROLE_ACCOUNT_ADMIN',
                 'ROLE_OWNER'
              )
           ),
           'support' => array(
              'firstName'    => 'Support',
              'lastName'     => 'User',
              'email'        => 'support@tradeserve.com',
              'role'         => 'ROLE_SUPPORT',
              'accounts'     => array(),
              'account_role' => array()
           ),
           'hutch'   => array(
              'firstName'    => 'Hutch',
              'lastName'     => 'White',
              'email'        => 'hutch@tradeserve.com',
              'role'         => array(
                 'ROLE_SUPER_ADMIN',
                 'ROLE_STAFF',
                 'ROLE_USER'
              ),
              'accounts'     => array(
                 'Global'
              ),
              'account_role' => array(
                 'ROLE_ACCOUNT_ADMIN'
              )
           ),
           'jake'    => array(
              'firstName'    => 'Jake',
              'lastName'     => 'Litwicki',
              'email'        => 'jake@tradeserve.com',
              'role'         => array(
                 'ROLE_SUPER_ADMIN',
                 'ROLE_STAFF',
                 'ROLE_USER'
              ),
              'accounts'     => array(
                 'Global',
              ),
              'account_role' => array(
                 'ROLE_ACCOUNT_ADMIN'
              )
           ),
        );
        $org = $manager->getRepository('TradeServeCoreBundle:Organization')->find(1);
        foreach ($users as $username => $data) {
            $user = new User();
            if (is_array($data['role']) && !empty($data['role'])) {
                foreach ($data['role'] as $rol) {
                    $role = $manager->getRepository('TradeServeCoreBundle:Role')->findOneByRole($rol);
                    if ($role instanceof Role) {
                        $user->addRole($role);
                    }
                }
            } else {
                $role = $manager->getRepository('TradeServeCoreBundle:Role')->findOneByRole($data['role']);
                $user->addRole($role);
            }

            if (is_array($data['accounts']) && !empty($data['accounts'])) {
                foreach ($data['accounts'] as $account) {
                    $acct = $manager->getRepository('TradeServeCoreBundle:Account')->findOneByName($account);
                    $user->addAccount($acct);
                }
            }

            if (is_array($data['account_role']) && !empty($data['account_role'])) {
                foreach ($data['account_role'] as $acct_role) {
                    $role = $manager->getRepository('TradeServeCoreBundle:Role')->findOneByRole($acct_role);
                    if ($role instanceof Role) {
                        $user->addAccountRole($role);
                    }
                }
            }

            $user->setUsername($data['email']);
            $user->setSalt(md5(uniqid()));
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setOrganization($org);
            $user->setCreatedAt(new \DateTime("now"));
            $user->setUpdatedAt(new \DateTime("now"));
            $user->setPassword($this->container->get('security.encoder_factory')->getEncoder($user)->encodePassword('password',
               $user->getSalt()));
            $user->setEmail($data['email']);
            $manager->persist($user);
        }
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }
}