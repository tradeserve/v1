<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/20/14
 * Time: 1:54 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\Organization;

class LoadOrganizationData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $acct1 = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Account')->findOneByName('Global');
        $address = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Address')->findOneByStreet('5150 University Blvd');

        $acct = array(
           'Global' => array(
              'url'       => 'www.trade-serve.com',
              'industry'  => 'marketing',
              'employees' => 2,
              'age'       => 1,
              'image'     => null,
              'acct'      => $acct1
           )
        );

        foreach ($acct as $acct_name => $data) {
            $account = new Organization();
            $account->setName($acct_name);
            $account->setImage($data['image']);
            $account->setUrl($data['url']);
            $account->setAccount($data['acct']);
            $account->setAddress($address);
            $account->setIndustry($data['industry']);
            $account->setFromEmail('noreply@trade-serve.com');
            $account->setFromName('TradeServe');
            $account->setAccount($acct1);
            $manager->persist($account);
        }
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6;
    }
} 