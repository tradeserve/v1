<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/19/14
 * Time: 12:28 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use TradeServe\CoreBundle\Entity\Address;

class LoadAddressData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $streets = array('4002 Beechwood Dr.', '1223 Commonwealth Avenue', '5150 University Blvd');
        $citys = array('Columbia', 'Allston', 'Jacksonville');
        $states = array('MO', 'FL', 'KS');
        $zips = array('65202', '32216', '02135');

        for ($i = 1; $i <= 100; $i++) {
            $address = new Address();
            $address->setStreet($streets[mt_rand(0, 2)]);
            $address->setCity($citys[mt_rand(0, 2)]);
            $address->setState($states[mt_rand(0, 2)]);
            $address->setZip($zips[mt_rand(0, 2)]);

            $manager->persist($address);
        }
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
} 