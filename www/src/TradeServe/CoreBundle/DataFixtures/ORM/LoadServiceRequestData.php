<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 11/20/14
 * Time: 1:54 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TradeServe\CoreBundle\Entity\ServiceRequestEventType;
use TradeServe\CoreBundle\Entity\Technician;
use DateTime;
use TradeServe\CoreBundle\Entity\Specialty;
use TradeServe\CoreBundle\Entity\ServiceRequest;
use TradeServe\CoreBundle\Entity\ServiceRequestType;
use TradeServe\CoreBundle\Entity\ServiceRequestStatus;
use TradeServe\CoreBundle\Entity\ServiceRequestEvent;
use TradeServe\CoreBundle\Entity\ServiceRequestTechnicianStatus;
use TradeServe\CoreBundle\Entity\Organization;

class LoadServiceRequestData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {


        $org = $this->container->get('Doctrine')->getRepository('TradeServe\CoreBundle\Entity\Organization')->find(1);
        $user = $this->container->get('Doctrine')->getRepository('TradeServe\CoreBundle\Entity\User')->find(1);


        $serviceRequestType = array('demand', 'warranty', 'sales', 'install');
        $events = array(
           0 => array(
              'scheduled',
              'has a time and technician but is not dispatched'
           ),
           4 => array(
              'booked',
              'initial status'
           ),
           1 => array(
              'dispatched',
              'has a time and technician and has been sent to tech'
           ),
           2 => array(
              'ready for review',
              'needs reviewed so that it can be marked as closed'
           ),
           3 => array(
              'closed',
              'has been reviewed and marked as closed'
           )
        );

        $srStatus = array(
           0 => array(
              'scheduled',
              'has a time and technician but is not dispatched'
           ),
           1 => array(
              'dispatched',
              'has a time and technician and has been sent to tech'
           ),
           2 => array(
              'ready for review',
              'needs reviewed so that it can be marked as closed'
           ),
           3 => array(
              'closed',
              'has been reviewed and marked as closed'
           )
        );

        $techEvents = array(
           2 => array(
              'accepted',
              'technician views service request and acknowledges the work to be performed'
           ),
           3 => array(
              'work started',
              'technician has started work'
           ),
           4 => array(
              'work completed',
              'technician has completed work'
           ),
           5 => array(
              'en-route',
              'technician is currently driving to customer location'
           ),
           6 => array(
              'arrived',
              'technician has arrived at location'
           ),
        );
        foreach ($events as $key) {
            $new = new ServiceRequestEventType();
            $new->setName($key[0]);
            $new->setDescription($key[1]);
            $manager->persist($new);
            $manager->flush();
        }

        foreach ($serviceRequestType as $type) {
            $new = new ServiceRequestType();
            $new->setName($type);
            $manager->persist($new);
            $manager->flush();
        }

        foreach ($techEvents as $key) {
            $new = new ServiceRequestTechnicianStatus();
            $new->setName($key[0]);
            $new->setDescription($key[1]);
            $manager->persist($new);
            $manager->flush();
        }

        foreach ($srStatus as $status) {
            $new = new ServiceRequestStatus();
            $new->setName($status[0]);
            $manager->persist($new);
            $manager->flush();
        }

        for ($i = 0; $i <= 25; $i++) {
            $type = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:ServiceRequestType')->findOneBy(array(
               'id' => rand(1, count($serviceRequestType))
            ));

            $status = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:ServiceRequestStatus')->findOneBy(array(
               'id' => rand(1, count($srStatus))
            ));

            $customer = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Customer')->findOneBy(array(
               'id'           => rand(1, 5),
               'organization' => $org
            ));

            $speciality = new ServiceRequest();
            $speciality->setOrganization($org);
            $speciality->setCustomer($customer);
            $speciality->setType($type);
            $speciality->setStatus($status);
            //$speciality->addEvent($event);
            $speciality->setEstimatedStartDate(new DateTime('today'));
            $speciality->setEstimatedStartTime(new DateTime('now'));
            $manager->persist($speciality);
            $manager->flush();
            foreach ($events as $evented) {
                $speciality->addEvent($evented[0], $user);
                $manager->persist($speciality);
                $manager->flush();
            }
        }


//        $user = $this->container->get('Doctrine')->getRepository('TradeServeAppBundle:User')->findOneBy(array(
//           'first_name' => 'Josh'
//        ));


//        $status = new ServiceRequestStatus();
//        $status->setName('dispatched');
//        $manager->persist($status);
//
//
//        $speciality = new ServiceRequest();
//        $speciality->setOrganization($acct1);
//        $speciality->setCustomer($customer);
//        $speciality->setTechnicians(array($techs));
//        $speciality->setType($type);
//        $speciality->setStatus($status);
//        $speciality->setEstimatedStartDate(new DateTime('today'));
//        $speciality->setEstimatedStartTime(new DateTime('now'));
//        $manager->persist($speciality);
//        $manager->flush();
//        foreach ($events['events'] as $evented) {
//            $eventType = $this->container->get('Doctrine')->getRepository('TradeServeAppBundle:ServiceRequestEventType')->findOneByName($evented);
//            $speciality->addEvent($eventType, $user);
//            $manager->persist($speciality);
//            $manager->flush();
//        }

    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 13;
    }
} 