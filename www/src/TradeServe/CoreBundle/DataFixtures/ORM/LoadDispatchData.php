<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 2:28 PM
 */

namespace TradeServe\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TradeServe\CoreBundle\Entity\Dispatch;
use TradeServe\CoreBundle\Entity\DispatchEvent;
use TradeServe\CoreBundle\Entity\DispatchEventType;

use TradeServe\CoreBundle\Entity\DispatchType;

class LoadDispatchData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $org = $this->container->get('Doctrine')->getRepository('TradeServe\CoreBundle\Entity\Organization')->find(1);

        $serviceRequests = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:ServiceRequest')->findBy(array(
            'organization' => $org
        ));

        $techs = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Technician')->findBy(array(
            'organization' => $org
        ));
        $techCount = count($techs);

        $types = array(
            'single day',
            'multiple day',
            'other',
            'unknown'
        );
        $techEvents = array(
            2 => array(
                'accepted',
                'technician views service request and acknowledges the work to be performed'
            ),
            3 => array(
                'work started',
                'technician has started work'
            ),
            4 => array(
                'work completed',
                'technician has completed work'
            ),
            5 => array(
                'en-route',
                'technician is currently driving to customer location'
            ),
            6 => array(
                'arrived',
                'technician has arrived at location'
            ),
            7 => array(
                'completed',
                'technician has completed work'
            ),
        );

        foreach ($techEvents as $type) {
            $account = new DispatchEventType();
            $account->setName($type[0]);
            $account->setDescription($type[1]);
            $manager->persist($account);
            $manager->flush();
        }

        foreach ($types as $type) {
            $account = new DispatchType();
            $account->setName($type);
            $account->setDescription($type);
            $manager->persist($account);
            $manager->flush();
        }

        foreach ($serviceRequests as $sr) {
            $dispatchesToAdd = rand(1, 3);
            for ($i = 0; $i <= $dispatchesToAdd; $i++) {
                $dispatch = new Dispatch();
                $dispatch->setOrganization($org);
                $dispatch->setServiceRequest($sr);
                $tech = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:Technician')->findOneBy(array(
                    'id' => rand(1, count($techs)),
                    'organization' => $org
                ));

                $dispatchType = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:DispatchType')->find(rand(1, count($types)));

                $dispatch->setTechnician($tech);
                $dispatch->setType($dispatchType);
                if ($i == 0) {
                    $dispatch->setPrimaryTech(true);
                } else {
                    $dispatch->setPrimaryTech(false);
                }
                $manager->persist($dispatch);
                $manager->flush();
                if ($i == 0) {
                    foreach ($techEvents as $key => $value) {
                        $event = $this->container->get('Doctrine')->getRepository('TradeServeCoreBundle:DispatchEventType')->findOneBy(array(
                            'name' => $value[0]
                        ));
                        $dispatch->addEvent($event, $tech);
                        $manager->persist($dispatch);
                        $manager->flush();
                    }
                }
            }
        }


        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 14;
    }
}