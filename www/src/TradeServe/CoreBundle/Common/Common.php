<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 8/13/14
 * Time: 2:41 PM
 */

namespace TradeServe\CoreBundle\Common;

class Common
{

    /**
     *  Get Ids of children
     */
    public static function getEntityIds($entities)
    {
        $ids = array();
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $ids[] = $entity->getId();
            }
        }

        return $ids;
    }

    /**
     *  Get Ids of children
     */
    public static function getEntityId($entity)
    {
        return is_object($entity) ? $entity->getId() : 0;
    }

//    public static function getType($type, $em)
//    {
//        return $em->getRepository('TradeServeAppBundle:SubscriberType')->findOneBy(array('name' => $type));
//    }


    public static function getIndustrySelectChoices()
    {
        return array(
           'AL' => 'Alabama',
           'AK' => 'Alaska',
           'AS' => 'American Samoa',
           'AZ' => 'Arizona',
           'AR' => 'Arkansas',
           'AE' => 'Armed Forces - Europe',
           'AP' => 'Armed Forces - Pacific',
           'AA' => 'Armed Forces - USA/Canada',
           'CA' => 'California',
           'CO' => 'Colorado',
           'CT' => 'Connecticut',
           'DE' => 'Delaware',
           'DC' => 'District of Columbia',
           'FL' => 'Florida',
           'GA' => 'Georgia',
           'GU' => 'Guam',
           'HI' => 'Hawaii',
           'ID' => 'Idaho',
           'IL' => 'Illinois',
           'IN' => 'Indiana',
           'IA' => 'Iowa',
           'KS' => 'Kansas',
           'KY' => 'Kentucky',
           'LA' => 'Louisiana',
           'ME' => 'Maine',
           'MD' => 'Maryland',
           'MA' => 'Massachusetts',
           'MI' => 'Michigan',
           'MN' => 'Minnesota',
           'MS' => 'Mississippi',
           'MO' => 'Missouri',
           'MT' => 'Montana',
           'NE' => 'Nebraska',
           'NV' => 'Nevada',
           'NH' => 'New Hampshire',
           'NJ' => 'New Jersey',
           'NM' => 'New Mexico',
           'NY' => 'New York',
           'NC' => 'North Carolina',
           'ND' => 'North Dakota',
           'OH' => 'Ohio',
           'OK' => 'Oklahoma',
           'OR' => 'Oregon',
           'PA' => 'Pennsylvania',
           'PR' => 'Puerto Rico',
           'RI' => 'Rhode Island',
           'SC' => 'South Carolina',
           'SD' => 'South Dakota',
           'TN' => 'Tennessee',
           'TX' => 'Texas',
           'UT' => 'Utah',
           'VT' => 'Vermont',
           'VI' => 'Virgin Islands',
           'VA' => 'Virginia',
           'WA' => 'Washington',
           'WV' => 'West Virginia',
           'WI' => 'Wisconsin',
           'WY' => 'Wyoming'
        );
    }


    public static function getTimeChoices()
    {
        return array(
           '1' => '1:00 AM',
           '2' => '2:00 AM',
           '3' => '3:00 AM',
           '4' => '4:00 AM',
           '5' => '5:00 AM',
           '6' => '6:00 AM',
           '7' => '7:00 AM',
           '8' => '8:00 AM',
           '9' => '9:00 AM',
           '10' => '10:00 AM',
           '11' => '11:00 AM',
           '12' => '12:00 PM',
           '13' => '1:00 PM',
           '14' => '2:00 PM',
           '15' => '3:00 PM',
           '16' => '4:00 PM',
           '17' => '5:00 PM',
           '18' => '6:00 PM',
           '19' => '7:00 PM',
           '20' => '8:00 PM',
           '21' => '9:00 PM',
           '22' => '10:00 PM',
           '23' => '11:00 PM',
           '24' => '12:00 AM',
        );
    }

    public static function getAgeChoices()
    {
        return array(
           '1' => '1 YEARS or LESS',
           '2' => '2 YEARS',
           '3' => '3 YEARS',
           '4' => '4 YEARS',
           '5' => '5 YEARS',
           '6' => '6 YEARS',
           '7' => '7 YEARS',
           '8' => '8 YEARS',
           '9' => '9 YEARS',
           '10' => '10 YEARS',
           '11' => '> 10 YEARS',
        );
    }


    public static function getCountrySelectChoices($usOnly = true)
    {
        if ($usOnly) {
            return array('US' => 'United States');
        }

        return array(
           'US' => 'United States',
           'AF' => 'Afghanistan',
           'AL' => 'Albania',
           'DZ' => 'Algeria',
           'AS' => 'American Samoa',
           'AD' => 'Andorra',
           'AO' => 'Angola',
           'AI' => 'Anguilla',
           'AQ' => 'Antarctica',
           'AG' => 'Antigua And Barbuda',
           'AR' => 'Argentina',
           'AM' => 'Armenia',
           'AW' => 'Aruba',
           'AU' => 'Australia',
           'AT' => 'Austria',
           'AZ' => 'Azerbaijan',
           'BS' => 'Bahamas',
           'BH' => 'Bahrain',
           'BD' => 'Bangladesh',
           'BB' => 'Barbados',
           'BY' => 'Belarus',
           'BE' => 'Belgium',
           'BZ' => 'Belize',
           'BJ' => 'Benin',
           'BM' => 'Bermuda',
           'BT' => 'Bhutan',
           'BO' => 'Bolivia',
           'BA' => 'Bosnia And Herzegowina',
           'BW' => 'Botswana',
           'BV' => 'Bouvet Island',
           'BR' => 'Brazil',
           'IO' => 'British Indian Ocean Territory',
           'BN' => 'Brunei Darussalam',
           'BG' => 'Bulgaria',
           'BF' => 'Burkina Faso',
           'BI' => 'Burundi',
           'KH' => 'Cambodia',
           'CM' => 'Cameroon',
           'CA' => 'Canada',
           'CV' => 'Cape Verde',
           'KY' => 'Cayman Islands',
           'CF' => 'Central African Republic',
           'TD' => 'Chad',
           'CL' => 'Chile',
           'CN' => 'China',
           'CX' => 'Christmas Island',
           'CC' => 'Cocos (Keeling) Islands',
           'CO' => 'Colombia',
           'KM' => 'Comoros',
           'CG' => 'Congo',
           'CD' => 'Congo, The Democratic Republic static Of The',
           'CK' => 'Cook Islands',
           'CR' => 'Costa Rica',
           'CI' => 'Cote D\'Ivoire',
           'HR' => 'Croatia (Local Name: Hrvatska)',
           'CU' => 'Cuba',
           'CY' => 'Cyprus',
           'CZ' => 'Czech Republic',
           'DK' => 'Denmark',
           'DJ' => 'Djibouti',
           'DM' => 'Dominica',
           'DO' => 'Dominican Republic',
           'TP' => 'East Timor',
           'EC' => 'Ecuador',
           'EG' => 'Egypt',
           'SV' => 'El Salvador',
           'GQ' => 'Equatorial Guinea',
           'ER' => 'Eritrea',
           'EE' => 'Estonia',
           'ET' => 'Ethiopia',
           'FK' => 'Falkland Islands (Malvinas)',
           'FO' => 'Faroe Islands',
           'FJ' => 'Fiji',
           'FI' => 'Finland',
           'FR' => 'France',
           'FX' => 'France, Metropolitan',
           'GF' => 'French Guiana',
           'PF' => 'French Polynesia',
           'TF' => 'French Southern Territories',
           'GA' => 'Gabon',
           'GM' => 'Gambia',
           'GE' => 'Georgia',
           'DE' => 'Germany',
           'GH' => 'Ghana',
           'GI' => 'Gibraltar',
           'GR' => 'Greece',
           'GL' => 'Greenland',
           'GD' => 'Grenada',
           'GP' => 'Guadeloupe',
           'GU' => 'Guam',
           'GT' => 'Guatemala',
           'GN' => 'Guinea',
           'GW' => 'Guinea-Bissau',
           'GY' => 'Guyana',
           'HT' => 'Haiti',
           'HM' => 'Heard And Mc Donald Islands',
           'HN' => 'Honduras',
           'HK' => 'Hong Kong',
           'HU' => 'Hungary',
           'IS' => 'Iceland',
           'IN' => 'India',
           'ID' => 'Indonesia',
           'IR' => 'Iran (Islamic Republic static Of)',
           'IQ' => 'Iraq',
           'IE' => 'Ireland',
           'IL' => 'Israel',
           'IT' => 'Italy',
           'JM' => 'Jamaica',
           'JP' => 'Japan',
           'JO' => 'Jordan',
           'KZ' => 'Kazakhstan',
           'KE' => 'Kenya',
           'KI' => 'Kiribati',
           'KP' => 'Korea, Democratic People\'S Republic static Of',
           'KR' => 'Korea, Republic static Of',
           'KW' => 'Kuwait',
           'KG' => 'Kyrgyzstan',
           'LA' => 'Lao People\'S Democratic Republic',
           'LV' => 'Latvia',
           'LB' => 'Lebanon',
           'LS' => 'Lesotho',
           'LR' => 'Liberia',
           'LY' => 'Libyan Arab Jamahiriya',
           'LI' => 'Liechtenstein',
           'LT' => 'Lithuania',
           'LU' => 'Luxembourg',
           'MO' => 'Macau',
           'MK' => 'Macedonia, Former Yugoslav Republic static Of',
           'MG' => 'Madagascar',
           'MW' => 'Malawi',
           'MY' => 'Malaysia',
           'MV' => 'Maldives',
           'ML' => 'Mali',
           'MT' => 'Malta',
           'MH' => 'Marshall Islands, Republic static of the',
           'MQ' => 'Martinique',
           'MR' => 'Mauritania',
           'MU' => 'Mauritius',
           'YT' => 'Mayotte',
           'MX' => 'Mexico',
           'FM' => 'Micronesia, Federated States Of',
           'MD' => 'Moldova, Republic static Of',
           'MC' => 'Monaco',
           'MN' => 'Mongolia',
           'MS' => 'Montserrat',
           'MA' => 'Morocco',
           'MZ' => 'Mozambique',
           'MM' => 'Myanmar',
           'NA' => 'Namibia',
           'NR' => 'Nauru',
           'NP' => 'Nepal',
           'NL' => 'Netherlands',
           'AN' => 'Netherlands Antilles',
           'NC' => 'New Caledonia',
           'NZ' => 'New Zealand',
           'NI' => 'Nicaragua',
           'NE' => 'Niger',
           'NG' => 'Nigeria',
           'NU' => 'Niue',
           'NF' => 'Norfolk Island',
           'MP' => 'Northern Mariana Islands, Commonwealth of the',
           'NO' => 'Norway',
           'OM' => 'Oman',
           'PK' => 'Pakistan',
           'PW' => 'Palau, Republic static of',
           'PA' => 'Panama',
           'PG' => 'Papua New Guinea',
           'PY' => 'Paraguay',
           'PE' => 'Peru',
           'PH' => 'Philippines',
           'PN' => 'Pitcairn',
           'PL' => 'Poland',
           'PT' => 'Portugal',
           'PR' => 'Puerto Rico',
           'QA' => 'Qatar',
           'RE' => 'Reunion',
           'RO' => 'Romania',
           'RU' => 'Russian Federation',
           'RW' => 'Rwanda',
           'KN' => 'Saint Kitts And Nevis',
           'LC' => 'Saint Lucia',
           'VC' => 'Saint Vincent And The Grenadines',
           'WS' => 'Samoa',
           'SM' => 'San Marino',
           'ST' => 'Sao Tome And Principe',
           'SA' => 'Saudi Arabia',
           'SN' => 'Senegal',
           'SC' => 'Seychelles',
           'SL' => 'Sierra Leone',
           'SG' => 'Singapore',
           'SK' => 'Slovakia (Slovak Republic)',
           'SI' => 'Slovenia',
           'SB' => 'Solomon Islands',
           'SO' => 'Somalia',
           'ZA' => 'South Africa',
           'GS' => 'South Georgia, South Sandwich Islands',
           'ES' => 'Spain',
           'LK' => 'Sri Lanka',
           'SH' => 'St. Helena',
           'PM' => 'St. Pierre And Miquelon',
           'SD' => 'Sudan',
           'SR' => 'Suriname',
           'SJ' => 'Svalbard And Jan Mayen Islands',
           'SZ' => 'Swaziland',
           'SE' => 'Sweden',
           'CH' => 'Switzerland',
           'SY' => 'Syrian Arab Republic',
           'TW' => 'Taiwan',
           'TJ' => 'Tajikistan',
           'TZ' => 'Tanzania, United Republic static Of',
           'TH' => 'Thailand',
           'TG' => 'Togo',
           'TK' => 'Tokelau',
           'TO' => 'Tonga',
           'TT' => 'Trinidad And Tobago',
           'TN' => 'Tunisia',
           'TR' => 'Turkey',
           'TM' => 'Turkmenistan',
           'TC' => 'Turks And Caicos Islands',
           'TV' => 'Tuvalu',
           'UG' => 'Uganda',
           'UA' => 'Ukraine',
           'AE' => 'United Arab Emirates',
           'GB' => 'United Kingdom',
           'UM' => 'United States Minor Outlying Islands',
           'UY' => 'Uruguay',
           'UZ' => 'Uzbekistan',
           'VU' => 'Vanuatu',
           'VA' => 'Vatican City, State of the',
           'VE' => 'Venezuela',
           'VN' => 'Viet Nam',
           'VG' => 'Virgin Islands (British)',
           'VI' => 'Virgin Islands (U.S.)',
           'WF' => 'Wallis And Futuna Islands',
           'EH' => 'Western Sahara',
           'YE' => 'Yemen',
           'YU' => 'Yugoslavia',
           'ZM' => 'Zambia',
           'ZW' => 'Zimbabwe'
        );
    }

    public static function getStateSelectChoices()
    {
        return array(
           'AL' => 'Alabama',
           'AK' => 'Alaska',
           'AS' => 'American Samoa',
           'AZ' => 'Arizona',
           'AR' => 'Arkansas',
           'AE' => 'Armed Forces - Europe',
           'AP' => 'Armed Forces - Pacific',
           'AA' => 'Armed Forces - USA/Canada',
           'CA' => 'California',
           'CO' => 'Colorado',
           'CT' => 'Connecticut',
           'DE' => 'Delaware',
           'DC' => 'District of Columbia',
           'FL' => 'Florida',
           'GA' => 'Georgia',
           'GU' => 'Guam',
           'HI' => 'Hawaii',
           'ID' => 'Idaho',
           'IL' => 'Illinois',
           'IN' => 'Indiana',
           'IA' => 'Iowa',
           'KS' => 'Kansas',
           'KY' => 'Kentucky',
           'LA' => 'Louisiana',
           'ME' => 'Maine',
           'MD' => 'Maryland',
           'MA' => 'Massachusetts',
           'MI' => 'Michigan',
           'MN' => 'Minnesota',
           'MS' => 'Mississippi',
           'MO' => 'Missouri',
           'MT' => 'Montana',
           'NE' => 'Nebraska',
           'NV' => 'Nevada',
           'NH' => 'New Hampshire',
           'NJ' => 'New Jersey',
           'NM' => 'New Mexico',
           'NY' => 'New York',
           'NC' => 'North Carolina',
           'ND' => 'North Dakota',
           'OH' => 'Ohio',
           'OK' => 'Oklahoma',
           'OR' => 'Oregon',
           'PA' => 'Pennsylvania',
           'PR' => 'Puerto Rico',
           'RI' => 'Rhode Island',
           'SC' => 'South Carolina',
           'SD' => 'South Dakota',
           'TN' => 'Tennessee',
           'TX' => 'Texas',
           'UT' => 'Utah',
           'VT' => 'Vermont',
           'VI' => 'Virgin Islands',
           'VA' => 'Virginia',
           'WA' => 'Washington',
           'WV' => 'West Virginia',
           'WI' => 'Wisconsin',
           'WY' => 'Wyoming'
        );
    }

    public static function getCanadianProvinceSelectChoices()
    {
        return array(
           'AB' => 'Alberta',
           'BC' => 'British Columbia',
           'MB' => 'Manitoba',
           'NB' => 'New Brunswick',
           'NF' => 'Newfoundland and Labrador',
           'NT' => 'Northwest Territories',
           'NS' => 'Nova Scotia',
           'NU' => 'Nunavut',
           'ON' => 'Ontario',
           'PE' => 'Prince Edward Island',
           'QC' => 'Quebec',
           'SK' => 'Saskatchewan',
           'YT' => 'Yukon Territory'
        );
    }

    public static function getAustralianStateSelectChoices()
    {
        return array(
           'ACT' => 'Australian Capital Territory',
           'JBT' => 'Jervis Bay Territory',
           'NSW' => 'New South Wales',
           'NT' => 'Northern Territory',
           'QLD' => 'Queensland',
           'SA' => 'South Australia',
           'TAS' => 'Tasmania',
           'VIC' => 'Victoria',
           'WA' => 'Western Australia'
        );
    }

    public static function getMonthSelectChoices()
    {
        return array(
           '01' => 'January',
           '02' => 'February',
           '03' => 'March',
           '04' => 'April',
           '05' => 'May',
           '06' => 'June',
           '07' => 'July ',
           '08' => 'August',
           '09' => 'September',
           '10' => 'October',
           '11' => 'November',
           '12' => 'December',
        );
    }

    public static function getYearSelectChoices()
    {
        $years = array(
           '1958' => '1958',
           '1959' => '1959',
           '1960' => '1960',
           '1961' => '1961',
           '1962' => '1962',
           '1963' => '1963',
           '1964' => '1964',
           '1965' => '1965',
           '1966' => '1966',
           '1967' => '1967',
           '1968' => '1968',
           '1969' => '1969',
           '1970' => '1970',
           '1971' => '1971',
           '1972' => '1972',
           '1973' => '1973',
           '1974' => '1974',
           '1975' => '1975',
           '1976' => '1976',
           '1977' => '1977',
           '1978' => '1978',
           '1979' => '1979',
           '1980' => '1980',
           '1981' => '1981',
           '1982' => '1982',
           '1983' => '1983',
           '1984' => '1984',
           '1985' => '1985',
           '1986' => '1986',
           '1987' => '1987',
           '1988' => '1988',
           '1989' => '1989',
           '1990' => '1990',
           '1991' => '1991',
           '1992' => '1992',
           '1993' => '1993',
           '1994' => '1994',
           '1995' => '1995',
           '1996' => '1996',
           '1997' => '1997',
           '1998' => '1998',
           '1999' => '1999',
           '2000' => '2000',
           '2001' => '2001',
           '2002' => '2002',
           '2003' => '2003',
           '2004' => '2004',
           '2005' => '2005',
           '2006' => '2006',
           '2007' => '2007',
           '2008' => '2008',
           '2009' => '2009',
           '2010' => '2010',
           '2011' => '2011',
           '2012' => '2012',
           '2013' => '2013',
           '2014' => '2014',
        );

        $year = array_reverse($years, true);

        return $year;
    }

    public static function timeAgo(\DateTime $oldDate, $depth = 2, \DateTime $now = null)
    {
        try {
            if ($now == null) {
                $now = new \DateTime("now");
            }

            if (!is_int($depth) || $depth < 1 || $depth > 6) {
                throw new \InvalidArgumentException("Time Ago depth cannot be an integer between 1 and 6");
            }

            if ($oldDate > $now) {
                throw new \InvalidArgumentException('The comparative date cannot be greater than now.');
            }

            $difference = $now->diff($oldDate);

            $intervals = array(
               'y' => 'year',
               'm' => 'month',
               'd' => 'day',
               'h' => 'hour',
               'i' => 'min',
               's' => 'sec'
            );

            $i = 0;
            $timeAgo = '';

            foreach ($intervals as $interval => $name) {

                if ($difference->$interval > 1) {
                    $timeAgo .= sprintf('%s %ss ', $difference->$interval, $intervals[$interval]);
                    $i++;
                } elseif ($difference->$interval == 1) {
                    $timeAgo .= sprintf('%s %s ', $difference->$interval, $intervals[$interval]);
                    $i++;
                }

                if ($i == $depth) {
                    break;
                }
            }

            return sprintf('%s ago', $timeAgo);
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * Parses a user agent string into its important parts
     *
     * @author Jesse G. Donat <donatj@gmail.com>
     * @link   https://github.com/donatj/PhpUserAgent
     * @link   http://donatstudios.com/PHP-Parser-HTTP_USER_AGENT
     *
     * @param string|null $u_agent User agent string to parse or null. Uses $_SERVER['HTTP_USER_AGENT'] on NULL
     *
     * @return array an array with browser, version and platform keys
     */
    public static function parseUserAgent($u_agent = null)
    {
        if (is_null($u_agent)) {
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $u_agent = $_SERVER['HTTP_USER_AGENT'];
            } else {
                throw new \Exception(sprintf('%s requires a user agent', __METHOD__));
            }
        }

        $platform = null;
        $browser = null;
        $version = null;

        $empty = array(
           'platform' => $platform,
           'browser' => $browser,
           'version' => $version
        );

        if (!$u_agent) {
            return $empty;
        }

        if (preg_match('/\((.*?)\)/im', $u_agent, $parent_matches)) {

            preg_match_all('/(?P<platform>BB\d+;|Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows(\ Phone)?|Silk|linux-gnu|BlackBerry|PlayBook|Nintendo\ (WiiU?|3DS)|Xbox(\ One)?)
                                    (?:\ [^;]*)?
                                    (?:;|$)/imx', $parent_matches[1], $result, PREG_PATTERN_ORDER);

            $priority = array(
               'Android',
               'Xbox One',
               'Xbox'
            );
            $result['platform'] = array_unique($result['platform']);
            if (count($result['platform']) > 1) {
                if ($keys = array_intersect($priority, $result['platform'])) {
                    $platform = reset($keys);
                } else {
                    $platform = $result['platform'][0];
                }
            } elseif (isset($result['platform'][0])) {
                $platform = $result['platform'][0];
            }
        }

        if ($platform == 'linux-gnu') {
            $platform = 'Linux';
        } elseif ($platform == 'CrOS') {
            $platform = 'Chrome OS';
        }

        preg_match_all('%(?P<browser>Camino|Kindle(\ Fire\ Build)?|Firefox|Iceweasel|Safari|MSIE|Trident/.*rv|AppleWebKit|Chrome|IEMobile|Opera|OPR|Silk|Lynx|Midori|Version|Wget|curl|NintendoBrowser|PLAYSTATION\ (\d|Vita)+)
                            (?:\)?;?)
                            (?:(?:[:/ ])(?P<version>[0-9A-Z.]+)|/(?:[A-Z]*))%ix', $u_agent, $result,
           PREG_PATTERN_ORDER);

        // If nothing matched, return null (to avoid undefined index errors)
        if (!isset($result['browser'][0]) || !isset($result['version'][0])) {
            return $empty;
        }

        $browser = $result['browser'][0];
        $version = $result['version'][0];

        $find = function ($search, &$key) use ($result) {
            $xkey = array_search(strtolower($search), array_map('strtolower', $result['browser']));
            if ($xkey !== false) {
                $key = $xkey;

                return true;
            }

            return false;
        };

        $key = 0;
        if ($browser == 'Iceweasel') {
            $browser = 'Firefox';
        } elseif ($find('Playstation Vita', $key)) {
            $platform = 'PlayStation Vita';
            $browser = 'Browser';
        } elseif ($find('Kindle Fire Build', $key) || $find('Silk', $key)) {
            $browser = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
            $platform = 'Kindle Fire';
            if (!($version = $result['version'][$key]) || !is_numeric($version[0])) {
                $version = $result['version'][array_search('Version', $result['browser'])];
            }
        } elseif ($find('NintendoBrowser', $key) || $platform == 'Nintendo 3DS') {
            $browser = 'NintendoBrowser';
            $version = $result['version'][$key];
        } elseif ($find('Kindle', $key)) {
            $browser = $result['browser'][$key];
            $platform = 'Kindle';
            $version = $result['version'][$key];
        } elseif ($find('OPR', $key)) {
            $browser = 'Opera Next';
            $version = $result['version'][$key];
        } elseif ($find('Opera', $key)) {
            $browser = 'Opera';
            $find('Version', $key);
            $version = $result['version'][$key];
        } elseif ($find('Midori', $key)) {
            $browser = 'Midori';
            $version = $result['version'][$key];
        } elseif ($find('Chrome', $key)) {
            $browser = 'Chrome';
            $version = $result['version'][$key];
        } elseif ($browser == 'AppleWebKit') {
            if (($platform == 'Android' && !($key = 0))) {
                $browser = 'Android Browser';
            } elseif (strpos($platform, 'BB') === 0) {
                $browser = 'BlackBerry Browser';
                $platform = 'BlackBerry';
            } elseif ($platform == 'BlackBerry' || $platform == 'PlayBook') {
                $browser = 'BlackBerry Browser';
            } elseif ($find('Safari', $key)) {
                $browser = 'Safari';
            }

            $find('Version', $key);

            $version = $result['version'][$key];
        } elseif ($browser == 'MSIE' || strpos($browser, 'Trident') !== false) {
            if ($find('IEMobile', $key)) {
                $browser = 'IEMobile';
            } else {
                $browser = 'MSIE';
                $key = 0;
            }
            $version = $result['version'][$key];
        } elseif ($key = preg_grep('/playstation \d/i', array_map('strtolower', $result['browser']))) {
            $key = reset($key);

            $platform = 'PlayStation ' . preg_replace('/[^\d]/i', '', $key);
            $browser = 'NetFront';
        }

        return array(
           'platform' => $platform,
           'browser' => $browser,
           'version' => $version
        );
    }

    public static function getTimezoneOptions()
    {

        return array(
           'America/New_York' => 'Eastern (EST)',
           'America/Chicago' => 'Central (CST)',
           'America/Denver' => 'Mountain (MDT)',
           'America/Phoenix' => 'Mountain (MST)',
           'America/Los_Angeles' => 'Pacific (PST)',
           'America/Anchorage' => 'Alaska',
           'America/Honolulu' => 'Hawaii',
        );
    }

    /**
     *  Format a DateTime by timezone and specified format
     *
     * @param $format string
     * @param $tz     \DateTimeZone
     *
     * @return $datetime string
     */
    public static function formatDateTime($format = 'Y-m-d h:ia', $timeZone)
    {
        $tz = new \DateTimeZone($timeZone);
    }

    /**
     * Convert BR tags to nl
     *
     * @param $string
     *
     * @return string The converted string
     */
    public static function br2nl($string)
    {
        return preg_replace('#<br\s*?/?>\\n#i', "\n", $string);
    }


    /**
     * Takes a word separated by underscores and capitalizes into camelcase.
     *
     * @param $string
     * @param bool $first_char_caps
     *
     * @return mixed
     */
    public static function underscoreToCamelCase($string, $first_char_caps = false)
    {
        if ($first_char_caps == true) {
            $string[0] = strtoupper($string[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $string);
    }
}