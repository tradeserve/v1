<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Model\EntityInterface;
use TradeServe\CoreBundle\Model\LoggableInterface;

/**
 * Product
 *
 * @ORM\Table(name="Product")
 * @ORM\Entity
 */
class Product extends TradeServeEntity implements EntityInterface, LoggableInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization",  inversedBy="products")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;

    /**
     * @ORM\Column(type="text", length=8000, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", length=8000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="text", length=8000, nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="text", length=8000, nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $product_video_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $technician_video_url;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sell_sheet_url;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $labor_minutes;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=4, nullable=true)
     */
    protected $material_cost;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=4, nullable=true)
     */
    protected $delivery_fee;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=4, nullable=true)
     */
    protected $commission;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $status;


    /**
     * @ORM\ManyToMany(targetEntity="Coupon")
     * @ORM\JoinTable(name="ProductToCoupon",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="coupon_id", referencedColumnName="id")}
     *      )
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedCouponIds")
     */
    protected $coupons;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\Tag")
     * @ORM\JoinTable(
     *     name="ProductToTag",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", nullable=false)}
     * )
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedTagIds")
     */
    protected $tags;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\Department")
     * @ORM\JoinTable(
     *     name="ProductToDepartment",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=false)}
     * )
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedDepartmentIds")
     */
    protected $departments;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\ProductCategory")
     * @ORM\JoinTable(
     *     name="ProductToProductCategory",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)}
     * )
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedCategoryIds")
     */
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\ProductLine")
     * @ORM\JoinTable(
     *     name="ProductToProductLine",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_line_id", referencedColumnName="id", nullable=false)}
     * )
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedProductLineIds")
     */
    protected $product_lines;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updated;

    public function __construct()
    {
        $this->coupons = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->product_lines = new ArrayCollection();
        $this->setStatus(1);
        $this->updatedTimestamps();
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
            $this->setUpdated(new \DateTime('now'));
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return Product
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductVideoUrl()
    {
        return $this->product_video_url;
    }

    /**
     * @param mixed $product_video_url
     * @return Product
     */
    public function setProductVideoUrl($product_video_url)
    {
        $this->product_video_url = $product_video_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTechnicianVideoUrl()
    {
        return $this->technician_video_url;
    }

    /**
     * @param mixed $technician_video_url
     * @return Product
     */
    public function setTechnicianVideoUrl($technician_video_url)
    {
        $this->technician_video_url = $technician_video_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSellSheetUrl()
    {
        return $this->sell_sheet_url;
    }

    /**
     * @param mixed $sell_sheet_url
     * @return Product
     */
    public function setSellSheetUrl($sell_sheet_url)
    {
        $this->sell_sheet_url = $sell_sheet_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLaborMinutes()
    {
        return $this->labor_minutes;
    }

    /**
     * @param mixed $labor_minutes
     * @return Product
     */
    public function setLaborMinutes($labor_minutes)
    {
        $this->labor_minutes = $labor_minutes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaterialCost()
    {
        return $this->material_cost;
    }

    /**
     * @param mixed $material_cost
     * @return Product
     */
    public function setMaterialCost($material_cost)
    {
        $this->material_cost = $material_cost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryFee()
    {
        return $this->delivery_fee;
    }

    /**
     * @param mixed $delivery_fee
     * @return Product
     */
    public function setDeliveryFee($delivery_fee)
    {
        $this->delivery_fee = $delivery_fee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     * @return Product
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * @param mixed $coupons
     * @return Product
     */
    public function setCoupons($coupons)
    {
        $this->coupons = $coupons;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     * @return Product
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param mixed $departments
     * @return Product
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     * @return Product
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductLines()
    {
        return $this->product_lines;
    }

    /**
     * @param mixed $product_lines
     * @return Product
     */
    public function setProductLines($product_lines)
    {
        $this->product_lines = $product_lines;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Product
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return Product
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Adds a log message when user creates an item
     *
     * @return String (log entry)
     */
    public function logCreate($user, $entity)
    {
        return $user . ' added the ' . $entity . ' to ' . $this->logOrganization($entity);
    }

    /**
     * Adds a log message when user edits or marks inactive an item
     *
     * @return String (log entry)
     */
    public function logEdit($user, $entity, $newName)
    {
        $label = $user . ' updated the ' . $entity . ' in ' . $this->logOrganization($entity);

        if ($newName) {
            $label = $label . ' and renamed it to ' . $newName;
        }

        return $label;

    }

    /**
     * Adds a log message when user deletes an item
     *
     * @return String (log entry)
     */
    public function logDelete($user, $entity)
    {
        return $user . ' removed the ' . $entity . ' from ' . $this->logOrganization($entity);
    }

    /**
     * Gets the growth case of the entity
     *
     * @return String (Growth Case)
     */
    public function logOrganization($entity)
    {
        $case = $entity->getOrganization();

        return $case;
    }

}
