<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\ServiceRequestEvent;
use TradeServe\CoreBundle\Common\Common;
use TradeServe\CoreBundle\Model\EntityInterface;

/**
 * Dispatch
 *
 * @ORM\Table(name="Dispatch")
 * @ORM\Entity
 */
class Dispatch extends TradeServeEntity implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="dispatches")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=false)
     * @Groups({"internal"})
     * @type("string")
     * @Accessor(getter="getOrganization")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceRequest", inversedBy="dispatches")
     * @ORM\JoinColumn(name="service_request_id", referencedColumnName="id")
     * @Groups({"internal"})
     * @Accessor(getter="getServiceRequest")
     */
    protected $service_request;

    /**
     * @ORM\ManyToOne(targetEntity="DispatchType")
     * @ORM\JoinColumn(name="dispatch_type_id", referencedColumnName="id")
     * @Groups({"internal","call"})
     * @type("string")
     * @Accessor(getter="getType")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Technician", inversedBy="dispatches")
     * @ORM\JoinColumn(name="technician_id", referencedColumnName="id")
     * @Groups({"internal","call"})
     * @type("string")
     * @Accessor(getter="getTechnician")
     */
    protected $technician;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"internal","call"})
     */
    protected $primary_tech;

    /**
     * @ORM\OneToMany(targetEntity="DispatchEvent", mappedBy="dispatch", cascade={"ALL"}, indexBy="event")
     * @Groups({"internal","call"})
     * @Type("array<TradeServe\CoreBundle\Entity\DispatchEvent>")
     * @Accessor(getter="getEvents")
     */
    protected $events;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"internal","call"})
     * @Type("DateTime<'g:ma'>")
     * @Accessor(getter="getScheduledTime")
     */
    protected $scheduled_time;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"internal","call"})
     * @Type("DateTime<'Y-m-d'>")
     * @Accessor(getter="getScheduledDate")
     */
    protected $scheduled_date;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"internal","call"})
     * @Type("DateTime")
     * @Accessor(getter="getCreated")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"internal","call"})
     * @Type("DateTime")
     * @Accessor(getter="getUpdated")
     */
    protected $updated;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->updatedTimestamps();
        $this->setPrimaryTech(0);
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        $this->setUpdated(new \DateTime('now'));

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getServiceRequest()
    {
        return $this->service_request;
    }

    /**
     * @param mixed $service_request
     * @return Dispatch
     */
    public function setServiceRequest($service_request)
    {
        $this->service_request = $service_request;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Dispatch
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param mixed $technician
     * @return Dispatch
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimaryTech()
    {
        return $this->primary_tech;
    }

    /**
     * @param mixed $primary_tech
     * @return Dispatch
     */
    public function setPrimaryTech($primary_tech)
    {
        $this->primary_tech = $primary_tech;
        return $this;
    }


    public function addEvent(DispatchEventType $eventType, Technician $tech)
    {
        $this->events[$eventType->getId()] = new DispatchEvent($eventType, $tech, $this);
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param mixed $events
     * @return Dispatch
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return Dispatch
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Dispatch
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Dispatch
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScheduledTime()
    {
        return $this->scheduled_time;
    }

    /**
     * @param mixed $scheduled_time
     * @return Dispatch
     */
    public function setScheduledTime($scheduled_time)
    {
        $this->scheduled_time = $scheduled_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScheduledDate()
    {
        return $this->scheduled_date;
    }

    /**
     * @param mixed $scheduled_date
     * @return Dispatch
     */
    public function setScheduledDate($scheduled_date)
    {
        $this->scheduled_date = $scheduled_date;
        return $this;
    }
}
