<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Common\Common;

/**
 * ServiceRequestEvent
 *
 * @ORM\Table(name="ServiceRequestEvent")
 * @ORM\Entity
 */
class ServiceRequestEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceRequest", inversedBy="events")
     */
    protected $service_request;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id")
     * @Groups({"internal","call"})
     * @Type("string")
     * @Accessor(getter="getUser")
     */
    protected $user;

    /**
     * @ORM\Column(type="string")
     * @Groups({"internal","call"})
     */
    protected $value;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Groups({"internal","call"})
     */
    protected $created;


    public function __construct($value, $user, $service_request)
    {
        $this->created = new \DateTime('now');
        $this->value = $value;
        $this->user = $user;
        $this->service_request = $service_request;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getServiceRequest()
    {
        return $this->service_request;
    }

    /**
     * @param mixed $service_request
     * @return ServiceRequestEvent
     */
    public function setServiceRequest($service_request)
    {
        $this->service_request = $service_request;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return ServiceRequestEvent
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return ServiceRequestEvent
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return ServiceRequestEvent
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

//    public function __toString()
//    {
//        return $this->value;
//    }

}
