<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Rhumsaa\Uuid\Uuid;
use TradeServe\CoreBundle\Entity\TradeServeEntity;
use TradeServe\CoreBundle\Model\EntityInterface;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Account
 *
 * @ORM\Table(name="Account")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Account extends TradeServeEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

//    /**
//     * @ORM\Column(type="integer")
//     */
//    protected $sms_on;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\User", mappedBy="accounts")
     *
     */
    protected $users;

//    /**
//     * @ORM\OneToMany(targetEntity="Subscriber", mappedBy="account")
//     **/
//    protected $subscribers;

    /**
     * @ORM\OneToMany(targetEntity="Organization", mappedBy="account", orphanRemoval=true)
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedOrganizationsIds")
     */
    protected $organizations;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     *
     */
    protected $guid;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Expose
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=8000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $api_key;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $subscription;

    /**
     * @ORM\Column(type="string")
     */
    protected $timezone;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $active_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $next_bill_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_bill_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $customer_accept_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $customer_decline_date;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->setStatus(1);
        $this->timezone = 'America/Chicago';
        $g = Uuid::uuid5(Uuid::NAMESPACE_DNS, (string)time() . uniqid());
        $this->guid = md5($g);
        $this->api_key = $g;
        $this->updatedTimestamps();
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        if ($this->getCreateDate() == null) {
            $this->setCreateDate(new \DateTime('now'));
            $this->setActiveDate(new \DateTime('now'));
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $api_key
     */
    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $subscription
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Add $subscribers
     *
     * @param \TradeServe\CoreBundle\Entity\User $users
     *
     * @return User
     */
    public function addUser(User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove $users
     *
     * @param \TradeServe\CoreBundle\Entity\User $users
     */
    public function removeUser(User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        if (!empty($this->users)) {
            return $this->users->toArray();
        }

        return $this->users;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return mixed
     */
    public function getUserss()
    {
        return $this->users;
    }


    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }


    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return mixed
     */
    public function getActiveDate()
    {
        return $this->active_date;
    }

    /**
     * @param mixed $active_date
     */
    public function setActiveDate($active_date)
    {
        $this->active_date = $active_date;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getCustomerAcceptDate()
    {
        return $this->customer_accept_date;
    }

    /**
     * @param mixed $customer_accept_date
     */
    public function setCustomerAcceptDate($customer_accept_date)
    {
        $this->customer_accept_date = $customer_accept_date;
    }

    /**
     * @return mixed
     */
    public function getCustomerDeclineDate()
    {
        return $this->customer_decline_date;
    }

    /**
     * @param mixed $customer_decline_date
     */
    public function setCustomerDeclineDate($customer_decline_date)
    {
        $this->customer_decline_date = $customer_decline_date;
    }

    /**
     * @return mixed
     */
    public function getLastBillDate()
    {
        return $this->last_bill_date;
    }

    /**
     * @param mixed $last_bill_date
     */
    public function setLastBillDate($last_bill_date)
    {
        $this->last_bill_date = $last_bill_date;
    }

    /**
     * @return mixed
     */
    public function getNextBillDate()
    {
        return $this->next_bill_date;
    }

    /**
     * @param mixed $next_bill_date
     */
    public function setNextBillDate($next_bill_date)
    {
        $this->next_bill_date = $next_bill_date;
    }


    static function getCurrentAccount($request, $em)
    {
        $uri = explode('/', $request->getRequestUri());
        $accountId = (int)$uri[1];

        if (is_integer($accountId) && $accountId > 0) {
            $account = $em->getRepository('TradeServeAppBundle:Account')->find($accountId);

            return $account;
        }

        return false;
    }

    /**
     * Add organizations
     *
     * @param Organization $organizations
     * @return Product
     */
    public function addFactorRatingOption(Organization $organizations)
    {
        $this->organizations[] = $organizations;

        return $this;
    }

    /**
     * Remove organizations
     *
     * @param Organization $organizations
     */
    public function removeFactorRatingOption(Organization $organizations)
    {
        $this->organizations->removeElement($organizations);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFactorRatingOptions()
    {
        return $this->organizations;
    }

}
