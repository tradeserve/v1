<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/2/15
 * Time: 2:42 PM
 */

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleAttribute
 *
 * @ORM\Table(name="ArticleAttribute")
 * @ORM\Entity
 */
class ArticleAttribute
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="attributes")
     */
    private $article;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $attribute;

    /**
     * @ORM\Column(type="string")
     */
    private $value;

    public function __construct($name, $value, $article)
    {
        $this->attribute = $name;
        $this->value = $value;
        $this->article = $article;
    }
}