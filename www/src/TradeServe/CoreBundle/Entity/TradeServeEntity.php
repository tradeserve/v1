<?php
namespace TradeServe\CoreBundle\Entity;

use TradeServe\CoreBundle\Common\Common;

class TradeServeEntity
{
    public function __call($method, $args)
    {
        if (strpos($method, 'getSerialized') === 0) {
            if (substr($method, -3) == 'Ids') {
                $access = 'get' . substr($method, 13, -3) . 's';

                return Common::getEntityIds($this->$access());
            } else {
                if (substr($method, -2) == 'Id') {
                    $access = 'get' . substr($method, 13, -2);

                    return Common::getEntityId($this->$access());
                }
            }
        }
        throw new \BadMethodCallException('Attempt to call method ' . $method . ' which does not exist on class ' . get_class($this));
    }
} 