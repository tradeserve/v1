<?php

namespace TradeServe\CoreBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Model\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="Role")
 * @ORM\Entity
 */
class Role extends TradeServeEntity implements RoleInterface, EntityInterface, \Serializable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, length=30, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", unique=true, length=20, nullable=false)
     */
    protected $role;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedUserIds")
     */
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="account_roles")
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedAcctUserIds")
     */
    protected $acct_users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add users
     *
     * @param \TradeServe\CoreBundle\Entity\User $users
     *
     * @return Role
     */
    public function addUser(\TradeServe\CoreBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \TradeServe\CoreBundle\Entity\User $users
     */
    public function removeUser(\TradeServe\CoreBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $users field !
         */
        return \serialize(array(
           $this->id,
           $this->role
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list($this->id, $this->role) = \unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getAcctUsers()
    {
        return $this->acct_users;
    }

    /**
     * @param mixed $acct_users
     * @return Role
     */
    public function setAcctUsers($acct_users)
    {
        $this->acct_users = $acct_users;
        return $this;
    }
}
