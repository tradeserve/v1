<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Customer
 *
 * @ORM\Table(name="Customer")
 * @ORM\Entity(repositoryClass="TradeServe\CoreBundle\Repository\CustomerRepository")
 *
 */
class Customer extends TradeServeEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"internal"})
     * @Exclude
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="customers")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * @Groups({"internal"})
     * @type("string")
     * @Accessor(getter="getOrganization")
     * @Exclude
     */
    protected $organization;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $last_name;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id")
     * @Groups({"internal", "call"})
     * @Type("string")
     * @Accessor(getter="getAddress")
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $work_phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $home_phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $mobile_phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $email;

    /**
     * @ORM\Column(type="text",length=8000, nullable=true)
     * @Groups({"internal", "call"})
     */
    protected $note;

    /**
     * @ORM\OneToMany(targetEntity="ServiceRequest", mappedBy="customer")
     * @Groups({"internal"})
     * @Type("array<TradeServe\CoreBundle\Entity\ServiceRequest>")
     * @Accessor(getter="getServiceRequests")
     */
    protected $service_requests;

    /**
     * @ORM\OneToMany(targetEntity="ServiceAgreement", mappedBy="customer")
     * @Groups({"internal"})
     * @Type("array<TradeServe\CoreBundle\Entity\ServiceAgreement>")
     * @Accessor(getter="getServiceAgreements")
     */
    protected $service_agreements;


    public function __construct()
    {
        $this->service_requests = new ArrayCollection();
        $this->service_agreements = new ArrayCollection();
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return Customer
     */
    public function setOrganization(\TradeServe\CoreBundle\Entity\Organization $organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     * @return Customer
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     * @return Customer
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceRequests()
    {
        if (!empty($this->service_requests)) {
            return $this->service_requests->toArray();
        }

        return $this->service_requests;
    }

    /**
     * @param mixed $service_requests
     * @return Customer
     */
    public function setServiceRequests($service_requests)
    {
        $this->service_requests = $service_requests;
        return $this;
    }

    /**
     * @param mixed $service_agreements
     * @return Customer
     */
    public function setServiceAgreements($service_agreements)
    {
        $this->service_agreements = $service_agreements;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getServiceAgreements()
    {
        if (!empty($this->service_agreements)) {
            return $this->service_agreements->toArray();
        }

        return $this->service_agreements;
    }

    /**
     * @return mixed
     */
    public function getWorkPhone()
    {
        return $this->work_phone;
    }

    /**
     * @param mixed $work_phone
     * @return Customer
     */
    public function setWorkPhone($work_phone)
    {
        $this->work_phone = $work_phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomePhone()
    {
        return $this->home_phone;
    }

    /**
     * @param mixed $home_phone
     * @return Customer
     */
    public function setHomePhone($home_phone)
    {
        $this->home_phone = $home_phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobilePhone()
    {
        return $this->mobile_phone;
    }

    /**
     * @param mixed $mobile_phone
     * @return Customer
     */
    public function setMobilePhone($mobile_phone)
    {
        $this->mobile_phone = $mobile_phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return Customer
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }
}
