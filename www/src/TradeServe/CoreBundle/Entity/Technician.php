<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Technician
 *
 * @ORM\Table(name="Technician")
 * @ORM\Entity
 */
class Technician
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="technicians")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;

    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="Specialty")
     * @ORM\JoinTable(name="TechnicianSpecialty",
     *      joinColumns={@ORM\JoinColumn(name="technician_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="speciality_id", referencedColumnName="id")}
     *      )
     */
    protected $specialities;


    /**
     * @ORM\OneToMany(targetEntity="Dispatch", mappedBy="technician")
     */
    protected $dispatches;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $last_name;

    /**
     * @ORM\Column(type = "datetime", nullable = false)
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updated;

    public function __construct()
    {
        $this->dispatches = new ArrayCollection();
        $this->specialities = new ArrayCollection();
        $this->updatedTimestamps();
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
            $this->setUpdated(new \DateTime('now'));
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return Technician
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialities()
    {
        return $this->specialities;
    }

    /**
     * @param mixed $specialities
     * @return Technician
     */
    public function setSpecialities($specialities)
    {
        $this->specialities = $specialities;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     * @return Technician
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     * @return Technician
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Technician
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Technician
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Technician
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDispatches()
    {
        return $this->dispatches;
    }

    /**
     * @param mixed $dispatches
     * @return Technician
     */
    public function setDispatches($dispatches)
    {
        $this->dispatches = $dispatches;
        return $this;
    }

    /**
     *
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }
}
