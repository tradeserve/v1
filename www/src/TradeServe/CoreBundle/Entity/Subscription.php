<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Subscription
 *
 * @ORM\Table(name="Subscription")
 * @ORM\Entity(repositoryClass="TradeServe\CoreBundle\Repository\SubscriptionRepository")
 */
class Subscription
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $billing_id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"api"})
     */
    private $product_name;

    /**
     * @ORM\Column(type="string")
     * @Groups({"api"})
     */
    private $product_handle;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $activated_at;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $balance_in_cents;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"api"})
     */
    private $cancel_at_end_of_period;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"api"})
     */
    private $canceled_at;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $expires_at;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $next_assessment_at;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $product_price_in_cents;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $signup_payment_id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"api"})
     */
    private $signup_revenue;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $state;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $total_revenue_in_cents;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"api"})
     */
    private $trial_started_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"api"})
     */
    private $trial_ended_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"api"})
     */
    private $updated_at;

//    /**
//     * @ORM\OneToMany(targetEntity="Vennli\AccountModelBundle\Entity\SubscriptionToFeatureComponent", mappedBy="subscription")
//     * @Groups({"api"})
//     * @Type("array<integer>")
//     * @Accessor(getter="getSerializedSubscriptionToFeatureComponentIds")
//     */
//    protected $subscription_to_feature_component;
//
//    /**
//     * @ORM\OneToMany(targetEntity="Vennli\AccountModelBundle\Entity\SubscriptionToQtyComponent", mappedBy="subscription")
//     * @Groups({"api"})
//     * @Type("array<integer>")
//     * @Accessor(getter="getSerializedSubscriptionToQtyComponentIds")
//     */
//    protected $subscription_to_qty_component;
//
//    /**
//     * @ORM\OneToMany(targetEntity="Vennli\AccountModelBundle\Entity\SubscriptionToUsageComponent", mappedBy="subscription")
//     * @Groups({"api"})
//     * @Type("array<integer>")
//     * @Accessor(getter="getSerializedSubscriptionToUsageComponentIds")
//     */
//    protected $subscription_to_usage_component;
//
//    /**
//     * @ORM\ManyToOne(targetEntity="User", inversedBy="subscriptions")
//     * @ORM\JoinColumn(name="customer_reference", referencedColumnName="id", nullable=true)
//     * @Groups({"api"})
//     * @Type("integer")
//     */
//    private $customer_reference;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set billing_id
     *
     * @param integer $billingId
     * @return Subscription
     */
    public function setBillingId($billingId)
    {
        $this->billing_id = $billingId;

        return $this;
    }

    /**
     * Get billing_id
     *
     * @return integer
     */
    public function getBillingId()
    {
        return $this->billing_id;
    }

    /**
     * Set product_name
     *
     * @param string $productName
     * @return Subscription
     */
    public function setProductName($productName)
    {
        $this->product_name = $productName;

        return $this;
    }

    /**
     * Get product_name
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * Set product_handle
     *
     * @param string $productHandle
     * @return Subscription
     */
    public function setProductHandle($productHandle)
    {
        $this->product_handle = $productHandle;

        return $this;
    }

    /**
     * Get product_handle
     *
     * @return string
     */
    public function getProductHandle()
    {
        return $this->product_handle;
    }

    /**
     * Set activated_at
     *
     * @param \DateTime $activatedAt
     * @return Subscription
     */
    public function setActivatedAt($activatedAt)
    {
        $this->activated_at = $activatedAt;

        return $this;
    }

    /**
     * Get activated_at
     *
     * @return \DateTime
     */
    public function getActivatedAt()
    {
        return $this->activated_at;
    }

    /**
     * Set balance_in_cents
     *
     * @param integer $balanceInCents
     * @return Subscription
     */
    public function setBalanceInCents($balanceInCents)
    {
        $this->balance_in_cents = $balanceInCents;

        return $this;
    }

    /**
     * Get balance_in_cents
     *
     * @return integer
     */
    public function getBalanceInCents()
    {
        return $this->balance_in_cents;
    }

    /**
     * Set cancel_at_end_of_period
     *
     * @param boolean $cancelAtEndOfPeriod
     * @return Subscription
     */
    public function setCancelAtEndOfPeriod($cancelAtEndOfPeriod)
    {
        $this->cancel_at_end_of_period = $cancelAtEndOfPeriod;

        return $this;
    }

    /**
     * Get cancel_at_end_of_period
     *
     * @return boolean
     */
    public function getCancelAtEndOfPeriod()
    {
        return $this->cancel_at_end_of_period;
    }

    /**
     * Set canceled_at
     *
     * @param \DateTime $canceledAt
     * @return Subscription
     */
    public function setCanceledAt($canceledAt)
    {
        $this->canceled_at = $canceledAt;

        return $this;
    }

    /**
     * Get canceled_at
     *
     * @return \DateTime
     */
    public function getCanceledAt()
    {
        return $this->canceled_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Subscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set expires_at
     *
     * @param \DateTime $expiresAt
     * @return Subscription
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * Get expires_at
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * Set next_assessment_at
     *
     * @param \DateTime $nextAssessmentAt
     * @return Subscription
     */
    public function setNextAssessmentAt($nextAssessmentAt)
    {
        $this->next_assessment_at = $nextAssessmentAt;

        return $this;
    }

    /**
     * Get next_assessment_at
     *
     * @return \DateTime
     */
    public function getNextAssessmentAt()
    {
        return $this->next_assessment_at;
    }

    /**
     * Set product_price_in_cents
     *
     * @param integer $productPriceInCents
     * @return Subscription
     */
    public function setProductPriceInCents($productPriceInCents)
    {
        $this->product_price_in_cents = $productPriceInCents;

        return $this;
    }

    /**
     * Get product_price_in_cents
     *
     * @return integer
     */
    public function getProductPriceInCents()
    {
        return $this->product_price_in_cents;
    }

    /**
     * Set signup_payment_id
     *
     * @param integer $signupPaymentId
     * @return Subscription
     */
    public function setSignupPaymentId($signupPaymentId)
    {
        $this->signup_payment_id = $signupPaymentId;

        return $this;
    }

    /**
     * Get signup_payment_id
     *
     * @return integer
     */
    public function getSignupPaymentId()
    {
        return $this->signup_payment_id;
    }

    /**
     * Set signup_revenue
     *
     * @param float $signupRevenue
     * @return Subscription
     */
    public function setSignupRevenue($signupRevenue)
    {
        $this->signup_revenue = $signupRevenue;

        return $this;
    }

    /**
     * Get signup_revenue
     *
     * @return float
     */
    public function getSignupRevenue()
    {
        return $this->signup_revenue;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Subscription
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set total_revenue_in_cents
     *
     * @param integer $totalRevenueInCents
     * @return Subscription
     */
    public function setTotalRevenueInCents($totalRevenueInCents)
    {
        $this->total_revenue_in_cents = $totalRevenueInCents;

        return $this;
    }

    /**
     * Get total_revenue_in_cents
     *
     * @return integer
     */
    public function getTotalRevenueInCents()
    {
        return $this->total_revenue_in_cents;
    }

    /**
     * Set trial_started_at
     *
     * @param \DateTime $trialStartedAt
     * @return Subscription
     */
    public function setTrialStartedAt($trialStartedAt)
    {
        $this->trial_started_at = $trialStartedAt;

        return $this;
    }

    /**
     * Get trial_started_at
     *
     * @return \DateTime
     */
    public function getTrialStartedAt()
    {
        return $this->trial_started_at;
    }

    /**
     * Set trial_ended_at
     *
     * @param \DateTime $trialEndedAt
     * @return Subscription
     */
    public function setTrialEndedAt($trialEndedAt)
    {
        $this->trial_ended_at = $trialEndedAt;

        return $this;
    }

    /**
     * Get trial_ended_at
     *
     * @return \DateTime
     */
    public function getTrialEndedAt()
    {
        return $this->trial_ended_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Subscription
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

//    /**
//     * Add subscription_to_feature_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToFeatureComponent $subscriptionToFeatureComponent
//     * @return Subscription
//     */
//    public function addSubscriptionToFeatureComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToFeatureComponent $subscriptionToFeatureComponent
//    ) {
//        $this->subscription_to_feature_component[] = $subscriptionToFeatureComponent;
//
//        return $this;
//    }
//
//    /**
//     * Remove subscription_to_feature_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToFeatureComponent $subscriptionToFeatureComponent
//     */
//    public function removeSubscriptionToFeatureComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToFeatureComponent $subscriptionToFeatureComponent
//    ) {
//        $this->subscription_to_feature_component->removeElement($subscriptionToFeatureComponent);
//    }
//
//    /**
//     * Get subscription_to_feature_component
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getSubscriptionToFeatureComponent()
//    {
//        return $this->subscription_to_feature_component;
//    }
//
//    /**
//     * Add subscription_to_qty_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToQtyComponent $subscriptionToQtyComponent
//     * @return Subscription
//     */
//    public function addSubscriptionToQtyComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToQtyComponent $subscriptionToQtyComponent
//    ) {
//        $this->subscription_to_qty_component[] = $subscriptionToQtyComponent;
//
//        return $this;
//    }
//
//    /**
//     * Remove subscription_to_qty_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToQtyComponent $subscriptionToQtyComponent
//     */
//    public function removeSubscriptionToQtyComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToQtyComponent $subscriptionToQtyComponent
//    ) {
//        $this->subscription_to_qty_component->removeElement($subscriptionToQtyComponent);
//    }
//
//    /**
//     * Get subscription_to_qty_component
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getSubscriptionToQtyComponent()
//    {
//        return $this->subscription_to_qty_component;
//    }
//
//    /**
//     * Add subscription_to_usage_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToUsageComponent $subscriptionToUsageComponent
//     * @return Subscription
//     */
//    public function addSubscriptionToUsageComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToUsageComponent $subscriptionToUsageComponent
//    ) {
//        $this->subscription_to_usage_component[] = $subscriptionToUsageComponent;
//
//        return $this;
//    }
//
//    /**
//     * Remove subscription_to_usage_component
//     *
//     * @param \Vennli\AccountModelBundle\Entity\SubscriptionToUsageComponent $subscriptionToUsageComponent
//     */
//    public function removeSubscriptionToUsageComponent(
//       \Vennli\AccountModelBundle\Entity\SubscriptionToUsageComponent $subscriptionToUsageComponent
//    ) {
//        $this->subscription_to_usage_component->removeElement($subscriptionToUsageComponent);
//    }
//
//    /**
//     * Get subscription_to_usage_component
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getSubscriptionToUsageComponent()
//    {
//        return $this->subscription_to_usage_component;
//    }
//
//    /**
//     * Set customer_reference
//     *
//     * @param \Vennli\AccountModelBundle\Entity\User $customerReference
//     * @return Subscription
//     */
//    public function setCustomerReference(\Vennli\AccountModelBundle\Entity\User $customerReference)
//    {
//        $this->customer_reference = $customerReference;
//
//        return $this;
//    }
//
//    /**
//     * Get customer_reference
//     *
//     * @return \Vennli\AccountModelBundle\Entity\User
//     */
//    public function getCustomerReference()
//    {
//        return $this->customer_reference;
//    }
}