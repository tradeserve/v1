<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessageDeliveryType
 *
 * @ORM\Table(name="MessageDeliveryType")
 * @ORM\Entity
 */
class MessageDeliveryType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="string",length=20) */
    protected $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
