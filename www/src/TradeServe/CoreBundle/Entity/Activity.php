<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TradeServe\CoreBundle\Model\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;

/**
 * @ORM\Entity
 * @ORM\Table(name="Activity")
 */
class Activity extends TradeServeEntity implements EntityInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Groups({"api"})
     */
    private $activity_date;

    /**
     * @ORM\Column(type="string", length=8000, nullable=false)
     * @Groups({"api"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({"api"})
     * @Type("integer")
     * @Accessor(getter="getSerializedUserId")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="activities")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * @Groups({"api"})
     * @Type("integer")
     * @Accessor(getter="getSerializedOrganizationId")
     */
    protected $organization;

    /**
     * @ORM\Column(name="entity_id", type="integer", length=11)
     * @Groups({"api"})
     */
    protected $entity_id;

    /**
     * @ORM\Column(name="entity_class", type="text", length=255)
     * @Groups({"api"})
     */
    protected $entity_class;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;

    }

    /**
     * @return mixed
     */
    public function getActivityDate()
    {
        return $this->activity_date;
    }

    /**
     * @param mixed $activity_date
     * @return Activity
     */
    public function setActivityDate($activity_date)
    {
        $this->activity_date = $activity_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Activity
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return Activity
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * @param mixed $entity_id
     * @return Activity
     */
    public function setEntityId($entity_id)
    {
        $this->entity_id = $entity_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entity_class;
    }

    /**
     * @param mixed $entity_class
     * @return Activity
     */
    public function setEntityClass($entity_class)
    {
        $this->entity_class = $entity_class;
        return $this;
    }
}
