<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\ServiceRequestEvent;
use TradeServe\CoreBundle\Common\Common;

/**
 * ServiceRequestEvent
 *
 * @ORM\Table(name="DispatchEvent")
 * @ORM\Entity(repositoryClass="TradeServe\CoreBundle\Repository\DispatchEventRepository")
 */
class DispatchEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"internal"})
     * @Exclude
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Dispatch", inversedBy="events")
     */
    protected $dispatch;

    /**
     * @ORM\ManyToOne(targetEntity="Technician")
     * @ORM\JoinColumn(name="tech_id")
     */
    protected $tech;

    /**
     * @ORM\ManyToOne(targetEntity="DispatchEventType")
     * @ORM\JoinColumn(name="type_id")
     * @Groups({"internal","call"})
     * @Type("string")
     * @Accessor(getter="getType")
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Groups({"internal","call"})
     * @Type("DateTime")
     * @Accessor(getter="getCreated")
     */
    protected $created;


    public function __construct($type, $tech, $dispatch)
    {
        $this->created = new \DateTime('now');
        $this->type = $type;
        $this->tech = $tech;
        $this->dispatch = $dispatch;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDispatch()
    {
        return $this->dispatch;
    }

    /**
     * @param mixed $dispatch
     * @return Dispatch
     */
    public function setDispatch($dispatch)
    {
        $this->dispatch = $dispatch;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTech()
    {
        return $this->tech;
    }

    /**
     * @param mixed $tech
     * @return DispatchEvent
     */
    public function setTech($tech)
    {
        $this->tech = $tech;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return DispatchEvent
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return DispatchEvent
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }
}
