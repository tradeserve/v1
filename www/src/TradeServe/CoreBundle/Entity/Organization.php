<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Organization
 *
 * @ORM\Table(name="Organization")
 * @ORM\Entity
 */
class Organization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=150)
     */
    protected $name;

    /** @ORM\Column(type="string",length=255) */
    protected $url;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id")
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="organizations")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;

    /** @ORM\Column(type="string",length=15, nullable=true) */
    protected $phone;

    /** @ORM\Column(type="string",length=15, nullable=true) */
    protected $fax;

    /** @ORM\Column(type="string",length=100, nullable=true) */
    protected $industry;

    /** @ORM\Column(type="string",length=100,nullable=true) */
    protected $timezone;

    /** @ORM\Column(type="string",length=100,nullable=true) */
    protected $image;

    /**
     * @ORM\Column(type="integer")
     */
    protected $active;

    /** @ORM\Column(type="string",length=300, nullable=true) */
    protected $from_email;

    /** @ORM\Column(type="string",length=300, nullable=true) */
    protected $from_name;

    /** @ORM\Column(type="string",length=15, nullable=true) */
    protected $from_number;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $twilio_account_sid;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $twilio_auth_token;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedProductIds")
     */
    protected $products;

    /**
     * @ORM\OneToMany(targetEntity="Coupon", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedCouponIds")
     */
    protected $coupons;

    /**
     * @ORM\OneToMany(targetEntity="TradeServe\CoreBundle\Entity\Customer", mappedBy="organization", cascade={"remove"}, orphanRemoval=true)
     * @Groups({"api"})
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedCustomerIds")
     */
    protected $customers;

    /**
     * @ORM\OneToMany(targetEntity="Department", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedDepartmentIds")
     */
    protected $departments;

    /**
     * @ORM\OneToMany(targetEntity="ServiceRequest", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedServiceRequestIds")
     */
    protected $service_requests;

    /**
     * @ORM\OneToMany(targetEntity="Dispatch", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedDispatchIds")
     */
    protected $dispatches;

    /**
     * @ORM\OneToMany(targetEntity="Technician", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedTechnicianIds")
     */
    protected $technicians;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="organization")
     *
     * @Type("array<integer>")
     * @Accessor(getter="getSerializedUserIds")
     */
    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->service_requests = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->coupons = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->dispatches = new ArrayCollection();
        $this->technicians = new ArrayCollection();
        $this->timezone = 'America/Chicago';
        $this->updatedTimestamps();
        $this->active = 1;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $industry
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    }

    /**
     * @return mixed
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add $account
     *
     * @param \TradeServe\CoreBundle\Entity\Account $account
     *
     * @return Account
     */
    public function addAccount(Account $account)
    {
        $this->account[] = $account;

        return $this;
    }

    /**
     * Remove $account
     *
     * @param \TradeServe\CoreBundle\Entity\Account $account
     */
    public function removeAccount(Account $account)
    {
        $this->account->removeElement($account);
    }

    /**
     * @param mixed $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccount()
    {
        /*if (!empty($this->account)) {
            return $this->account->toArray();
        }*/

        return $this->account;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Render as string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getFromEmail()
    {
        return $this->from_email;
    }

    /**
     * @param mixed $from_email
     */
    public function setFromEmail($from_email)
    {
        $this->from_email = $from_email;
    }

    /**
     * @return mixed
     */
    public function getFromName()
    {
        return $this->from_name;
    }

    /**
     * @param mixed $from_name
     */
    public function setFromName($from_name)
    {
        $this->from_name = $from_name;
    }

    /**
     * @return mixed
     */
    public function getFromNumber()
    {
        return $this->from_number;
    }

    /**
     * @param mixed $from_number
     */
    public function setFromNumber($from_number)
    {
        $this->from_number = $from_number;
    }

    /**
     * @return mixed
     */
    public function getTwilioAccountSid()
    {
        return $this->twilio_account_sid;
    }

    /**
     * @param mixed $twilio_account_sid
     */
    public function setTwilioAccountSid($twilio_account_sid)
    {
        $this->twilio_account_sid = $twilio_account_sid;
    }

    /**
     * @return mixed
     */
    public function getTwilioAuthToken()
    {
        return $this->twilio_auth_token;
    }

    /**
     * @param mixed $twilio_auth_token
     */
    public function setTwilioAuthToken($twilio_auth_token)
    {
        $this->twilio_auth_token = $twilio_auth_token;
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        $this->setUpdated(new \DateTime('now'));

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
        }
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     * @return Organization
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * @param mixed $coupons
     * @return Organization
     */
    public function setCoupons($coupons)
    {
        $this->coupons = $coupons;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomers()
    {
        if (!empty($this->customers)) {
            return $this->customers->toArray();
        }
        return $this->customers;
    }

    /**
     * @param mixed $customers
     * @return Organization
     */
    public function setCustomers($customers)
    {
        $this->customers = $customers;
        return $this;
    }

    /**
     * Add customers
     *
     * @param \TradeServe\CoreBundle\Entity\Customer $customers
     *
     * @return Organization
     */
    public function addCustomer(\TradeServe\CoreBundle\Entity\Customer $customers)
    {
        $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \TradeServe\CoreBundle\Entity\Customer $customers
     */
    public function removeCustomer(\TradeServe\CoreBundle\Entity\Customer $customers)
    {
        $this->customers->removeElement($customers);
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param mixed $departments
     * @return Organization
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceRequests()
    {
        return $this->service_requests;
    }

    /**
     * @param mixed $service_requests
     * @return Organization
     */
    public function setServiceRequests($service_requests)
    {
        $this->service_requests = $service_requests;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTechnicians()
    {
        return $this->technicians;
    }

    /**
     * @param mixed $technicians
     * @return Organization
     */
    public function setTechnicians($technicians)
    {
        $this->technicians = $technicians;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return Organization
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDispatches()
    {
        return $this->dispatches;
    }

    /**
     * @param mixed $dispatches
     * @return Organization
     */
    public function setDispatches($dispatches)
    {
        $this->dispatches = $dispatches;
        return $this;
    }

}
