<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use TradeServe\CoreBundle\Entity\UserRole;
USE TradeServe\CoreBundle\Model\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use TradeServe\CoreBundle\Common\Common;

/**
 * User
 *
 * @ORM\Table(name="User")
 * @ORM\Entity()
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="users")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\Role", inversedBy="acct_users")
     * @ORM\JoinTable(name="UserAccountRole")
     */
    protected $account_roles;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $salt;

    /**
     * @ORM\Column(type="string")
     */
    protected $last_name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_online_date;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $username;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\Account", inversedBy="users")
     * @ORM\JoinTable(name="UserAccount")
     */
    protected $accounts;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $password_token;

    /**
     * @ORM\ManyToMany(targetEntity="TradeServe\CoreBundle\Entity\Role", inversedBy="users")
     * @ORM\JoinTable(name="UserRole")
     */
    protected $roles;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $join_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $user_ip;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $password_token_expire;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $password_reset_date;


    public function __construct()
    {
        $this->isActive = true;
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->account_users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->accounts = new ArrayCollection();
        $this->setJoinDate(new \DateTime());
        $this->setPasswordResetDate(new \DateTime());
        $this->setSalt(md5(uniqid()));
        $this->setStatus(1);
        $this->updatedTimestamps();
    }

    /**
     *
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set last_online_date
     *
     * @param \DateTime $lastOnlineDate
     *
     * @return User
     */
    public function setLastOnlineDate($lastOnlineDate)
    {
        $this->last_online_date = $lastOnlineDate;

        return $this;
    }

    /**
     * Get last_online_date
     *
     * @return \DateTime
     */
    public function getLastOnlineDate()
    {
        return $this->last_online_date;
    }

    /**
     * Set join_date
     *
     * @param \DateTime $joinDate
     *
     * @return User
     */
    public function setJoinDate($joinDate)
    {
        $this->join_date = $joinDate;

        return $this;
    }

    /**
     * Get join_date
     *
     * @return \DateTime
     */
    public function getJoinDate()
    {
        return $this->join_date;
    }

    /**
     * Set user_ip
     *
     * @param string $userIp
     *
     * @return User
     */
    public function setUserIp($userIp)
    {
        $this->user_ip = $userIp;

        return $this;
    }

    /**
     * Used for interface, currently unused
     */
    public function eraseCredentials()
    {
        return;
    }

    /**
     * Returns the full name.
     *
     * @return string
     */
    public function getFullName()
    {
        if (isset($this->first_name) && isset($this->last_name)) {
            return $this->getFirstName() . ' ' . $this->getLastName();
        }

        return $this->getUsername();
    }

    /**
     * Get user_ip
     *
     * @return string
     */
    public function getUserIp()
    {
        return $this->user_ip;
    }

    /**
     * @param mixed $first_name
     *
     * @return mixed $first_name
     *
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $last_name
     *
     * @return mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $password
     *
     * @return mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add roles
     *
     * @param Role $roles
     *
     * @return User
     */
    public function addRole(Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param UserRole $roles
     */
    public function removeRole(Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        if (!empty($this->roles)) {
            return $this->roles->toArray();
        }

        return $this->roles;
    }

    /**
     * Returns TRUE if users have same username
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     *
     * @return boolean
     */
    public function equals(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        if ($user->getUsername() == $this->getUsername()) {
            return true;
        }

        return false;
    }

    /**
     * Generates a random string.
     *
     * @param int $length
     *
     * @return string
     */
    public static function random_password($length = 12)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);

        return $password;
    }

    /**
     *  Get Roles as array of Ids
     *
     * @return $ids array of integers
     */
    public function getRoleIds()
    {
        $ids = array();

        if (!empty($this->roles)) {
            foreach ($this->roles as $role) {
                $ids[] = $role->getId();
            }
        }

        return $ids;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Returns TRUE if user is active
     *
     * @return boolean
     */
    public function isActive()
    {
        if ($this->getStatus() == 1) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */
        return \serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->salt,
            $this->password,
            $this->status
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list ($this->id, $this->username, $this->email, $this->salt, $this->password, $this->status) = \unserialize($serialized);
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    public function isAdmin()
    {
        $roles = $this->getRoles();

        foreach ($roles as $role) {
            if ($role->getRoles() == 'ROLE_ADMIN' || $role->getRoles() == 'ROLE_SUPER_ADMIN') {
                return true;
            }
        }

        return false;
    }

    public function isSupport()
    {
        return $this->hasRole('ROLE_SUPPORT');
    }

    public function isConsultant()
    {
        return $this->hasRole('ROLE_CONSULTANT');
    }

    public function hasRole($roleName)
    {
        $roles = $this->getRoles();
        foreach ($roles as $role) {
            if ($role->getRole() == $roleName) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add $account_roles
     *
     * @param \TradeServe\CoreBundle\Entity\Role $account_roles
     *
     * @return User
     */
    public function addAccountRole(\TradeServe\CoreBundle\Entity\Role $account_roles)
    {
        $this->account_roles[] = $account_roles;

        return $this;
    }

    /**
     * Remove $account_roles
     *
     * @param \TradeServe\CoreBundle\Entity\Role $account_roles
     */
    public function removeAccountRole(\TradeServe\CoreBundle\Entity\Role $account_roles)
    {
        $this->account_roles->removeElement($account_roles);
    }

    /**
     * @param mixed $account_roles
     */
    public function setAccountRoles($account_roles)
    {
        $this->account_roles = $account_roles;
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccountRoles()
    {
        if (!empty($this->account_roles)) {
            return $this->account_roles->toArray();
        }

        return $this->account_roles;
    }

    /**
     * Add $accounts
     *
     * @param \TradeServe\CoreBundle\Entity\Account $accounts
     *
     * @return User
     */
    public function addAccount(\TradeServe\CoreBundle\Entity\Account $accounts)
    {
        $this->accounts[] = $accounts;

        return $this;
    }

    /**
     * Remove $accounts
     *
     * @param \TradeServe\CoreBundle\Entity\Account $accounts
     */
    public function removeAccount(\TradeServe\CoreBundle\Entity\Account $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * @param mixed $accounts
     */
    public function setAccounts($accounts)
    {
        $this->accounts = $accounts;
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccounts()
    {
        if (!empty($this->accounts)) {
            return $this->accounts->toArray();
        }

        return $this->accounts;
    }


    public function checkPassword($encoder, $password)
    {
        return ($this->password == $encoder->getEncoder($this)
                ->encodePassword('password', $this->getSalt())) ? true : false;
    }

    /**
     * Sets the user password token.
     *
     * @return string
     */
    public function setPasswordToken()
    {
        $this->password_token = uniqid();
        $this->password_token_expire = time() + 3600;
    }

    /**
     * Returns the user password token.
     *
     * @return string
     */
    public function getPasswordToken()
    {
        return $this->password_token;
    }

    /**
     *  Check if a token has expired
     */
    public function passwordTokenExpired()
    {
        if ($this->password_token_expire < time()) {
            return true;
        }

        return false;
    }

    /**
     * Set password_token_expire
     *
     * @param integer $passwordTokenExpire
     *
     * @return User
     */
    public function setPasswordTokenExpire($passwordTokenExpire)
    {
        $this->password_token_expire = $passwordTokenExpire;

        return $this;
    }

    /**
     * Get password_token_expire
     *
     * @return integer
     */
    public function getPasswordTokenExpire()
    {
        return $this->password_token_expire;
    }

    public function __call($method, $args)
    {
        if (strpos($method, 'getSerialized') === 0) {
            if (substr($method, -3) == 'Ids') {
                $access = 'get' . substr($method, 13, -3) . 's';
                return Common::getEntityIds($this->$access());
            } else {
                if (substr($method, -2) == 'Id') {
                    $access = 'get' . substr($method, 13, -2);
                    return Common::getEntityId($this->$access());
                }
            }
        }
        throw new \BadMethodCallException('Attempt to call method ' . $method . ' which does not exist on class ' . get_class($this));
    }

    /**
     * @return mixed
     */
    public function getPasswordResetDate()
    {
        return $this->password_reset_date;
    }

    /**
     * @param mixed $password_reset_date
     */
    public function setPasswordResetDate($password_reset_date)
    {
        $this->password_reset_date = $password_reset_date;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return User
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }


}
