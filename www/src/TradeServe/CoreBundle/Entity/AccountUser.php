<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use Rhumsaa\Uuid\Uuid;
use TradeServe\CoreBundle\Model\EntityInterface;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Entity\User;
use TradeServe\CoreBundle\Entity\TradeServeEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="AccountUser")
 * @ORM\HasLifecycleCallbacks
 *
 *
 */
class AccountUser extends TradeServeEntity implements EntityInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $api_key;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @ORM\ManyToOne(targetEntity="TradeServe\CoreBundle\Entity\User", inversedBy="account_users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Groups({"api"})
     * @Type("integer")
     * @Accessor(getter="getSerializedUserId")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="account_users")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     * @Groups({"api"})
     * @Type("integer")
     * @Accessor(getter="getSerializedAccountId")
     */
    private $account;

//    /**
//     * @ORM\OneToMany(targetEntity="TradeServe\CoreBundle\Entity\Impersonation", mappedBy="account_user")
//     * @Groups({"api"})
//     * @Type("array<integer>")
//     * @Accessor(getter="getSerializationImpersonationIds")
//     */
//    private $impersonations;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"api"})
     */
    protected $is_account_manager;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->is_account_manager = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set api_key
     *
     * @param string $apiKey
     *
     * @return AccountUser
     */
    public function setApiKey($apiKey)
    {
        $this->api_key = $this->encrypt($apiKey);

        return $this;
    }

    /**
     * Get api_key
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->decrypt($this->api_key);
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return AccountUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return AccountUser
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set account
     *
     * @param \TradeServe\CoreBundle\Entity\Account $account
     *
     * @return AccountUser
     */
    public function setAccount(\TradeServe\CoreBundle\Entity\Account $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \TradeServe\CoreBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Encrypts the string with a 2 way hash.
     */
    private function encrypt($string)
    {
        return AccountUser::staticEncrypt($string);
    }

    public static function encryptParameter($key)
    {
        return AccountUser::staticEncrypt($key);
    }

    public static function staticEncrypt($string)
    {
        return openssl_encrypt($string, "AES-256-CBC", "25c6c7ff35b9979b151f2136cd13b0ff", 0, "3133363132343638");
    }

    /**
     * Decrypts the string with a 2 way hash.
     */
    private function decrypt($string)
    {
        return openssl_decrypt($string, "AES-256-CBC", "25c6c7ff35b9979b151f2136cd13b0ff", 0, "3133363132343638");
    }

    /**
     * Updates the api key
     */
    public function updateApiKey()
    {
        $this->setSalt(uniqid());
        $this->setApiKey(Uuid::uuid5(Uuid::NAMESPACE_DNS,
           $this->getSalt() . $this->getUser()->getEmail() . $this->getAccount()->getName()));
    }

    /**
     * @ORM\PrePersist
     *
     * Creates an apikey if not already set on persist
     */
    public function onPrePersist()
    {
        if (!($this->getApiKey())) {
            $this->updateApiKey();
        }
    }

//    /**
//     * Add impersonations
//     *
//     * @param \TradeServe\CoreBundle\Entity\Impersonation $impersonations
//     *
//     * @return AccountUser
//     */
//    public function addImpersonation(\TradeServe\CoreBundle\Entity\Impersonation $impersonations)
//    {
//        $this->impersonations[] = $impersonations;
//
//        return $this;
//    }

//    /**
//     * Remove impersonations
//     *
//     * @param \TradeServe\CoreBundle\Entity\Impersonation $impersonations
//     */
//    public function removeImpersonation(\TradeServe\CoreBundle\Entity\Impersonation $impersonations)
//    {
//        $this->impersonations->removeElement($impersonations);
//    }
//
//    /**
//     * Get impersonations
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getImpersonations()
//    {
//        return $this->impersonations;
//    }

    /**
     * Set is_account_manager
     *
     * @param boolean $isAccountManager
     *
     * @return AccountUser
     */
    public function setIsAccountManager($isAccountManager)
    {
        $this->is_account_manager = $isAccountManager;

        return $this;
    }

    /**
     * Get is_account_manager
     *
     * @return boolean
     */
    public function getIsAccountManager()
    {
        return $this->is_account_manager;
    }
}
