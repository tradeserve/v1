<?php

namespace TradeServe\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;
use Doctrine\Common\Collections\ArrayCollection;
use TradeServe\CoreBundle\Entity\Organization;
use TradeServe\CoreBundle\Entity\ServiceRequestEvent;
use TradeServe\CoreBundle\Common\Common;

/**
 * ServiceRequest
 *
 * @ORM\Table(name="ServiceRequest")
 * @ORM\Entity(repositoryClass="TradeServe\CoreBundle\Repository\ServiceRequestRepository")
 */
class ServiceRequest extends TradeServeEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"internal"})
     * @Accessor(getter="getId")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="service_requests")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * @Groups({"call"})
     * @type("string")
     * @Accessor(getter="getOrganization")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="service_requests")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * @Groups({"call"})
     * @Accessor(getter="getCustomer")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceRequestType")
     * @ORM\JoinColumn(name="service_request_type_id", referencedColumnName="id")
     * @Groups({"call"})
     * @Type("string")
     * @Accessor(getter="getType")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceRequestStatus")
     * @ORM\JoinColumn(name="service_request_status_id", referencedColumnName="id")
     * @Groups({"call"})
     * @Type("string")
     * @Accessor(getter="getStatus")
     */
    protected $status;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"call"})
     */
    protected $estimated_start_date;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"call"})
     */
    protected $estimated_start_time;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * @ORM\OneToMany(targetEntity="ServiceRequestEvent", mappedBy="service_request", cascade={"ALL"}, indexBy="event")
     * @Groups({"call"})
     * @Type("array<TradeServe\CoreBundle\Entity\ServiceRequestEvent>")
     * @Accessor(getter="getEvents")
     */
    protected $events;

    /**
     * @ORM\OneToMany(targetEntity="Dispatch", mappedBy="service_request")
     * @Groups({"call"})
     * @Type("array<TradeServe\CoreBundle\Entity\Dispatch>")
     * @Accessor(getter="getDispatches")
     */
    protected $dispatches;

//    /**
//     * @ORM\ManyToOne(targetEntity="ServiceRequestTechnicianStatus")
//     * @ORM\JoinColumn(name="service_request_status_id", referencedColumnName="id")
//     */
//    protected $tech_status;

    public function __construct()
    {
        $this->dispatches = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->updatedTimestamps();
    }

    /**
     * Function to update the timestamps
     */
    public function updatedTimestamps()
    {
        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime('now'));
            $this->setUpdated(new \DateTime('now'));
        }
    }

//    public function __call($method, $args)
//    {
//        if (strpos($method, 'getSerialized') === 0) {
//            if (substr($method, -3) == 'Ids') {
//                $access = 'get' . substr($method, 13, -3) . 's';
//                return Common::getEntityIds($this->$access());
//            } else {
//                if (substr($method, -2) == 'Id') {
//                    $access = 'get' . substr($method, 13, -2);
//                    return Common::getEntityId($this->$access());
//                }
//            }
//        }
//        throw new \BadMethodCallException('Attempt to call method ' . $method .
//           ' which does not exist on class ' . get_class($this));
//
//    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return ServiceRequest
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     * @return ServiceRequest
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return ServiceRequest
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return ServiceRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimatedStartDate()
    {
        return $this->estimated_start_date;
    }

    /**
     * @param mixed $estimated_start_date
     * @return ServiceRequest
     */
    public function setEstimatedStartDate($estimated_start_date)
    {
        $this->estimated_start_date = $estimated_start_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimatedStartTime()
    {
        return $this->estimated_start_time;
    }

    /**
     * @param mixed $estimated_start_time
     * @return ServiceRequest
     */
    public function setEstimatedStartTime($estimated_start_time)
    {
        $this->estimated_start_time = $estimated_start_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return ServiceRequest
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return ServiceRequest
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

//    /**
//     * Add event
//     *
//     * @param ServiceRequestEvent $event
//     *
//     * @return ServiceRequestEvent
//     */
//    public function addEvent(ServiceRequestEvent $event)
//    {
//        $this->events[] = $event;
//
//        return $this;
//
//    }

    public function addEvent($value, User $user)
    {
        $this->events[$value] = new ServiceRequestEvent($value, $user, $this);
    }

//    public function addEvent(ServiceRequestEvent $event)
//    {
//        $this->events[] = $event;
//        return $this;
//    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        if (!empty($this->events)) {
            return $this->events->toArray();
        }
        return $this->events;
    }

    /**
     * @return mixed
     */
    public function getDispatches()
    {
        if (!empty($this->dispatches)) {
            return $this->dispatches->toArray();
        }
        return $this->dispatches;
    }

    /**
     * @param mixed $dispatches
     * @return ServiceRequest
     */
    public function setDispatches($dispatches)
    {
        $this->dispatches = $dispatches;
        return $this;
    }

    /**
     * @param mixed $events
     * @return ServiceRequest
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */
        return \serialize(
            array(
                $this->id,
                $this->organization,
                $this->customer,
                $this->type,
                $this->dispatches,
                $this->estimated_start_date,
                $this->estimated_start_time,
                $this->events,
                $this->status
            )
        );
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list ($this->id,
            $this->organization,
            $this->customer,
            $this->type,
            $this->dispatches,
            $this->estimated_start_date,
            $this->estimated_start_time,
            $this->events,
            $this->status) = \unserialize(
            $serialized
        );
    }
}
