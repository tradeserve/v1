<?php

namespace TradeServe\CoreBundle\Model;

use TradeServe\CoreBundle\Model\EntityInterface;

/**
 */
Interface UserInterface extends EntityInterface, \Symfony\Component\Security\Core\User\UserInterface
{

   /**
    * Get id
    *
    * @return integer
    */
   public function getId();

   /**
    * Set email
    *
    * @param string $email
    *
    * @return User
    */
   public function setEmail($email);

   /**
    * Get email
    *
    * @return string
    */
   public function getEmail();

   /**
    * Set password
    *
    * @param string $password
    *
    * @return User
    */
   public function setPassword($password);

   /**
    * Get password
    *
    * @return string
    */
   public function getPassword();

   /**
    * Set salt
    *
    * @param string $salt
    *
    * @return User
    */
   public function setSalt($salt);

   /**
    * Get salt
    *
    * @return string
    */
   public function getSalt();

   /**
    * Set first_name
    *
    * @param string $firstName
    *
    * @return User
    */
   public function setFirstName($firstName);

   /**
    * Get first_name
    *
    * @return string
    */
   public function getFirstName();

   /**
    * Set last_name
    *
    * @param string $lastName
    *
    * @return User
    */
   public function setLastName($lastName);

   /**
    * Get last_name
    *
    * @return string
    */
   public function getLastName();

   /**
    * Set username
    *
    * @param string $username
    *
    * @return User
    */
   public function setUsername($username);

   /**
    * Get username
    *
    * @return string
    */
   public function getUsername();

   /**
    * Set last_online_date
    *
    * @param \DateTime $lastOnlineDate
    *
    * @return User
    */
   public function setLastOnlineDate($lastOnlineDate);

   /**
    * Get last_online_date
    *
    * @return \DateTime
    */
   public function getLastOnlineDate();

   /**
    * Set join_date
    *
    * @param \DateTime $joinDate
    *
    * @return User
    */
   public function setJoinDate($joinDate);

   /**
    * Get join_date
    *
    * @return \DateTime
    */
   public function getJoinDate();

   /**
    * Set status
    *
    * @param integer $status
    *
    * @return User
    */
   public function setStatus($status);

   /**
    * Get status
    *
    * @return integer
    */
   public function getStatus();

   /**
    * Set user_ip
    *
    * @param string $userIp
    *
    * @return User
    */
   public function setUserIp($userIp);

   /**
    * Get user_ip
    *
    * @return string
    */
   public function getUserIp();

   /**
    * Used for interface, currently unused
    */
   public function eraseCredentials();

   public function getFullName();

   /**
    * Returns TRUE if user is active
    *
    * @return boolean
    */
   public function isActive();

   /**
    *
    */
   public function __toString();

   /**
    * Returns TRUE if users have same username
    *
    * @return boolean
    */
   public function equals(\Symfony\Component\Security\Core\User\UserInterface $user);
}
