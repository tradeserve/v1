<?php

namespace TradeServe\CoreBundle\Model;

use TradeServe\CoreBundle\Model\EntityInterface;
use Symfony\Component\HttpFoundation\Request;

interface HandlerInterface
{

    /**
     * Get a entity given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return EntityInterface
     */
    public function get($id);

    /**
     * Get a list of entities.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($ids = null);

    /**
     * Post entity, creates a new entity.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function post(array $parameters);

    /**
     * Edit a entity.
     *
     * @api
     *
     * @param EntityInterface $entity
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function put(EntityInterface $entity, array $parameters);

    /**
     * Partially update a entity.
     *
     * @api
     *
     * @param EntityInterface $entity
     * @param array $parameters
     *
     * @return EntityInterface
     */
    public function patch(EntityInterface $entity, array $parameters);

}
