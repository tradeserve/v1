<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 1/20/16
 * Time: 6:51 PM
 */

namespace TradeServe\CoreBundle\Model;


/**
 * Used for outputting to activity log
 */
Interface LoggableInterface
{

    /**
     * Adds a log message when user creates an item
     *
     * @return String (log entry)
     */
    public function logCreate($user, $entity);

    /**
     * Adds a log message when user edits or marks inactive an item
     *
     * @return String (log entry)
     */
    public function logEdit($user, $entity, $newName);

    /**
     * Adds a log message when user deletes an item
     *
     * @return String (log entry)
     */
    public function logDelete($user, $entity);

    /**
     * Adds a log message when user deletes an item
     *
     * @return String (Organization)
     */
    public function logOrganization($entity);
}