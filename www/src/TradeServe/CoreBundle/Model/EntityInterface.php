<?php

namespace TradeServe\CoreBundle\Model;

/**
 */
Interface EntityInterface
{

    /**
     * Get id
     *
     * @return integer
     */
    public function getId();
}
