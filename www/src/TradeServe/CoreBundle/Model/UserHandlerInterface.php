<?php

namespace TradeServe\CoreBundle\Model;

use TradeServe\CoreBundle\Model\EntityInterface;
use Symfony\Component\HttpFoundation\Request;

interface UserHandlerInterface
{

   /**
    * Get a User given the identifier
    *
    * @api
    *
    * @param mixed $id
    *
    * @return EntityInterface
    */
   public function get($id);

   /**
    * Get a list of Users.
    *
    * @param int $limit the limit of the result
    * @param int $offset starting from the offset
    *
    * @return array
    */
   public function all();

   /**
    * Post User, creates a new User.
    *
    * @api
    *
    * @param array $parameters
    *
    * @return EntityInterface
    */
   public function post(array $parameters);

   /**
    * Edit a User.
    *
    * @api
    *
    * @param EntityInterface $user
    * @param array $parameters
    *
    * @return EntityInterface
    */
   public function put(EntityInterface $user, array $parameters);

   /**
    * Partially update a User.
    *
    * @api
    *
    * @param EntityInterface $user
    * @param array $parameters
    *
    * @return EntityInterface
    */
   public function patch(EntityInterface $user, array $parameters);

}
