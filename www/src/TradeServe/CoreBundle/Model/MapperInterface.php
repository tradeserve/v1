<?php

namespace TradeServe\CoreBundle\Model;

use DateTime;
use TradeServe\CoreBundle\Entity\Message;

interface MapperInterface
{
   public function commit();

   public function abort();

   public function saveBatch(array $messages);

   public function queueBatch(array $messages);
}