<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 9:12 PM
 */

namespace TradeServe\ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends Controller
{
    /**
     * Fetch a Entity or throw an 404 Exception.
     *
     * @param string $entity
     * @param int $id
     *
     * @return EntityInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($entity, $id)
    {
        if (!($entity = $this->container->get('trade_serve.' . $entity . '.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }

        return $entity;
    }

    /**
     * Serializes $data to $method type using the 'api' serialization group
     *
     * @param $data
     * @param string $method
     * @param string $group
     *
     * @return mixed
     */
    protected function serialize($data, $method = 'json', $group = 'internal')
    {
        $serializer = $this->container->get('trade_serve_serializer');

        return $serializer->serialize($data, $method, $group);
    }
}
