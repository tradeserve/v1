<?php

namespace TradeServe\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormTypeInterface;
use TradeServe\CoreBundle\Exception\InvalidFormException;
use TradeServe\CoreBundle\Model\EntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TradeServe\CoreBundle\Entity\Organization;

class DefaultController extends ApiController
{

    /**
     * Returns all entities of the specified type, or just the ones matching the
     * value of all 'ids' query parameters if indicated
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \TradeServe\CoreBundle\Entity\Organization $org
     * @param $group
     * @param $entity
     * @param $_format
     *
     * @return array
     * @throws \Exception
     */
    public function getEntitiesAction(Request $request, Organization $org, $group, $entity, $_format)
    {
        $page = $request->query->get('page');
        $size = $request->query->get('size');
        $items = $this->container->get('trade_serve.' . $entity . '.handler')->allByOrganization($org, true, $size, $page);
        $data = $this->serialize(array($entity => $items), $_format, $group);
        return new Response($data);
    }

    /**
     * Returns all entities of the specified type, or just the ones matching the
     * value of all 'ids' query parameters if indicated
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $group
     * @param $entity
     * @param $_format
     *
     * @return array
     */
    public function allEntitiesAction(Request $request, $org, $group, $entity, $_format)
    {
        $routeOptions = array(
            'organization' => $org,
            'group' => $group,
            'entity' => $entity,
            '_format' => $_format,
        );
        return $this->forward('TradeServeApiBundle:Default:getEntities', $routeOptions);
    }

    /**
     * Get single Entity.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \TradeServe\CoreBundle\Entity\Organization $org
     * @param $group
     * @param $entity
     * @param int $id the entity id
     *
     * @param $_format
     *
     * @return array
     * @throws \Exception
     */
    public function getEntityAction(Request $request, Organization $org, $group, $entity, $id, $_format)
    {
        try {
            $item = $this->container->get('trade_serve.' . $entity . '.handler')->findByOrganization($id, $org);
            $data = $this->serialize($item, $_format, $group);
            return new Response($data);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Create a Entity from the submitted data.
     *
     * @param \TradeServe\CoreBundle\Entity\Organization $org
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface
     */
    public function postEntityAction(Request $request, Organization $org, $group, $entity, $_format)
    {
        $orgs = array('organization' => $org->getId());
        $options = $request->request->all();

        $option = array_merge($orgs, $options);

        try {
            $newEntity = $this->container->get('trade_serve.' . $entity . '.handler')->post($option);

            $routeOptions = array(
                'org' => $org->getId(),
                'group' => $group,
                'entity' => $entity,
                'id' => $newEntity->getId(),
                'format' => $_format,
            );
            return $this->forward('TradeServeApiBundle:Default:getEntity', $routeOptions);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Update existing entity from the submitted data or create a new entity at a specific location.
     *
     * @param \TradeServe\CoreBundle\Entity\Organization $org
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $id the entity id
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface
     *
     * @throws NotFoundHttpException when entity not exist
     */
    public function putEntityAction(Request $request, Organization $org, $group, $entity, $id, $_format)
    {
        $orgs = array('organization' => $org->getId());
        $options = $request->request->all();

        $option = array_merge($orgs, $options);

        try {
            if (!($data = $this->container->get('trade_serve.' . $entity . '.handler')->findByOrganization($id, $org))) {
                $data = $this->container->get('trade_serve.' . $entity . '.handler')->post($data, $option);
            } else {
                $data = $this->container->get('trade_serve.' . $entity . '.handler')->find($id);
                $this->container->get('trade_serve.' . $entity . '.handler')->put($data, $option);
            }
            $routeOptions = array(
                'org' => $org->getId(),
                'group' => $group,
                'entity' => $entity,
                'id' => $data->getId(),
                'format' => $_format,
            );
            return $this->forward('TradeServeApiBundle:Default:getEntity', $routeOptions);
        } catch (InvalidFormException $exception) {
            return $exception->getForm()->getErrorsAsString();
        }
    }

    /**
     * Delete existing entity from the submitted id.
     *
     * @param \TradeServe\CoreBundle\Entity\Organization $org
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $id the entity id
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface
     *
     * @throws NotFoundHttpException when entity not exist
     */
    public function deleteEntityAction(Request $request, Organization $org, $group, $entity, $id, $_format)
    {
        if ($data = $this->container->get('trade_serve.' . $entity . '.handler')->findByOrganization($id, $org)) {
            $this->container->get('trade_serve.' . $entity . '.handler')->remove($data);
            return new Response('Object deleted.', 200);
        }
        //throw $this->createNotFoundException();
        return new Response('Object not found.', 400);
    }

}

