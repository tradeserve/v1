<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/14/15
 * Time: 7:18 PM
 */

namespace TradeServe\ApiBundle\Controller;

// Get Route Definition
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("Dispatch")
 */
class DispatchController extends FOSRestController
{

    /**
     * GET Route annotation.
     * @Get("/api/v1/{group}/organization/{org}/dispatch")
     */
    public function cgetAction($group, $org)
    {
        $em = $this->getDoctrine()->getManager();
        $service_request = $em->getRepository('TradeServe\CoreBundle\Entity\ServiceRequest')->findBy(array('organization' => $org));
        $method = 'json';
        $serializer = $this->container->get('trade_serve_serializer');

        $data = $serializer->serialize(array('service requests' => $service_request), $method, $group);
        return new Response($data);
    } // "get_dispatch"     [GET] /dispatch


    /**
     * GET Route annotation.
     * @Get("/api/v1/{group}/organization/{org}/dispatch/{id}")
     */
    public function getAction($group, $org, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $service_request = $em->getRepository('TradeServe\CoreBundle\Entity\ServiceRequest')->findOneBy(array(
            'organization' => $org,
            'id' => $id
        ));
        //$customers = $em->getRepository('TradeServe\CoreBundle\Entity\Dispatch')->findBy(array('organization' => $org));
        $method = 'json';
        $serializer = $this->container->get('trade_serve_serializer');

        $data = $serializer->serialize(array('service requests' => $service_request), $method, $group);
        return new Response($data);
    } // "get_dispatch"     [GET] /dispatch

}