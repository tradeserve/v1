<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 12/9/15
 * Time: 9:10 PM
 */

namespace TradeServe\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormTypeInterface;
use TradeServe\CoreBundle\Entity\Account;
use TradeServe\CoreBundle\Exception\InvalidFormException;
use TradeServe\CoreBundle\Model\EntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use TradeServe\ApiBundle\Controller\ApiController;

class CoreApiController extends ApiController
{
    /**
     * Returns all entities of the specified type, or just the ones matching the
     * value of all 'ids' query parameters if indicated
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $group
     * @param $entity
     * @param $_format
     *
     * @return \TradeServe\AppApiBundle\Controller\type
     * @throws \Exception
     *
     */
    public function getEntitiesAction(Request $request, $group, $entity, $_format)
    {
        try {
            $items = $this->container->get('trade_serve.' . $entity . '.handler')->all();
            $data = $this->serialize($items, $_format, $group);
            return new Response($data);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Returns all entities of the specified type, or just the ones matching the
     * value of all 'ids' query parameters if indicated
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $group
     * @param $entity
     * @param $_format
     *
     * @return type
     */
    public function allEntitiesAction(Request $request, $group, $entity, $_format)
    {
        $routeOptions = array(
            'group' => $group,
            'entity' => $entity,
            '_format' => $_format,
        );

        return $this->forward('TradeServeApiBundle:Default:getEntities', $routeOptions);
    }

    /**
     * Get single Entity.
     *
     * @param $group
     * @param $entity
     * @param int $id the entity id
     *
     * @param $_format
     *
     * @return array
     * @throws \Exception
     */
    public function getEntityAction($group, $entity, $id, $_format)
    {
        try {
            $item = $this->container->get('trade_serve.' . $entity . '.handler')->find($id);
            $data = $this->serialize($item, $_format, $group);
            return new Response($data);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Create a Entity from the submitted data.
     *
     * @param Request $request the request object
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface|View
     */
    public function postEntityAction(Request $request, $group, $entity, $_format)
    {
        try {

            $newEntity = $this->container->get('trade_serve.' . $entity . '.handler')->post(array_merge(array(
                json_decode($request->getContent(), true)
            )));

            $routeOptions = array(
                'group' => $group,
                'entity' => $entity,
                'id' => $newEntity->getId(),
                'format' => $_format,
            );

            return $this->forward('TradeServeApiBundle:Default:getEntity', $routeOptions);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Update existing entity from the submitted data or create a new entity at a specific location.
     *
     * @param Request $request the request object
     * @param int $id the entity id
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when entity not exist
     */
    public function putEntityAction(Request $request, $group, $entity, $id, $_format)
    {
        try {
            if (!($data = $this->container->get('trade_serve.' . $entity . '.handler')->find($id))) {
                $data = $this->container->get('trade_serve.' . $entity . '.handler')->post($data,
                    array_merge(array(json_decode($request->getContent(), true))));
            } else {
                $data = $this->container->get('trade_serve.' . $entity . '.handler')->put($data,
                    array_merge(array(json_decode($request->getContent(), true))));
            }
            $routeOptions = array(
                'group' => $group,
                'entity' => $entity,
                'id' => $data->getId(),
                'format' => $_format,
            );

            return $this->forward('TradeServeApiBundle:Default:getEntity', $routeOptions);
        } catch (InvalidFormException $exception) {
            return $exception->getForm()->getErrorsAsString();
        }
    }

    /**
     * Delete existing entity from the submitted id.
     *
     * @param Request $request the request object
     * @param int $id the entity id
     * @param $group
     * @param $entity
     * @param $_format
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when entity not exist
     */
    public function deleteEntityAction(Request $request, $group, $entity, $id, $_format)
    {
        if ($data = $this->container->get('trade_serve.' . $entity . '.handler')->find($id)) {
            $this->container->get('trade_serve.' . $entity . '.handler')->remove($data);

            return new \Symfony\Component\HttpFoundation\Response('Object deleted.', 200);
        }

        //throw $this->createNotFoundException();
        return new \Symfony\Component\HttpFoundation\Response('Object not found.', 400);
    }

}
