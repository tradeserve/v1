# Trade Serve DEV ENVIRONMENT

The development environment for TradeServe uses several tools to build a virtual machine on your local workstation. This virtual machine allows an exact replica of a production environment to exist on every workstation so each developer/designer works off of a standardized environment across the entire team.

This will give you a brief overview of the installation and build processes to create your local development environment for TradeServe.

The instructions below assume you are using a MAC (or Unix based operating system), but will function almost identically if using Windows except for directory structures.

### System Requirements

* 4GB RAM (8GB preferred)
* Dual Core CPU, Intel Core i5 or better recommended
* At least 4GB of available hard disk space
* **Reliable** broadband internet connection

## Local Setup

Follow these steps to build the complete TradeServe development environment for the application.

#### General Setup

Install the required software, and create a working directory `TradeServe`

## Required Software

* [Vagrant](https://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

### Generate an SSH Key

Generate an SSH Key locally to auto-authenticate with Githug.com: https://help.github.com/articles/generating-ssh-keys

### Create local working directory.
    $ sudo mkdir /users/<yourname>/TradeServe

### Clone the TradeServe repositories to your working directory.

    $ cd /users/<yourname>/TradeServe
    $ git clone git@bitbucket.org:tradeserve/v1.git

### Spawn the TradeServe virtual machine.

    $ cd /users/<yourname>/TradeServe/v1
    $ vagrant up

When vagrant is finished (expect 15-20 minutes to build), you will need to setup a hostname to point to the IP Address of the application host.

## Setup workstation hosts file

Add the following to your ```hosts``` file:

    $ 192.168.50.20  tradeserve.dev        

### Linux/MAC

    $ /private/etc/hosts

### Windows

    $ c:/windows/system32/drivers/etc/hosts

## SSH into the TradeServe virtual machine & Build the Assets (Frontend)

    $ cd /users/<yourname>/TradeServe/v1
    $ vagrant ssh

    password is vagrant

    $ vagrant@precise64:~$  cd /var/www/tradeserve/www
    $ vagrant@precise64:~$  composer update --prefer-dist
    $ vagrant@precise64:~$  npm install
    $ vagrant@precise64:~$  gulp